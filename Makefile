################################################################
# Makefile for MathSeer Pipeline
#
# R. Zanibbi, Aug. 12, 2021
#################################################################

# Weight file names (will change)
SSDW="ssd512GTDB_256_epoch15.pth"
SSDW_C="ssd512GTDB_256_epoch7_chem.pth"
QDDW="net_39.pt"
QDDW="net_89.pt"

##################################
# Installation
###################################
# default: build all, stop on failure
all:
	./bin/install

# 'make force' - do not stop on failure
force:
	./bin/install force

##################################
# Docker container
###################################
# Usage: 
#  1. Login to dockerhub (docker login -u 'dprl')
#  2. To make 'staging' container, issue:
#
#        make docker
#        make docker-push
#
#  3. To make custom tag for a container, issue:
#
#        make tag=tag_name docker-tag
#        make tag=tag_name docker-push-tag
docker:
	docker build . -f chemscraper.Dockerfile -t dprl/chemscraper:staging

docker-push: docker
	docker push dprl/chemscraper:staging

docker-tag:
	# USAGE: make tag=tag_label docker-tag 
	# Will create chemscraper container with label 'tag_label'
	docker build . -f chemscraper.Dockerfile -t dprl/chemscraper:${tag}

docker-push-tag: 
	# USAGE: make tag=tag_label docker-push-tag 
	# Pushes the named container.
	docker push dprl/chemscraper:${tag}

##################################
# Example executions
###################################
# 'make example' - run small ACL collection
example:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL \
		--convert_pdfs 1  -v 1 -pm 1

example-yolo:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL \
		--convert_pdfs 1  -v 1 --detector yolo

# 'make sacl' - run just one page from each ACL file.
sacl:
	./run-msp --input_dir ./inputs/ACL-small --output_dir ./outputs/ACL-small \
		--convert_pdfs 1 -v 1

sacl-yolo:
	./run-msp --input_dir ./inputs/ACL-small --output_dir ./outputs/ACL-small \
		--convert_pdfs 1 -v 1 --detector yolo


# 'make acl2' - run two pages from 2 ACL docs
acl2:
	./run-msp --input_dir ./inputs/ACL-2 --output_dir ./outputs/ACL-2 \
		--convert_pdfs 1 -v 1

acl2-yolo:
	./run-msp --input_dir ./inputs/ACL-2 --output_dir ./outputs/ACL-2 \
		--convert_pdfs 1 -v 1 --detector yolo

# 'make k15' - run single ACL doc
k15:
	./run-msp --input_dir ./inputs/K15-1002 --output_dir ./outputs/K15-1002 \
		--convert_pdfs 1 -v 1

k15-yolo:
	./run-msp --input_dir ./inputs/K15-1002 --output_dir ./outputs/K15-1002 \
		--convert_pdfs 1 -v 1 --detector yolo

# 'make 1-example' - run one complete ACL file.
1-example:
	./run-msp --input_dir ./inputs/A00_3007 --output_dir ./outputs/A00_3007 \
		--convert_pdfs 1 -v 1

1-example-yolo:
	./run-msp --input_dir ./inputs/A00_3007 --output_dir ./outputs/A00_3007 \
		--convert_pdfs 1 -v 1 --detector yolo

# 'make qexample' - run small ACL collection, don't regenerate images or vis.
qexample:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL 

qexample-yolo:
	./run-msp --input_dir ./inputs/ACL --output_dir ./outputs/ACL --detector yolo

# 'make chem-test' - run 1 Chem CLEF document
chem-test:
	./run-msp --input_dir ./inputs/CLEF_TEST --output_dir ./outputs/CLEF_TEST --mode 1 \
	    --convert_pdfs 1 -v 1

chem-test-yolo:
	./run-msp --input_dir ./inputs/CLEF_TEST --output_dir ./outputs/CLEF_TEST --mode 1 \
	    --convert_pdfs 1 -v 1 --detector yolo


# 'make chem-test-multi' - run 2 Chem CLEF documents
chem-test-2:
	./run-msp --input_dir ./inputs/CLEF_TEST_2 --output_dir ./outputs/CLEF_TEST_2 --mode 1 \
	    --convert_pdfs 1 -v 1

chem-test-2-yolo:
	./run-msp --input_dir ./inputs/CLEF_TEST_2 --output_dir ./outputs/CLEF_TEST_2 --mode 1 \
	    --convert_pdfs 1 -v 1 --detector yolo

chem-scraper-example:
	./run-chemx --v2 --in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0 --rd_smiles 0

chem-scraper-example-small:
	./run-chemx --v2 --in_pdf_dir inputs/chemxtest_inpdfs/small --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0 --rd_smiles 0

chem-scraper-example-small-images:
	./run-chemx --v2 --in_pdf_dir inputs/chemxtest_inpdfs/small_images --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0 --rd_smiles 0

chem-scraper-example-3:
	./run-chemx --in_pdf_dir inputs/chemxtest_inpdfs/3page --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

chem-scraper-example-4:
	./run-chemx --in_pdf_dir inputs/chemxtest_inpdfs/4page --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

chem-scraper-example-8:
	./run-chemx --in_pdf_dir inputs/chemxtest_inpdfs/8page --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

chem-scraper-again-example:
	./run-chemx --parse_only \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

##################################
# Parser v. 2 for ChemScraper
###################################
chem-v2-example:
	./run-chemx --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0 --rd_smiles 0  --viz_dets 1

chem-v2-skip-ss-yolo:
	./run-chemx --v2 --no_ssc --no_yolo\
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 1 --rd_smiles 0  --viz_dets 1

chem-v2-all-test:
	./run-chemx --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 1 --rd_smiles 0  --viz_dets 1 --svg

chem-v2-parse-svg:
	./run-chemx --parse_only --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 1 --svg

chem-v2-all-test-p:
	./run-chemx --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/one_page --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 1 --rd_smiles 0  --viz_dets 1 

chem-v2-example-new-smiles:
	./run-chemx --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

chem-v2-parse:
	./run-chemx --parse_only --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

chem-v2-indigo:
	./digital-parse-eval-smiles
	
chem-v2-parse-seq:
	# Running parser without parallelization
	./run-chemx --parse_only --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0 --np

chem-v2-parse-seq-p:
	# Running parser without parallelization
	./run-chemx --parse_only --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/one_page --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0 --np

chem-scraper-tests:
	./run-chemx --in_pdf_dir inputs/chemxtest_inpdfs/tests --smi_pngs 1 --dpi 300 --cdxml_page 1 \
    --viz_dets 1

chem-scraper-again-tests:
	./run-chemx --in_pdf_dir inputs/chemxtest_inpdfs/tests --dpi 300 --cdxml_page 1 \
    --viz_dets 1 --parse_only

chem-tests-v2:
	./run-chemx --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/tests --dpi 300 --cdxml_page 1 --smi_pngs 1 \
		--eval 0

chem-tests-v2-parse:
	./run-chemx --parse_only --v2 \
		--in_pdf_dir inputs/chemxtest_inpdfs/tests --dpi 300 --cdxml_page 1 --smi_pngs 0 \
		--eval 0

chem-scraper-extra:
	./run-chemx --in_pdf_dir inputs/scott_extra/ --dpi 300 --cdxml_page 1

#icdar/ijdar related
chem-ijdar-USPTO-indigo:
	./digital-parse-eval-smiles -c modules/chemscraper/metrics/configs_datasets/USPTO.ini

chem-ijdar-CLEF:
	./digital-parse-eval-smiles -c modules/chemscraper/metrics/configs_datasets/CLEF.ini

chem-ijdar-UOB:
	./digital-parse-eval-smiles -c modules/chemscraper/metrics/configs_datasets/UOB.ini

##################################
# Cleaning and Reinstallation
###################################
# 'make clean-out' remove only output data from running examples
clean-out:
	rm -fr ./outputs/* ./STDERR_*

# 'make clean-example' - remove all data from running examples
clean-example:
	rm -fr ./outputs/*
	rm -fr ./inputs/*/images/*

# 'make clean' - delete all items from installation script and example.
clean: clean-example
	rm -fr ./modules/lgeval
	rm -f  ./modules/scanssd/ssd/trained_weights/${SSDW}
	rm -f  ./modules/scanssd/ssd/trained_weights/${SSDW_C}
	rm -f  ./modules/lgap-parser/outputs/run/infty_contour/${QDDW}
	rm -f  ./run-msp
	rm -f  ./run-chemx
	rm -f  ./generate-visual-lg
	rm -f  ./run-server
	rm -f  ./digital-parse-eval-smiles
	rm -f  ./visual-postprocess-eval-smiles
	rm -f ./images-to-lg
	rm -f ./lg2nx
	rm -f ./bin/viz-svgs
	rm -f ./bin/filter-exact-matches
	rm -f ./bin/filter-triple-bonds
	rm -f ./bin/combine-chunks

#Stop and remove docker containers
clean-containers: 
	./bin/clean-containers
	

# 'make clean-repos' - remove repositories in the modules directory.
clean-repos:
	rm -fr ./modules/symbolscraper-server
	rm -fr ./modules/scanssd
	rm -fr ./modules/lgap-parser
	rm -fr ./modules/protables
	rm -fr ./modules/scaled-yolov4
	rm -fr ./outputs/*
	rm -fr ./modules/lgeval
	rm -f  ./modules/scanssd/ssd/trained_weights/${SSDW}
	rm -f  ./modules/scanssd/ssd/trained_weights/${SSDW_C}
	rm -f  ./modules/lgap-parser/outputs/run/infty_contour/${QDDW}
	rm -f  ./run-msp
	rm -f  ./run-chemx
	rm -f  ./digital-parse-eval-smiles
	rm -f  ./visual-postprocess-eval-smiles
	rm -f  ./grid-search

# 'make clean-full' - remove all installation data.
clean-full: clean clean-repos clean-containers

# 'make conda-remove' - remove mathseer conda environment
conda-remove:
	conda env remove -n graphext

# The 'start again' rule.
# 'make rebuild' - removes conda environment, cleans everything,  reinstalls pipeline
rebuild: conda-remove clean-full all


##################################
# Remove logs
###################################
# 'make clean-logs' removes all .log files in the logs folder and its sub-folders
clean-logs:
	rm -f ./logs/*.log
	rm -f ./logs/aiopipeline/*.log

download-ACL-10:
	./bin/download-acl-subset ACL-subsets/acl10.txt ACL-10

download-ACL-25:
	./bin/download-acl-subset ACL-subsets/acl25.txt ACL-25

download-ACL-50:
	./bin/download-acl-subset ACL-subsets/acl50.txt ACL-50

download-ACL-100:
	./bin/download-acl-subset ACL-subsets/acl100.txt ACL-100

download-ACL-500:
	./bin/download-acl-subset ACL-subsets/acl500.txt ACL-500

download-ACL-1000:
	./bin/download-acl-subset ACL-subsets/acl1k.txt ACL-1000

download-synthetic-chem-data:
	./bin/download-synthetic-chem-data

download-real-chem-data:
	./bin/download-real-chem-data
	
ACL-10: download-ACL-10
	./run-msp --input_dir ./inputs/ACL-10 --output_dir ./outputs/ACL-10 \
		--convert_pdfs 1  -v 0

ACL-10-yolo: download-ACL-10
	./run-msp --input_dir ./inputs/ACL-10 --output_dir ./outputs/ACL-10 \
		--convert_pdfs 1  -v 0 --detector yolo

ACL-25: download-ACL-25
	./run-msp --input_dir ./inputs/ACL-25 --output_dir ./outputs/ACL-25 \
		--convert_pdfs 1  -v 0

ACL-25-yolo: download-ACL-25
	./run-msp --input_dir ./inputs/ACL-25 --output_dir ./outputs/ACL-25 \
		--convert_pdfs 1  -v 0 --detector yolo

ACL-50: download-ACL-50
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 0

ACL-50-yolo: download-ACL-50
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 0 --detector yolo

ACL-100: download-ACL-100
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 1

ACL-100-yolo: download-ACL-100
	./run-msp --input_dir ./inputs/ACL-50 --output_dir ./outputs/ACL-50 \
		--convert_pdfs 1  -v 1 --detector yolo

generate-test-data:
	./generate-visual-lg --data_file modules/chemscraper/data/indigo_pubchem_small.csv -id 1 -sm 3 -o outputs/generated_dataset/indigo_small/

generate-indigo-data:
	./generate-visual-lg --data_file modules/chemscraper/data/synthetic/indigo.csv -id 0 -sm 2

generate-chem-small-train:
	./generate-visual-lg -c 1 -v 0 -p 1 --data_file modules/chemscraper/data/indigo_pubchem_small.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/chem/train -lgs 0

generate-chem-small-test:
	./generate-visual-lg -c 1 -v 0 -p 1 --data_file modules/chemscraper/data/indigo_pubchem_small_test.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/chem/test -lgs 0

generate-chem-small-train-vis:
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_pubchem_small.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/chem/train -lgs 0

generate-chem-small-test-vis:
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_pubchem_small_test.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/chem/test -lgs 0

generate-chem-debug-one:
	./generate-visual-lg -c 1 -v 0 -p 0 --data_file modules/chemscraper/data/tests_primitives_bryan/indigo_bryan_one.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/bryan_debug -lgs 0

generate-chem-5k-uspto-indigo-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -cs 1000 --data_file modules/chemscraper/data/synthetic/indigo.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/test_uspto_indigo/ -w 6

generate-chem-5k-uspto-rdkit-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 -s _rdkit -cs 1000 --data_file modules/chemscraper/data/synthetic/indigo.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/test_uspto_rdkit/ -w 20 

generate-chem-5k-pubchem-indigo-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -cs 1000 --data_file modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo/

generate-chem-5k-pubchem-rdkit-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 --debug 1 -s _rdkit -cs 1000 --data_file modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_rdkit

generate-chem-5k-pubchem-indigo-aug-str-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -ma 1 -s _structure -cs 1000 --data_file modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/

generate-chem-5k-pubchem-indigo-aug-ren-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -d 0 -s _rendered -cs 1000 --data_file modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/

generate-chem-5k-pubchem-indigo-aug-rare-full:
	./generate-visual-lg -c 1 -v 1 -p 1 -mar 1 -s _rare -cs 1000 --data_file modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/

generate-chem-5k-uspto-indigo:
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_5k/chunk1.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/test_uspto_indigo/chunk1 -w 8 -cslg 100
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_5k/chunk2.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/test_uspto_indigo/chunk2 -w 8 -cslg 100
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_5k/chunk3.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/test_uspto_indigo/chunk3 -w 8 -cslg 100
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_5k/chunk4.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/test_uspto_indigo/chunk4 -w 8 -cslg 100

generate-chem-5k-pubchem-indigo:
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/pubchem_5k_random/chunk1.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo/chunk1
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/pubchem_5k_random/chunk2.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo/chunk2
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/pubchem_5k_random/chunk3.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo/chunk3
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/pubchem_5k_random/chunk4.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo/chunk4
	./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/pubchem_5k_random/chunk5.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo/chunk5

generate-chem-5k-pubchem-rdkit:
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 --debug 1 -s _rdkit --data_file modules/chemscraper/data/pubchem_5k_random/chunk1.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_rdkit/chunk1
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 --debug 1 -s _rdkit --data_file modules/chemscraper/data/pubchem_5k_random/chunk2.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_rdkit/chunk2
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 --debug 1 -s _rdkit --data_file modules/chemscraper/data/pubchem_5k_random/chunk3.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_rdkit/chunk3
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 --debug 1 -s _rdkit --data_file modules/chemscraper/data/pubchem_5k_random/chunk4.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_rdkit/chunk4
	./generate-visual-lg -c 1 -v 1 -p 1 -an 1 --debug 1 -s _rdkit --data_file modules/chemscraper/data/pubchem_5k_random/chunk5.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_rdkit/chunk5

generate-chem-5k-pubchem-indigo-aug-str:
	./generate-visual-lg -c 1 -v 1 -p 1 -ma 1 -s _structure --data_file modules/chemscraper/data/pubchem_5k_random/chunk1.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk1
	./generate-visual-lg -c 1 -v 1 -p 1 -ma 1 -s _structure --data_file modules/chemscraper/data/pubchem_5k_random/chunk2.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk2
	./generate-visual-lg -c 1 -v 1 -p 1 -ma 1 -s _structure --data_file modules/chemscraper/data/pubchem_5k_random/chunk3.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk3
	./generate-visual-lg -c 1 -v 1 -p 1 -ma 1 -s _structure --data_file modules/chemscraper/data/pubchem_5k_random/chunk4.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk4
	./generate-visual-lg -c 1 -v 1 -p 1 -ma 1 -s _structure --data_file modules/chemscraper/data/pubchem_5k_random/chunk5.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk5

generate-chem-5k-pubchem-indigo-aug-ren:
	./generate-visual-lg -c 1 -v 1 -p 1 -d 0 -s _rendered --data_file modules/chemscraper/data/pubchem_5k_random/chunk1.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk1
	./generate-visual-lg -c 1 -v 1 -p 1 -d 0 -s _rendered --data_file modules/chemscraper/data/pubchem_5k_random/chunk2.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk2
	./generate-visual-lg -c 1 -v 1 -p 1 -d 0 -s _rendered --data_file modules/chemscraper/data/pubchem_5k_random/chunk3.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk3
	./generate-visual-lg -c 1 -v 1 -p 1 -d 0 -s _rendered --data_file modules/chemscraper/data/pubchem_5k_random/chunk4.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk4
	./generate-visual-lg -c 1 -v 1 -p 1 -d 0 -s _rendered --data_file modules/chemscraper/data/pubchem_5k_random/chunk5.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk5

generate-chem-5k-pubchem-indigo-aug-rare:
	./generate-visual-lg -c 1 -v 1 -p 1 -mar 1 -s _rare --data_file modules/chemscraper/data/pubchem_5k_random/chunk1.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk1
	./generate-visual-lg -c 1 -v 1 -p 1 -mar 1 -s _rare --data_file modules/chemscraper/data/pubchem_5k_random/chunk2.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk2
	./generate-visual-lg -c 1 -v 1 -p 1 -mar 1 -s _rare --data_file modules/chemscraper/data/pubchem_5k_random/chunk3.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk3
	./generate-visual-lg -c 1 -v 1 -p 1 -mar 1 -s _rare --data_file modules/chemscraper/data/pubchem_5k_random/chunk4.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk4
	./generate-visual-lg -c 1 -v 1 -p 1 -mar 1 -s _rare --data_file modules/chemscraper/data/pubchem_5k_random/chunk5.csv  -id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_indigo_rdkit/train_pubchem_5k_indigo_augmentation/chunk5

generate-chem-5k-pubchem-augmentation: generate-chem-5k-pubchem-indigo-full generate-chem-5k-pubchem-indigo-aug-str-full generate-chem-5k-pubchem-indigo-aug-ren-full generate-chem-5k-pubchem-indigo-aug-rare-full 

generate-chem-small: generate-chem-small-train generate-chem-small-test

generate-chem-small-vis: generate-chem-small-train-vis generate-chem-small-test-vis

convert_lg2nx: 
	./lg2nx

convert_lg2nx-test: 
	./lg2nx -i modules/lgap-parser/data/generated-dataset/chem/test/lgs/line/ -o modules/lgap-parser/data/generated-dataset/chem/test/cdxml_svg/line/

visualize-svgs:
	./bin/viz-svgs

visualize-svgs-test:
	./bin/viz-svgs -s modules/lgap-parser/data/generated-dataset/chem/test/cdxml_svg/line/ -p modules/lgap-parser/data/generated-dataset/chem/test/pdf/
