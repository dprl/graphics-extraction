[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# ChemScraper: Leveraging PDF Graphics Instructions for Molecular Diagram Parsing

**Authors:** Ayush Kumar Shah, Bryan Amador, Abhisek Dey, Ming Creekmore, Richard Zanibbi   
[Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html), Rochester Institute of Technology, USA

## Overview
**Purpose:** This pipeline extracts molecular diagrams from PDF documents,
parses their symbols, and then exports them as CDXML files or SMILES strings. It
was developed at RIT (USA) to provide chemical diagram extraction for use within
literature mining tools being created for the [Molecule Maker Lab
Institute](https://gitlab.com/dprl/graphics-extraction). 

The following is an overview of processing in ChemScraper used in different
parts of the system:

1. **Render PDF from SMILES**: Render PDF from SMILES strings using [Indigo Toolkit](https://lifescience.opensource.epam.com/indigo/).
2. **Character and Graphics (PDF primitives) Extraction ([SymbolScraper](https://gitlab.com/dprl/symbolscraper-server))**: Extract individual characters and graphical elements from the PDF document.
3. **PDF to Image Conversion (pymupdf)**: Convert PDF pages or specific formulas into PNG image format.
4. **Formula Detection (YOLOv8)**: Detect and localize formula regions within the PDF page images, generating bounding boxes for each formula.
5. **Visual Primitive Extraction**: Extract basic visual elements (such as lines) from the formula images (PNGs) by applying line-extraction method (openCV) and geometric constraints.
6. **Visual Graph Construction**:
   - **From PDF**: Build a minimum spanning tree (MST) from the extracted PDF symbols, followed by restructuring.
   - **From Raster Images**: Use the Line-of-sight Graph Attention Parser (LGAP) to generate a graph from extracted visual primitives.
7. **Label Graph File Generation**: Create label graph (LG) files from the visual graphs of raster images for use in training or inference.
8. **Conversion to Chemical Graph**: Apply graph rewrite rules based on chemical and geometric constraints to transform the visual graph, replacing bond nodes with edges.
9. **Conversion to CDXML**: Export the chemical graph into CDXML format, annotating nodes as atoms and edges as bonds.
10. **CDXML to SMILES and SVG ([molconvert](https://docs.chemaxon.com/display/docs/molconvert.md))**: Generate SMILES strings and render SVG representations by converting the CDXML files using Marvin molconvert.

These processing steps are shared among the different modes/parts of the system,
which is tabulated below:

| Born-digital Parser                                 | Visual Parser (Pipeline Mode)                    |Ground Truth Annotated Data Generation                                |
|-----------------------------------------------------|------------------------------------------------------------|---------------------------------------------------------|
| |                                                        | Render PDF from SMILES                                  |
| Character and Graphics (PDF primitives) Extraction from PDF |                                                        | Character and Graphics (PDF primitives) Extraction from PDF 
| PDF to Image Conversion                             | PDF to Image Conversion                                     | PDF to Image Conversion                                 |
|                                                     | Visual Primitive Extraction (from PNG)                      | Visual Primitive Extraction (from PNG)                  |
| Formula Detection                                   | Formula Detection                                           | Align visual primitives with PDF primitives             |
| Visual Graph Construction on PDF primitives: MST + restructure | Visual Graph Construction on visual primitives: LGAP         | Visual Graph Construction on aligned visual primitives: MST + restructure    |
|                                                     | Predicted Label Graph File Generation                        | Ground Truth Label Graph File Generation                |
| Conversion to Chemical Graph                        | Conversion to Chemical Graph                                |                                                         |
| Conversion to CDXML                                 | Conversion to CDXML                                         |                                                         |
| CDXML to SMILES and SVG                             | CDXML to SMILES and SVG                                     |                                                         |


<!-- ## Overview -->

<!-- **Purpose:** This pipeline extracts molecular diagrams from PDF documents, parses their symbols, and then exports them as CDXML files or SMILES strings. It was developed at RIT (USA) to provide chemical diagram extraction for use within literature mining tools being created for the [Molecule Maker Lab Institute](https://gitlab.com/dprl/graphics-extraction). --> 

<!-- The pipeline generates a set of CDXML documents, and a set of SMILES strings for each PDF. Notes: -->

<!-- 1. SMILES strings are generated using Marvin [molconvert](https://docs.chemaxon.com/display/docs/molconvert.md) from the exported CDXML files -->
<!-- 2. Our PDF symbol scraping tool SymbolScraper is called using a Docker Container [service](https://gitlab.com/dprl/symbolscraper-server) --> 
<!-- 3. Bounding box locations for molecular diagrams are obtained using a YOLOv8 detector (PyTorch). -->
<!-- 4. SMILES Index (SMILES-Page Region) or CDXML molecule files are provided per region or per page for every PDF. -->

<!-- The following is an overview of processing in ChemScraper: -->

![ChemScraper](Figures/pipeline.jpg "Chemscraper")
Fig: Overview of the ChemScraper Pipeline

<!-- Currently outputs from different applications are combined using command-line scripts. We hope to provide a Docker-based version in the future. -->

## Installation

### Downloading Repo, Selecting ChemScraper Branch

1. Issue `git clone https://gitlab.com/dprl/graphics-extraction.git`
2. Then issue the following commands from the same directory:
```zsh
cd graphics-extraction
git checkout dev
```

**Important!:** If you are not on the correct branch (`dev`), you will be 
unable to run the commands below (the full system also includes modules for math
formula extraction).

### Requirements

1. Python
2. Conda (e.g., from Anaconda) and pip for Python environments
3. Java (Oracle JDK, See below)
4. Maven (build tool for Java)
5. Unix shell (e.g., bash or zsh)
6. wget unix utility
7. Marvin Suite
8. ChemDraw Suite (optional)

**Marvin Suite and Oracle JDK Installation (Ubuntu) (Required) (For Exporting SMILES of extracted Molecules in TSV)**

Make sure you have Oracle Java - 18 (NOT OpenJDK) installed on your system and the `$JAVA_HOME` env variable contains the path to Java (e.g., `/usr/` on our system). You can download the .deb package from [here](https://www.oracle.com/java/technologies/downloads/).

* Sign-up for a ChemAxon Account [here](https://account.chemaxon.com/register)
* You can then download and install the Marvin .deb package from [here](https://chemaxon.com/products/marvin/download#deb) and obtain a non-commercial license
* If you get `JAVA Not Found` or `JAVA version not supported error` make sure the right Java is being used - you can check which java is being used by `sudo update-alternatives --config java`


The pipeline has been tested primarily on Linux systems (specifically, Ubuntu 20.04 LTS).

### Building the System

Once you have the requirements described in the previous section installed, from the top directory of the project, run the build script by issuing:

```zsh
make
```
This will install all modules, and create a bash script named `run-chemx` (for the ChemScraper pipeline). Instructions on how to use the script are provided below.

To force installation to continue after encountering errors, issue:

```zsh
make force
```

### Removal and Reinstallation

To remove all installed data and tools, issue:

```zsh
make clean-repos
```
This is particularly useful if you want to rebuild the system from scratch ('cleanly').

To rebuild the entire system, including the conda environment, issue:

```zsh
make rebuild
```


## Getting Started: Running the ChemScraper Demo

To run ChemScraper on a provided example PDF, you can issue either:

```zsh
make chem-v2-example
```

or, equivalently:
```zsh
./run-chemx --v2 \
  --in_pdf_dir inputs/chemxtest_inpdfs/All --dpi 300 \
  --cdxml_page 1 --smi_pngs 0 --eval 0 --rd_smiles 0  --viz_dets 1
```
**Outputs** are written to the `outputs/All` directory, whose subdirectories include:
1. `xmls`: SymbolScraper outputs
2. `images`: Page-wise images for each PDF
3. `yolo_boxes`: YOLOv8 detections for each PDF
4. `generated_cdxmls`: Generated CDXML encodings for each detected and parsed 
molecule per page per PDF. Document level CDXML also available. These can be
directly imported in ChemDraw or similar program and viewed/edited.
5. `generated_smiles`: Generated SMILES strings in text files, one for each molecule found in the example PDF, with pages and numbers associated with each molecule in filenames.
*Optional* - Generated PNGs for each SMILES string.
6. `generated_tsv`: Generated index for all molecules processing in the demonstration PDF.

After running the demo, SMILES strings along with their associated CDXML files
are recorded in the TSV file `outputs/All/generated_smiles/or100.09.tables/smiles_out.txt`. 

For the demo example, png generation mode was enabled and rendered molecule
pngs may be found in the same directory. For example, the SMILES for the CDXML
`Page_10_No7.cdxml` in the directory `outputs/All/generated_cdxmls/or100.09.tables`
is **`*c1ccccc1`**, and its corresponding png image is shown below:

![Page10Mol7](Figures/Page_10_No7.png "Chemscraper")

**Optional Parameters**

* `viz_dets`: Visualize the detections of YOLOv4 
* `smi_pngs`: Generate Extracted Molecule PNGs
* `dpi`: The DPI to convert PDF to Images and run the detector


For description of different parts of the system, go to
[README-system-parts.md](README-system-parts.md).

## Parsing and evaluating standard datasets (paper)
### Download datasets
```
make download-synthetic-chem-data
make download-real-chem-data
```

### Run ChemScraper
1. Indigo dataset (USPTO SMILES rendered using Indigo Toolkit) - 5719 molecules <br>
  ```zsh
  make chem-ijdar-USPTO-indigo
 ```
2. Maybridge UOB Dataset - 5740 molecules
  ```zsh
  make chem-ijdar-UOB
 ```
3. CLEF 2012 Dataset - 992 molecules
  ```zsh
  make chem-ijdar-CLEF
 ```

## Generating Annotated dataset
To run annotated dataset generation on a given test file containing SMILES,
```zsh
make generate-test-data
```

The data is generated in the directory `outputs/generated-dataset/indigo_small/` by
default.

To generate annotated dataset from other SMILES source, use:
```zsh
./generate-visual-lg --data_file path/to/SMILES.csv \
  --id_index_in_file 1 --smiles_index_in_file 3 \
  --output_dir outputs/output_dir_name --visualize 0
```

The main parameters shown above are:
- `data_file`: The CSV file containing SMILES and it's corresponding ID or
    filename
- `id_index_in_file`: The column number corresponding to the ID or filename
- `smiles_index_in_file`: The column number corresponding to the SMILES
- `output_dir`: The output directory for the annotated data

For detailed description of all the available arguments, look at `modules/generate_data/generate_visual_lg.py`

For detailed description of related data generation and evaluation scripts with examples,
go to [README-generate-eval.md](README-generate-eval.md)

## Running your own PDFs
**Note:** The system only processes born-digital PDFs with molecule diagrams
encoded using PDF drawing instructions (i.e., not embedded as png/jpeg images
within a PDF).

### Using python code
To run your own PDFs with ChemScraper, first  create a directory containing your
collection of PDF files. For this example, let's imagine you name this directory `in_pdfs`. 

To run ChemScraper on these PDFs, issue in the command:
```zsh
./run-chemx --v2 \
  --in_pdf_dir dir/to/in_pdfs --dpi 300 \
  --cdxml_page 1 --smi_pngs 0 --eval 0
```

### Using server (through fastapi)
Run the following command to serve a fastapi version of chemscraper.
```zsh
./run-server
```

Navigate to [localhost:8000/docs](http://localhost:8000/docs) and try the
`extractPdf` service on your PDF. This will generate the cdxml files (per
molecule, per page, and per document), along with SVGs if requested through
`generate_svg` flag in a downloadable zip file.

<!-- # SMILES Molecular Fingerprint Search Demo -->

<!-- As a proof of concept, we implemented a tool that indexes molecules extracted from ChemScraper as extended connectivity fingerprint vectors. For each query, the similiarity of the query fingerprint and the candidate fingerprint is computed, and this is used to rank each candidate in the database. --> 

<!-- To run a demonstration, execute the following command: -->

<!-- ```bash -->
<!-- ./chem-search-poc -->
<!-- ``` -->

## Visual Parser

### Generate input data for parser
```bash
make generate-chem-small
```

### (Optional) Convert generated files to cdxml/svg
```bash
make convert_lg2nx
make convert_lg2nx-test
```

### (Optional) Visualize generated cdxml/svgs
```bash
make visualize-svgs
make visualize-svgs-test
```

### Training and testing Parser
```bash
cd modules/lgap-parser
make conda-remove &&  make
```

#### Training (recurrent)
```bash
make train-chem-small-single
```

### Testing (recurrent)
```bash
make test-chem-small-single
```

## Contributors

The dprl graphics extraction pipeline was developed by the [Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html) at the Rochester Institute of Technology (USA), for the [MathSeer](https://www.cs.rit.edu/~dprl/mathseer) and [MMLI](https://moleculemaker.org/) projects. The contributors are listed below:

* **Parser, CDXML translation/indexing, Pipeline and Documentation:** Ayush Kumar Shah, Bryan Amador, Abhisek Dey, Ming Creekmore
* **Visual Primitive Extraction:** Bryan Amador, Brandon Kirincich, Ayush Kumar Shah
* **Annotated Data Generation:** Ayush Kumar Shah
* **Evaluation:** Bryan Amador, Ayush Kumar Shah
* **YOLOv8-Based Chemical Detector:** Matt Langenkamp
* **SymbolScraper, Chemical Search:** Bryan Amador, Matt Langenkamp
* **Chemistry Expertise and Chemical Group Name Dictionary:** Blake Ocampo, Abhisek Dey, Scott Denmark
* **Misc. Programming and Documentation:** Richard Zanibbi

The first ChemScraper pipeline was released in February 2023.

## Support

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.
