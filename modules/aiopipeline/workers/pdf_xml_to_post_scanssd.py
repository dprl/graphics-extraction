import logging
from io import BytesIO
from queue import Empty
from typing import List, Type, Dict
from multiprocessing import Queue, Event
import aiohttp
from copy import copy
from modules.aiopipeline.model.PdfXml import PdfXml
import os
import asyncio
from modules.aiopipeline.workers.utils import all_true, check_status

logger = logging.getLogger('post_scanssd')


def pdf_xml_to_post_scanssd(pdf_xml_queue: Queue,
                            post_scanssd_queue: Queue,
                            sscraper_done: List[Event],
                            scanssd_done: Event,
                            config: Dict,
                            server_i: int):
    host = "localhost"
    cur_server_conf = config['servers']['scanssd'][server_i]
    cur_server_name = list(cur_server_conf.keys())[0]
    scanssd_port = cur_server_conf[cur_server_name]['ports']
    dpi = config['parameters']['dpi']
    conf = config['parameters']['conf']
    stride = config['parameters']['stride']
    print("here scanssd")
    loop = asyncio.get_event_loop()
    while not (all_true(sscraper_done) and pdf_xml_queue.empty()):
        try:
            cur_pdf_xmls: List[PdfXml] = pdf_xml_queue.get(timeout=1)
            post_scanssd_list = \
                loop.run_until_complete(
                    send_pdfs_to_scanssd(cur_pdf_xmls, host, scanssd_port, dpi, conf, stride))
            post_scanssd_queue.put(post_scanssd_list)
        except Empty:
            pass
        except Exception as e:
            logger.warning(e)

    scanssd_done.set()
    logger.info("Scanssd finished")


async def send_pdfs_to_scanssd(pdf_xmls: List[PdfXml], host, port, dpi, conf, stride):
    timeout = aiohttp.ClientTimeout(total=1200)
    session = aiohttp.ClientSession(timeout=timeout)
    coroutines = []
    for pdf in pdf_xmls:
        logger.info(f"sending {pdf.file_name} to scanssd for processing")
        url = f'http://{host}:{port}/predict/?dpi={dpi}&conf={conf}&stride={stride}'
        file = {"file": copy(pdf.pdf)}
        coroutines.append(session.post(url, data=file))
    responses = await asyncio.gather(*coroutines)

    post_scanssd_list = []
    for resp, pdf_xml in zip(responses, pdf_xmls):
        await check_status(resp, logger)
        content = await resp.content.read()
        post_scanssd_list.append(pdf_xml.to_post_scannssd(BytesIO(content)))
    await session.close()
    return post_scanssd_list
