from queue import Empty
from typing import List
from multiprocessing import Queue, Event
from modules.aiopipeline.workers.utils import all_true
from modules.protables.ProTables import *
from modules.aiopipeline.model.PostParser import PostParser
import modules.pipeline.msp_settings as MSP
import logging

logger = logging.getLogger('gen_html')


def save_and_generate_html(post_parser_queue: Queue,
                           parser_done: List[Event],
                           out_dir,
                           in_dir):

    conf = os.getenv("CONF", "0.5")
    xml_dir = osp.join(MSP.ROOT_DIR, out_dir, 'sscraper')
    csv_dir = osp.join(MSP.ROOT_DIR, out_dir, 'scanssd', 'SSD', f"conf_{conf}")
    tsv_dir = osp.join(MSP.ROOT_DIR, out_dir, 'qdgga-parser', 'output')
    while not (all_true(parser_done) and post_parser_queue.empty()):
        try:
            parsed: List[PostParser] = post_parser_queue.get(timeout=1)
            for p in parsed:
                logger.info(f"saving data for {p.file_name}")
                p.save(xml_dir=xml_dir, csv_dir=csv_dir, tsv_dir=tsv_dir)
        except Empty:
            pass
        except Exception as e:
            logger.warning(e)
