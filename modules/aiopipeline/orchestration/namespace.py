import os


def make_namespace(namespace) -> str:
    return namespace_template.format(namespace=namespace)


def create_namespace(namespace, spec_dir):
    os.makedirs(spec_dir, exist_ok=True)
    with open(os.path.join(spec_dir, f'{namespace}.yaml'), 'w+') as f:
        f.write(make_namespace(namespace))


namespace_template="""
kind: Namespace
apiVersion: v1
metadata:
  name: {namespace}
  labels:
    name: {namespace}
"""