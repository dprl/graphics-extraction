import logging
import os
from copy import copy
from io import BytesIO
from pydantic import BaseModel
logger = logging.getLogger("post_parser")


class PostParser(BaseModel):
    file_name: str
    num_pages: int
    pdf: BytesIO
    tsv: BytesIO
    region_tsv: BytesIO
    csv: BytesIO
    xml: BytesIO

    def save(self, xml_dir, tsv_dir, csv_dir):
        self.save_file(xml_dir, self.xml, ".xml")
        self.save_file(xml_dir, self.tsv, ".tsv", os.path.splitext(self.file_name)[0]+"_math_regions")
        self.save_file(tsv_dir, self.tsv, ".tsv", os.path.splitext(self.file_name)[0]+"-out")
        self.save_file(csv_dir, self.csv, ".csv")

    def save_file(self, file_dir, resource: BytesIO, ext, name=None):
        os.makedirs(file_dir, exist_ok=True)
        if name is None:
            name = os.path.splitext(self.file_name)[0]
        file_path = os.path.join(file_dir, name + ext)
        with open(file_path, 'wb') as f:
            f.write(copy(resource).read())

    class Config:
        arbitrary_types_allowed = True
