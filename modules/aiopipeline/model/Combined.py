import os
from io import BytesIO
from copy import copy
from pydantic import BaseModel

from modules.aiopipeline.model.PostParser import PostParser


class Combined(BaseModel):
    file_name: str
    num_pages: int
    pdf: BytesIO
    tsv: BytesIO
    region_tsv: BytesIO
    csv: BytesIO
    xml: BytesIO

    def to_post_parser(self, tsv: BytesIO) -> PostParser:
        return PostParser(
            file_name=self.file_name,
            pdf=self.pdf,
            tsv=tsv,
            region_tsv=self.region_tsv,
            csv=self.csv,
            xml=self.xml,
            num_pages=self.num_pages
        )

    def save_xml(self, xml_dir, name=None):
        os.makedirs(xml_dir, exist_ok=True)
        if name is None:
            name = os.path.splitext(self.file_name)[0]+".tsv"
        file_path=os.path.join(xml_dir, name)
        with open(file_path, 'wb') as f:
            f.write(copy(self.tsv).read())

    class Config:
        arbitrary_types_allowed = True
