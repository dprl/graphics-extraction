###########################################################################
#
# modules/chemscraper/conversions.py
#   CDXML_Soup_Container class + utility function and data for conversion
#
###########################################################################
# Revision History
# (Header added) Richard Zanibbi, Nov 13 2023 01:43:24 PM
###########################################################################


import os
import copy
import networkx as nx
import re
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF
from bs4 import BeautifulSoup as bsoup
from bs4 import Doctype
from bs4.formatter import XMLFormatter
import json

from modules.chemscraper.debug_fns import *
from modules.chemscraper.graph_conversions.rdkit_abbrevs import abbrev_lookup
from modules.chemscraper.parser_v2 import Parser_v2



################################################################
# Chemical Constants; Template CDXML
################################################################
ELEMENTS = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P',
            'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
            'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru',
            'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce',
            'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf',
            'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn',
            'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm',
            'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Uut',
            'Fl', 'Uup', 'Lv', 'Uus', 'Uuo'] 

ABBREVS = abbrev_lookup() # abbreviations
CHARGES = {"+", "-"}

# CDXML template
# Boilerplate / template for CDXML files (*load just once here)
# Create text string and bsoup tree for later use/reuse
SAMPLE_CDXML = 'modules/chemscraper/sample_mol.cdxml'
file_handle = open(SAMPLE_CDXML, 'r')
BASE_SOUP = bsoup( file_handle, 'xml' )
file_handle.close()


################################################################
# CDXML Output Utility Functions
################################################################


class UnsortedAttributes(XMLFormatter):
    # Used to keep attributes in the same order they were inputted
    # bsoup automatically alphabetizes
    def attributes(self, tag):
        for k, v in tag.attrs.items():
            yield k, v

def scale_bbox( bbox, scale_x, scale_y ):
        # RZ: Bounding box
        bbox[0] *= scale_x
        bbox[1] *= scale_y
        bbox[2] *= scale_x
        bbox[3] *= scale_y

        return bbox

def spaced_digits( dlist ):
    rounded_values = [ round(x, ndigits=2) for x in dlist ]
    #rounded_values = dlist
    digit_string = str(rounded_values)[1:-1].replace(',','')
    return digit_string

def bbox_string( G, n ):
    out_string="Undefined"
    if 'bbox' in G.nodes[n]:
        out_string = spaced_digits( G.nodes[n]['bbox'] )
    return out_string

def pos_string(G, n):
    # NOTE: created when it seemed things at left needed to move.
    # turns out the MarginWidth attribute (space around strings) and
    # bracket positining needed correction instead. 
    node_pos = G.nodes[n]['pos']
    out_string = spaced_digits( node_pos )
    return out_string

def point_size(G, n):
    point_size = 6  # minimum font size (hack)
    if 'font_size' in G.nodes[n]:
        point_size = G.nodes[n]['font_size']
    return point_size

##################################
# Chem sequence name functions
##################################

def is_known_sequence(sequence):
    #remove digits
    no_digits = ''.join(i for i in sequence if not i.isdigit())                
    for charge in CHARGES:
        no_digits = no_digits.replace(charge, "")
    if not re.match(r"(^[A-Z]([A-Za-z])*)$", no_digits):
        return False            
    elements = re.findall(r"[A-Z][a-z]*", no_digits)

    #elements = [s for s in re.split(r"\d+", sequence) if s]        
    for element in elements:
        if not element in ELEMENTS:
            return False
    
    return True

def get_sequence_hydrogens_and_charge( sequence ):
    # Count hydrogens from H_* tokens
    hydrogens = re.findall(r"H\d*", sequence)            
    total_hydrogens = 0
    for hydrogen in hydrogens:
        num = hydrogen[1:]
        if len(num)==0:
            total_hydrogens += 1 
        else:
            total_hydrogens += int(hydrogen[1:])  

    # Compute charge from +/- tokens
    pos_charges = re.findall(r"\+", sequence)
    neg_charges = re.findall(r"\-", sequence)
    count_pos_charges = len(pos_charges)
    count_neg_charges = len(neg_charges)

    count_charges = count_pos_charges - count_neg_charges

    return (total_hydrogens, count_charges)


def strip_hydrogens_charges( sequence ):
    stripped_string = Parser_v2.remove_hydrogen(sequence)
    for charge in CHARGES:
        stripped_string = stripped_string.replace(charge, "")

    return stripped_string



################################################################
#  CDXML Soup Container Class 
################################################################

class CDXML_Soup_Container(object):
    # Class/static method
    @staticmethod
    def prettify_cdxml(in_soup):
        mol_soup = in_soup.prettify(formatter=UnsortedAttributes())
        s_tag_corrected_soup = re.sub('(<s[^>]*>)\s*([^\s]+)\s*</s>','\\1\\2</s>', mol_soup)
        return s_tag_corrected_soup

    @staticmethod
    def has_bb(tag):
        return tag.has_attr('BoundingBox')

    @staticmethod
    def has_p(tag):
        return tag.has_attr('p')

    # Object methods
    def __init__(self, doc, save_dir, page_idx=0, no_id=0, page_length=1, cdxml_template_soup=BASE_SOUP, page_height=720) -> None:
        # Load the sample cdxml file and process it
        # For CDXML outer templates, many of these parameters will be ignored.
        self.doc = doc
        self.save_dir = save_dir
        self.page_idx = page_idx
        self.no_id = no_id

        self.page_length = page_length
        self.page_height=page_height

        self.soup = copy.deepcopy(cdxml_template_soup)
        self.soup.CDXML.page['HeightPages'] = str( page_length )

        #set version    
        root = os.getcwd()
        with open(os.path.join(root,".cz.json")) as f:
            config = json.load(f)     
        version = config["commitizen"]["version"]
        self.soup.CDXML['ChemScraperVersion'] = str( version )

    def __str__(self):
        return CDXML_Soup_Container.prettify_cdxml( self.soup )

    def str_fragment( self ):
        str_out = ''
        #print('CONTENTS', self.soup.CDXML.page.contents)
        #input('...')
        fragment_tags = self.soup.CDXML.page.contents
        for tag in fragment_tags:
            # Somehow newline is showing up as an entry.
            if tag != '\n':
                str_out +=  ''.join( CDXML_Soup_Container.prettify_cdxml(tag) )

        return str_out 

    def str_context( self ):
        # ASSUMES the container is only being used to hold CDXML and page tag atrributes
        # Return top/bottom text as a pair, to faciliate writing routines.
        soup_str = str(self)

        # Get lines, remove newlines
        lines = [ line for line in soup_str.split('\n') if line != '' ]
        cdxml_top = lines[1:len(lines) - 2]
        cdxml_bottom = lines[-2:]

        cdxml_top_str = '\n'.join(cdxml_top)
        cdxml_bottom_str = '\n' + '\n'.join(cdxml_bottom)

        return ( cdxml_top_str, cdxml_bottom_str )

    def fragments(self):
        return self.soup.page.contents 

    def add_fragments(self, tags, page_no = -1, page_height=0):
        # DEBUG (Hard won): extending with tag references REMOVES them from their source.
        #  Copying tags prevents removal from the source bsoup parse tree; copying list
        #  copies the tag references, and does not make a copy of individual tags.
        tag_copy_list = [ copy.copy(tag) for tag in tags ]

        self.soup.CDXML.page.extend( tag_copy_list )

    def write_cdxml(self):
        dir_name = self.save_dir
        out_soup = CDXML_Soup_Container.prettify_cdxml( self.soup )

        num_pdigits = len(str(self.page_idx+1))
        p_format = '0'*(3-num_pdigits)+str(self.page_idx+1)
        num_ndigits = len(str(self.no_id+1))
        n_format = '0'*(3-num_ndigits)+str(self.no_id+1)
        f_name = 'Page_' + p_format + '_No' + n_format + '.cdxml' 

        # DEBUG: Insure that subdirectory for document exists.
        if not os.path.isdir( dir_name ):
            os.mkdir( dir_name )
        out_file = os.path.join(dir_name, f_name )

        f = open(out_file, 'w')
        f.write(out_soup)
        f.close()

    def vertical_shift_for_multipage(self, reverse=False):
        # Provide offsets/reverse offsets when writing multiple pages.
        # 'True' matches any value for an attribute (here, p and BoundingBox)
        y_offset = self.page_idx * self.page_height
        if reverse:
            y_offset = -y_offset

        for tag in self.soup.find_all(p=True):
            position = tag['p']
            pcoords = [ float(coord) for coord in position.split() ]
            tag['p'] = spaced_digits( [ pcoords[0], pcoords[1] + y_offset ] )

        for tag in self.soup.find_all(BoundingBox=True):
            bb = tag['BoundingBox']
            bb_coords = [ float(coord) for coord in bb.split() ]
            tag['BoundingBox'] = spaced_digits( [ bb_coords[0], bb_coords[1] + y_offset, bb_coords[2], bb_coords[3] + y_offset ] )


    def write_cdxml_fragments_txt( self, dpr_cdxml_dict ):
        # Write page file using soup container text strings
        # self.page_length controls whether this is written as one file per page OR
        # all pages in a single CDXML document.

        # USES current container only as context (repeatedly)
        doc = self.doc
        dir_name = self.save_dir
        (cdxml_top_str, cdxml_bottom_str) = self.str_context()

        cdxml_fragment_txt = ''
        for page in dpr_cdxml_dict[doc]:

            # Collect strings from fragments in existing CDXML conversions
            for no_id in dpr_cdxml_dict[doc][page]:
                if self.page_length < 2:
                    cdxml_fragment_txt += dpr_cdxml_dict[doc][page][no_id].str_fragment()
                else:
                    # Shift coordinates, add fragment, then reverse coordinate shift.
                    dpr_cdxml_dict[doc][page][no_id].vertical_shift_for_multipage()
                    cdxml_fragment_txt += dpr_cdxml_dict[doc][page][no_id].str_fragment()
                    dpr_cdxml_dict[doc][page][no_id].vertical_shift_for_multipage(reverse=True)

            if self.page_length < 2:
                # Generate file name for page file, write file
                num_pdigits = len(str(page+1))
                p_format = '0'*(3-num_pdigits)+str(page+1)
                f_name = 'Page_' + p_format + '.cdxml'
                
                # Add header/footer to fragment CDXML
                file_str = cdxml_top_str + cdxml_fragment_txt + cdxml_bottom_str

                if not os.path.isdir( dir_name ):
                    os.mkdir( dir_name )
                out_file = os.path.join(dir_name, f_name )
                f = open(out_file, 'w')
                f.write(file_str)
                f.close()

                cdxml_fragment_txt = '' # reset file content string

        if self.page_length > 1: 
            f_name = doc + '.cdxml'

            # Add header/footer to fragment CDXML
            file_str = cdxml_top_str + cdxml_fragment_txt + cdxml_bottom_str

            # Write CDXML file for all pages.
            if not os.path.isdir( dir_name ):
                os.mkdir( dir_name )
            out_file = os.path.join(dir_name, f_name )
            f = open(out_file, 'w')
            f.write(file_str)
            f.close()


################################################################
# SVG generation and testing
################################################################
def render_pdf(in_dir, out_dir='data/svg2pdfs'):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    svgs = os.listdir(in_dir)
    f_names = [svg.split('.svg')[0] for svg in svgs]
    for idx, svg in enumerate(svgs):
        drawing = svg2rlg(os.path.join(in_dir, svg))
        renderPDF.drawToFile(drawing, os.path.join(out_dir, f_names[idx] + '.pdf'))


def add_label_cdxml(cdxml_path, out_path='data/mod_cdxmls'):
    in_file = open(cdxml_path, 'r')
    contents = in_file.read()
    in_file.close()
    soup = bsoup(contents, 'xml')

    # Find the max ID and Z to start the label process
    page_data = soup.find('page')
    fragments = page_data.find_all('fragment', recursive=False)
    max_id = 0
    max_z = 0
    for tag in fragments:
        try:
            t_id = tag['id']
            z_id = tag['Z']
            if int(t_id) > max_id:
                max_id = int(t_id)
            if int(z_id) > max_z:
                max_z = int(z_id)
        except KeyError:
            pass
    start_id = max_id + 1
    start_z = max_z + 1

    mins = []
    fragments = page_data.find_all('fragment', recursive=False)

    for mol in fragments:
        nodes = mol.find_all('n')
        min_x = 1000
        min_y = 1000
        for node in nodes:
            try:
                p = node['p'].split(' ')
                p = [float(p_x) for p_x in p]
                if p[0] < min_x:
                    min_x = p[0]
                if p[1] < min_y:
                    min_y = p[1]
            except KeyError:
                pass
        mins.append([min_x, min_y-15])


    # Create the captions
    for idx, min_p in enumerate(mins):
        new_t = soup.new_tag('t', id=str(start_id + (idx*2)),
                             p=str(min_p[0]) + ' ' + str(min_p[1]),
                             BoundingBox=str(min_p[0]) + ' '
                                        + str(min_p[1] - 10.96) + ' '
                                         + str(min_p[0] + 45.27) + ' '
                                         + str(min_p[1] + 4.89),
                             Z=str(start_z + idx),
                             LineHeight=str(16))
        new_s = soup.new_tag('s', font=str(20),
                             size=str(9.95),
                             color=str(4))

        new_s.string = 'Molecule ' + str(idx+1)
        new_t.append(new_s)
        soup.page.insert(-1,new_t)

    # soup.page.insert(0,cap_data)
    f = open(os.path.join(out_path,'mod_xml_tab10_2.cdxml'), 'w')
    f.write(str(soup))
    f.close()

################################################################
# Test
################################################################

if __name__ == '__main__':
    svg_dir = 'data/svg_files'
    # render_pdf(svg_dir)
    cd_path = 'Table_cdxml_files/Table_cdxml_files/LB-LJ-Tab10-e004-.cdxml'
    add_label_cdxml(cd_path)



