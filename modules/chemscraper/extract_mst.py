from debug_fns import *
from operator import itemgetter
import os
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.animation as animation
import cv2
from shapely.geometry import Polygon, Point
import copy
import math
from modules.chemscraper.nx_debug_fns import shapedraw,shapesdraw, gdraw
from scipy.spatial.distance import cdist
from shapely.geometry import LineString
from modules.chemscraper.geomtric_utils import GeometricToolbox
from modules.protables.util import OBJECT, REGION

from nx_debug_fns import *


class ExtractMST():

    def __init__(self, thresholds:dict):
        # SOLID_WEDGE_LONGEST_LENGTHS_DIFF_THRESHOLD = 0.1        
        self.LONGEST_LENGTHS_DIFF_RATIO = thresholds["LONGEST_LENGTHS_DIFF_RATIO"]        
        # SOLID_WEDGE_LONGEST_LENGTHS_RATIO_THRESHOLD = 0.2
        # SOLID_WEDGE_LONGEST_ANGLES_DIFF_THRESHOLD = [3, 25]
        # SOLID_WEDGE_MIN_AREA = 50 # RZ addition

        # For pruning angles between characters
        self.MIN_COS_PRUNE=thresholds["COS_PRUNE"]
        self.MAX_COS_PRUNE=1-thresholds["COS_PRUNE"]

        self.PARALLEL_TOLERANCE =  thresholds["PARALLEL_TOLERANCE"]  # 5 degree difference permitted for parallel lines (from AD)        
        self.paranthesis_labels = {"Par", "LP", "RP"}
        self.CHARGES = {"+", "-"}
        
        self.geo = GeometricToolbox(thresholds)
        # END_INTERSECT_THRESHOLD = .15 # difference between intersecting points and line
        # boundary should be less than this threshold percentage of line length
        # to be considered connected at the end

    def compute_graph_stats(self, graph):
        num_nodes = graph.number_of_nodes()
        num_edges = graph.number_of_edges()
        return {
            "num_nodes":num_nodes,
            "num_edges":num_edges
        }

    def euc_distance(self, box1, box2):
        box1_cx = (box1[2] + box1[0])/2
        box1_cy = (box1[3] + box1[1])/2
        box2_cx = (box2[2] + box2[0])/2
        box2_cy = (box2[3] + box2[1])/2

        box1_c = np.array([box1_cx, box1_cy])
        box2_c = np.array([box2_cx, box2_cy])

        dist = np.linalg.norm(box1_c - box2_c)
        return dist

    def regiondraw(self, region, pdf_obj=None, title=None, invert_y=True):
        plt.figure()
        if invert_y:
            # ax.set_ylim(ax.get_ylim()[::-1])
            plt.gca().invert_yaxis()
            if title:
                title += '\n(*Y axis inverted)'
            else:
                title = '(*Y axis inverted)'

        # plt.axis("off")
        # Include title if requested
        if title:
            plt.title( title )

        for _, obj in enumerate(region):
            obj_type = obj[5]
            if obj_type == "S":
                obj_type = "Char"

            obj_label = obj[4]
            obj_properties = obj[6]

            # Get properties
            polygon = obj_properties.get("polygon", None)
            linestring = obj_properties.get("linestring", None)
                
            obj_label = self.get_label_class(obj_label, obj_type, obj_properties)
            _, shape_obj = self.get_shape(obj_label, polygon, linestring)

            if shape_obj.geom_type == "LineString":
                plt.plot(*shape_obj.coords.xy)
            else:
                plt.plot(*shape_obj.exterior.coords.xy)
            
        # Display or save plot as requested
        if not pdf_obj:
            plt.show(block=False)
            plt.pause(0.001)
        else:
            plt.savefig( pdf_obj, format="pdf")

    def get_coords(self, shape):
        if shape.geom_type == "LineString" or shape.geom_type == "Point":
            return shape.coords
        else:
            return shape.exterior.coords

    def create_node(self, obj_properties, obj_label, obj_type, obj_id,
                    lbl_type, g_dict):
        # Get properties
        box = obj_properties.get("box", None)
        polygon = obj_properties.get("polygon", None)
        filled = obj_properties.get("filled", None)
        linestring = obj_properties.get("linestring", None)

        # Properties of constituent graphics
        points = obj_properties.get("points", [])
        bezierPoints = obj_properties.get("bezierPoints", [])
        lineWidth = obj_properties.get("lineWidth", [])
        angle = obj_properties.get("angle", [])
        length = obj_properties.get("length", [])
        curvedTo = obj_properties.get("curvedTo", [])
        lengthSegments = obj_properties.get("lengthSegments", [])
        isLine = obj_properties.get("isLine", [])
        
        if obj_label in self.CHARGES:
            obj_type = "Charge"
            lbl_type = obj_label
            
        shape, shape_obj = self.get_shape(obj_label, polygon, linestring)

        paranthesis = False
        if obj_label in self.paranthesis_labels:
            paranthesis = True
        assert(obj_label)

        if obj_label not in g_dict:
            g_dict[obj_label] = 1
        else:
            g_dict[obj_label] += 1
        
        node_dict = {"poly": polygon,
                    "box": box,
                    "lbl_type": lbl_type,
                    "lbl_cls": obj_label,
                    "points": points,
                    "filled": filled,
                    "linestring": linestring,
                    "bezierPoints": bezierPoints,
                    "lineWidth": lineWidth,
                    "angle": angle,
                    "length": length,
                    "curvedTo": curvedTo,
                    "lengthSegments": lengthSegments,
                    "isLine": isLine,
                    "shape": shape,
                    "shape_obj": shape_obj,
                    "paranthesis": paranthesis,
                    "obj_type": obj_type,
                    "obj_id": [obj_id] 
                }

        node_name = obj_label + "_" + str(g_dict[obj_label])
        # if page == 8 and region_id == 14 and obj_id==17:
        #     shapedraw(shape_obj)
        return node_name, node_dict, g_dict

    def get_obj_attrs(self, obj):
        obj_type = obj[OBJECT.TYPE.value]
        if obj_type == "S":
            obj_type = "Char"
        assert (obj_type == "GC" or "G" or "Char")
        # lbl_type = Actual character for Symbols (S) - e.g. 'H'
        # lbl_type = collection of graphics for graphics - e.g. 'line line'
        lbl_type = obj[OBJECT.LABEL.value]
        obj_properties = obj[OBJECT.SHAPE_INFO.value]
        obj_label = self.get_label_class(lbl_type, obj_type, obj_properties)
        return obj_type, obj_label, lbl_type, obj_properties

    def extract_mst_poly(self, regionProTable):
        page_region_msts = {}
        page_region_ingraphs = {}
        page_region_bboxes = {}
        page_region_mst_stats = {}
        g_dict = {}

        g_dict = {}

        for page, page_regions in enumerate(regionProTable):
            region_msts = []
            region_ingraphs = []
            region_bboxes = []
            region_mst_stats= []
            
            for region_id, region in enumerate(page_regions):
                region_bbox = region[REGION.BBOX.value]
                objects = region[REGION.OBJECTS.value]
                mst = None
                in_graph = None
                mst_stats = None

                if len(objects):
                    g_dict = {}
                    nodes_list = []
                    in_graph = nx.Graph()
                    # if (page == 2 and region_id == 15):
                    #     regiondraw(region[1])
                    for obj_id, obj in enumerate(objects):
                        obj_type, obj_label, lbl_type, obj_properties = self.get_obj_attrs(obj)
                        node_name, node_dict, g_dict = self.create_node(obj_properties, obj_label, 
                                                                        obj_type, obj_id, lbl_type, 
                                                                        g_dict)
                        nodes_list.append((node_name, node_dict))
                    in_graph.add_nodes_from(nodes_list)
                    # gdraw(in_graph)

                    # Construct fully-connected edges and find MST
                    # nodes = sorted(list(in_graph.nodes()))
                    nodes = list(in_graph.nodes())
                    for i, node1 in enumerate(nodes):
                        node1_shape = in_graph.nodes[node1]["shape_obj"]
                        for node2 in nodes[i+1:]:
                            node2_shape = in_graph.nodes[node2]["shape_obj"]
                            ln_intersect = False

                            # Use min distance among distances between all pairs of end
                            # points between two nodes, if both are lines
                            # TODO: maybe also add for wedges
                            if "Ln" in node1 and "Ln" in node2:
                                boundary_points1 = list(self.get_coords(node1_shape))
                                boundary_points2 = list(self.get_coords(node2_shape))
                                distance = cdist(boundary_points1,
                                                boundary_points2).min()
                                # distance = node1_shape.distance(node2_shape)
                                # Boolean attribute set to True if 2 lines intersect,
                                # used later during close edges
                                try:                   
                                    ln_intersect = node1_shape.intersects(node2_shape)                            
                                except:
                                    pass
                            # Otherwise, use normal distance
                            else:
                                distance = node1_shape.distance(node2_shape)

                            ##################################
                            # Avoid characters merging at
                            # unusual angles
                            ##################################
                            if in_graph.nodes[node1]['obj_type'] == 'Char' and \
                                    in_graph.nodes[node2]['obj_type'] == 'Char':
                                # Compute angle between centroids
                                #pcheck('Labels', (node1, node2))
                                poly1 =  in_graph.nodes[node1]['poly']
                                poly2 =  in_graph.nodes[node2]['poly']

                                node1_c = poly1.centroid
                                node2_c = poly2.centroid
                                y = node1_c.y - node2_c.y
                                x = node1_c.x - node2_c.x
                                abs_cos_angle = abs( math.cos( math.atan2(y, x) ) )

                                # Prune if angle is 'too sharp'
                                if '(' not in node1 and ')' not in node1 and \
                                    '(' not in node2 and ')' not in node2 and \
                                        abs_cos_angle > self.MIN_COS_PRUNE and \
                                            abs_cos_angle < self.MAX_COS_PRUNE:
                                    distance = math.inf

                            in_graph.add_edge(node1, node2, weight=distance,
                                            ln_intersect=ln_intersect)
                    # Construct MST
                    mst = nx.minimum_spanning_tree(in_graph)
                    mst_stats = self.compute_graph_stats(mst)

                region_msts.append(mst)
                region_ingraphs.append(in_graph)
                region_bboxes.append(region_bbox)
                region_mst_stats.append(mst_stats)
            
            if len(region_msts) > 0:
                page_region_msts[page] = region_msts
                page_region_ingraphs[page] = region_ingraphs
                page_region_bboxes[page] = region_bboxes
                page_region_mst_stats[page] = region_mst_stats
        return page_region_msts, page_region_ingraphs, page_region_bboxes, page_region_mst_stats, g_dict

    def get_shape(self, obj_label, poly, linestring):
        if linestring:
            shape = "linestring"
            shape_obj = linestring
        elif poly:
            shape = "polygon"
            shape_obj = poly
        else:
            print("No shape available")
            shape = None
            shape_obj = None
        # For wave, use polygon for rendering purpose
        if obj_label == "Wave":
            shape = "polygon"
            shape_obj = poly
        return shape, shape_obj

    def get_node_shape(self, graph, node):
        node_poly = graph.nodes[node]['poly']
        node_linestring = graph.nodes[node]['linestring']
        if node_poly:
            node_shape = node_poly
        elif node_linestring:
            node_shape = node_linestring
        else:
            print("No shape available")
            node_shape = None
        return node_shape

    def get_label_class(self, obj_label, obj_type, obj_properties):
        if obj_type == "Char":
            return obj_label

        isLine = obj_properties.get("isLine", None)
        
        if isLine:        
            # TODO: Based on assumption that solid wedges and other wedges are not
            # approximated as line in JSON (Check with Bryan)
            return "Ln"

        if 'curve' in obj_label:
            curvedTo = obj_properties.get("curvedTo", None)
            if self.is_bracket(obj_label):
                bracket = self.get_bracket_type(curvedTo[0])
                return bracket

            linestring = obj_properties.get("linestring", None)
            curve_type = self.get_curve_type(obj_label, curvedTo, linestring)
            # print(curve_type)
            return curve_type
        
        elif 'line' in obj_label:
            # return get_line_bond_type(obj_properties)
            if self.check_plus_charge(obj_label, obj_properties):
                return "+"
            if self.check_solid_wedge(obj_label, obj_properties):
                # import pdb;pdb.set_trace()
                # check_solid_wedge(obj_label, obj_properties)
                # print("Solid wedge")
                return "SW"
            if self.check_hashed_wedge(obj_label, obj_properties):
                # print("Hashed wedge")
                return "HW"
            return 'Ln'

        elif 'rectangle' in obj_label:
            return 'Ln'

        else:
            return obj_label


    def check_plus_charge(self, obj_label, obj_properties):
        obj_label_list = obj_label.split(" ")
        obj_label_set = set(obj_label_list)
        # Must consist only of lines
        if obj_label_set != {"line"}:
            return False
        # Must have 2 or more lines
        if len(obj_label_list) < 2:
            return False
        # Probably a minus
        if len(obj_label_list) == 3 or len(obj_label_list) == 4:
            return False

        # Must be filled
        filled = obj_properties.get("filled", None)
        if not filled:
            return False

        # Lines must be perpendicular
        angles = obj_properties.get("angle", None)
        if not angles:
            return False
        else:
            angles = np.sort(np.unique((np.array(angles))))
            if len(angles) != 2:
                return False
            perpendicular = np.array([0, 90])
            if not np.allclose(angles, perpendicular, atol=self.PARALLEL_TOLERANCE):
                # print(angles)
                return False

        return True

    def is_bracket(self, obj_label):
        # Must consist of only 1 curve
        return obj_label == "curve"

    def get_bracket_type(self, curvedTo):
        if not curvedTo:
            return "Par"
        elif curvedTo == "left":
            return "LP"
        elif curvedTo == "right":
            return "RP"
        else:
            return "Par"

    def get_curve_type(self, obj_label, curvedTo, linestring):
        if not curvedTo:
            return obj_label
        curvedTo_set = set(curvedTo)

        # NOTE
        # the len(curvedTo_set) constraints don't give much insight into the type of curve we are looking at.
        # It's not uncommon to see wavy bonds with 4 directions or circles with 2 or vice versa.
        # 
        # However, circles should always be closed while wavy bonds should not.

        if len(curvedTo_set) >= 2 and linestring.is_closed:
            return "Circle"
        
        # if len(curvedTo_set) <= 2 and not linestring.is_closed:
        if not linestring.is_closed:
            return "Wave"
        
        return obj_label

    def get_midpoint(self, x1,y1,x2,y2) -> Point:
        return Point((0.5*(x1+x2),0.5*(y1+y2)))

    def get_longest_pair_polygon(self, polygon):
        coords = polygon.exterior.coords
        
        line_list = []
        
        for idx in range(len(coords)-1):
            new_line = LineString([coords[idx], coords[idx+1]])
            line_list.append(new_line)
        line_list = sorted(line_list, key=lambda x: -x.length)
        
        return [line_list[0],line_list[1]]
        

    def are_lines_opening(self, line1, line2):
        p1l1 = Point(line1.coords[0])
        p1l2 = Point(line1.coords[1])
        p2l1 = Point(line2.coords[0])
        p2l2 = Point(line2.coords[1])

        midpoint_l1 = self.get_midpoint(p1l1.x, p1l1.y, p2l1.x, p2l1.y)
        midpoint_l2 = self.get_midpoint(p1l2.x, p1l2.y, p2l2.x, p2l2.y) 
        
        if p1l1.distance(p2l1)<p1l1.distance(p2l2):
            return abs(round(p1l1.distance(p2l1),2)-round(midpoint_l1.distance(midpoint_l2),2))>0
        else:
            return abs(round(p1l1.distance(p2l2),2)-round(midpoint_l1.distance(midpoint_l2),2))>0


    def check_solid_wedge(self, obj_label, obj_properties):
        # Collect properties
        obj_label_list = obj_label.split(" ")
        obj_label_set = set(obj_label_list)
        filled = obj_properties.get("filled", None)
        poly = obj_properties.get("polygon", None)
        line_lengths = obj_properties.get("length", None)

        # Must consist only of lines, and have 3 or more lines.
        # ALSO Must be a filled polygon that is closed.
        if obj_label_set != {"line"} or \
                len(obj_label_list) < 3 or \
                not filled or \
                not poly or \
                not poly.is_closed or \
                not line_lengths:
            return False
        
        longest, second_longest = self.get_longest_pair_polygon(obj_properties["polygon"])
        
        diff_ratio = second_longest.length / longest.length# close to 1 are similar

        are_parallel = self.geo.are_lines_parallel(longest, second_longest, self.PARALLEL_TOLERANCE)
        
        # import pdb;pdb.set_trace()
        if diff_ratio > self.LONGEST_LENGTHS_DIFF_RATIO and not are_parallel:
            return True
        return False
        
    def check_hashed_wedge(self, obj_label, obj_properties):
        obj_label_list = obj_label.split(" ")
        obj_label_set = set(obj_label_list)
        # Must consist only of lines
        if obj_label_set != {"line"}:
            return False
        # Must have 3 or more lines
        if len(obj_label_list) < 3:
            return False
        # Must not be filled
        filled = obj_properties.get("filled", None)
        if filled:
            return False

        # All lines must be parallel
        angles = obj_properties.get("angle", None)
        if not angles:
            return False
        else:
            angles = np.array(angles)
            mean_angle = angles.mean()
            if not np.allclose(angles, mean_angle, atol=self.PARALLEL_TOLERANCE):
                # print(angles)
                return False

        # Lengths must be in an increasing or decreasing order
        line_lengths = obj_properties.get("length", None)
        if not line_lengths:
            return False
        else:
            line_lengths = np.array(line_lengths)
            if not ((np.all(line_lengths[:-1] < line_lengths[1:])) or
                    (np.all(line_lengths[:-1] > line_lengths[1:]))):
                return False
        return True

    def compute_max_side(self, poly):
        x,y = poly.exterior.coords.xy
        edge_distances = []
        vert_ids = []
        # Calculate all the possible vertex distances and then take the max side
        for i in range(len(x)-1):
            edg_len = Point(x[i],y[i]).distance(Point(x[i+1],y[i+1]))
            edge_distances.append(edg_len)
            vert_ids.append([i,i+1])

        max_edge_len = max(edge_distances)
        max_edge_len_id = edge_distances.index(max_edge_len)
        max_vert_ids = vert_ids[max_edge_len_id]
        max_vert1 = [x[max_vert_ids[0]],y[max_vert_ids[0]]]
        max_vert2 = [x[max_vert_ids[1]],y[max_vert_ids[1]]]
        max_verts = [max_vert1, max_vert2]
        
        return max_edge_len, max_verts
        
        
    def compute_hashed_wedge(self, mst):
        visited_nodes = []
        cur_lengths = []
        cur_nodes = []
        cur_verts = []
        hw_found = False
        cur_node = list(mst.nodes())[0]
        while len(visited_nodes) < len(mst.nodes()):
            if 'line' in mst.nodes[cur_node]['lbl_type'] or 'rectangle' in mst.nodes[cur_node]['lbl_type']:
                cur_length, cur_vert = self.compute_max_side(mst.nodes[cur_node]['poly'])
                cur_lengths.append(cur_length)
                cur_nodes.append(cur_node)
                cur_verts.append(cur_vert)
                nei_iter = mst.neighbors(cur_node)
                valid_neighs = [n for n in nei_iter if n not in visited_nodes and 
                                ('line' in mst.nodes[n]['lbl_type'] or 
                                'rectangle' in mst.nodes[n]['lbl_type'])]
                if not len(valid_neighs):
                    if len(cur_nodes) > 2:
                        hw_found = True
                        break
                    else:
                        visited_nodes.append(cur_node)
                        for node in mst.nodes():
                            if node not in visited_nodes:
                                cur_node = node
                            break
                        break
                else:
                    nxt_node = None
                    for valid_n in valid_neighs:
                        side_length, _ = self.compute_max_side(mst.nodes[valid_n]['poly'])
                        if abs(side_length - cur_length) > 2:
                            nxt_node = valid_n
                            break
                    visited_nodes.append(cur_node)
                    if nxt_node != None:
                        cur_node = nxt_node
                    else:
                        cur_node = valid_neighs[0]
                        cur_nodes = []
                        cur_lengths = []
                        cur_verts = []
            else:
                nxt_found = False
                visited_nodes.append(cur_node)
                cur_nodes = []
                cur_lengths = []
                cur_verts = []
                for node in mst.nodes():
                    if node not in visited_nodes:
                        cur_node = node
                        nxt_found = True
                    break
                if not nxt_found:
                    break
        
        # If a H Wedge has been detected, restructure the MST
        if hw_found:
            cur_nodes = np.array(cur_nodes, dtype=object)
            cur_lengths = np.array(cur_lengths)
            cur_verts = np.array(cur_verts).reshape((-1,2,2))
            sort_id = np.argsort(cur_lengths)
            sorted_nodes = cur_nodes[sort_id]
            sorted_verts = cur_verts[sort_id]

            # Find a neighbor of the smallest line
            touch_nei = None
            tiny_nei_iter = mst.neighbors(sorted_nodes[0])
            for n in tiny_nei_iter:
                if n not in sorted_nodes and ('line' in mst.nodes[n]['lbl_type'] or 
                                'rectangle' in mst.nodes[n]['lbl_type']):
                    touch_nei = n
                    break
            if touch_nei != None:
                # Create a polygon out of the lines:
                vert1, vert2 = sorted_verts[0,0], sorted_verts[0,1]
                vert3, vert4 = sorted_verts[-1,0], sorted_verts[-1,1]
                new_poly = Polygon([vert1, vert2, vert3, vert4])
                # Remove all the nodes for constituting the H Wedge
                for node in sorted_nodes:
                    mst.remove_node(node)
                # Create the new node
                mst.add_node('HW_1', poly=new_poly, lbl_type='-HashedWedge-', cls='HW')
                # Add the edge
                mst.add_edge(touch_nei, 'HW_1', weight=0.0)

        return mst


    def id_graphics(self, cluster_graphics):
        char_cnt_dict = {}
        new_list = []
        visited_chars = []

        for graphics in cluster_graphics:
            if not graphics.split('_')[0] == 'Ln':
                if graphics not in visited_chars:
                    new_list.append(graphics+'_'+str(1))
                    visited_chars.append(graphics)
                    char_cnt_dict[graphics] = 1
                else:
                    new_list.append(graphics+'_'+str(char_cnt_dict[graphics]+1))
                    char_cnt_dict[graphics] += 1
            else:
                new_list.append(graphics)
        return new_list

    def view_msts(self, full_gs, ss_msts, ss_poly_msts, cd_page_ids):
        t_id = 2
        p_id = 4
        all_gs = np.array(full_gs, dtype=object)
        ss_msts = np.array(ss_msts, dtype=object)
        ss_poly_msts = np.array(ss_poly_msts, dtype=object)
        cd_page_ids = np.array(cd_page_ids)
        p_ids = np.where(cd_page_ids == t_id)[0]
        p_gs = all_gs[p_ids]
        p_msts = ss_msts[p_ids]
        p_poly_msts = ss_poly_msts[p_ids]
        cdxml_g = p_gs[p_id]
        ss_g = p_msts[p_id]
        ss_poly_g = p_poly_msts[p_id]
        
        fig, (ax1, ax2, ax3) = plt.subplots(1,3)
        # Draw CDXML Graph
        self.draw_graph(cdxml_g, ax1)
        # Draw SS MST Graph
        self.draw_graph(ss_g, ax2)
        # Draw SS Poly MST Graph
        self.draw_graph(ss_poly_g, ax3)
        ax1.title.set_text('CDXML Graph')
        ax2.title.set_text('SS MST Graph (Boxes)')
        ax3.title.set_text('SS MST Graph (Polygons)')
        plt.axis('tight')
        plt.axis('off')
        # plt.savefig('Typo_graph.svg', format='svg', bbox_inches='tight')
        plt.show()
        plt.close()

    def draw_graph(self, g, axs):    
        pos = nx.kamada_kawai_layout(g, scale=1)

        # Remove Node and cluster IDS for visualization
        dup_check = []
        node_names = []
        ln_cnt = 1
        for idx, node in enumerate(g.nodes()):
            char = node.split('_')[0]
            if char == 'Ln':
                node_names.append(char+'_'+str(ln_cnt))
                ln_cnt += 1
            else:
                if char not in dup_check:
                    node_names.append(char)
                    dup_check.append(char)
                else:
                    node_names.append(char+'_'+str(idx+1))

        
        nx.draw(g, pos, edge_color='black', width=3, linewidths=3,
                node_size=500, node_color='pink', alpha=0.9,
                labels={node: node_names[idx] for idx, node in enumerate(g.nodes())}, ax=axs)


    def viz_polys(self, cd_page_ids, pdf_ids, all_pages_boxes, all_page_objs, img_dir):
        t_id = 2
        p_id = 4

        img = cv2.imread(os.path.join(img_dir, str(t_id+3)+'.png'),1)
        cd_page_ids = np.array(cd_page_ids)
        p_ids = np.where(cd_page_ids == t_id)[0]
        pdf_ids = np.array(pdf_ids)
        pdf_matches = pdf_ids[p_ids]
        page_id = pdf_matches[p_id][0]
        cluster_id = pdf_matches[p_id][1]

        min_x, min_y, max_x, max_y = all_pages_boxes[page_id][cluster_id]
        pad = 15
        mol_img = img[min_y-pad:max_y+pad, min_x-pad:max_x+pad]
        scale = 3
        width = int(mol_img.shape[1] * scale)
        height = int(mol_img.shape[0] * scale)
        dim = (width, height)
        mol_img = cv2.resize(mol_img, dim, interpolation=cv2.INTER_AREA)
        x_offset = min_x - pad
        y_offset = min_y - pad
        plt.imshow(mol_img)

        cluster_objs = all_page_objs[page_id][cluster_id]
        for obj in cluster_objs:
            poly = obj.poly
            g_type = obj.g_type
            # box = obj.box
            # lbl = obj.lbl_type
            x,y = poly.exterior.xy
            # print(x,y,box,lbl)
            x = (x-x_offset)*scale 
            y = (y-y_offset)*scale
            # print(x,y)
            if g_type == 'char':
                plt.plot(x,y, 'r-')
            else:
                plt.plot(x,y, 'g-')
            plt.plot(x,y, 'b.')

        plt.show()
        exit(0)


    ## Function to visualize the merges for MST graphs overlaid on the molecule
    def view_overlay(self, all_pages_boxes, pdf_ids, ss_msts, in_g_box, ss_poly_msts, in_g_poly, 
                        cd_page_ids, img_dir):
        t_id = 2
        p_id = 4

        img = cv2.imread(os.path.join(img_dir, str(t_id+3)+'.png'),1)
        cd_page_ids = np.array(cd_page_ids)
        p_ids = np.where(cd_page_ids == t_id)[0]
        pdf_ids = np.array(pdf_ids)
        ss_msts = np.array(ss_msts, dtype=object)
        in_g_box = np.array(in_g_box, dtype=object)
        ss_poly_msts = np.array(ss_poly_msts, dtype=object)
        in_g_poly = np.array(in_g_poly, dtype=object)
        pdf_matches = pdf_ids[p_ids]
        page_id = pdf_matches[p_id][0]
        cluster_id = pdf_matches[p_id][1]

        min_x, min_y, max_x, max_y = all_pages_boxes[page_id][cluster_id]
        pad = 15
        mol_img = img[min_y-pad:max_y+pad, min_x-pad:max_x+pad]
        scale = 3
        width = int(mol_img.shape[1] * scale)
        height = int(mol_img.shape[0] * scale)
        dim = (width, height)
        mol_img = cv2.resize(mol_img, dim, interpolation=cv2.INTER_AREA)
        x_offset = min_x - pad
        y_offset = min_y - pad
        
        fig, (ax1, ax2) = plt.subplots(1,2)
        ax1.imshow(mol_img)
        ax2.imshow(mol_img)

        p_msts = ss_msts[p_ids]
        p_in_box_gs = in_g_box[p_ids]
        p_poly_msts = ss_poly_msts[p_ids]
        p_in_poly_gs = in_g_poly[p_ids]

        c_mst = p_msts[p_id]
        c_in_box_g = p_in_box_gs[p_id]
        c_poly_mst = p_poly_msts[p_id]
        c_in_poly_g = p_in_poly_gs[p_id]

        opacity = {'line': 0.6, 'centroid': 0.5}
        colors = {'general': '#ff0000', 'curve': '#bb7b4f', 'end_pt': '#0000ff', 
                    'end_pt_leaf': '#00ff00'}
        # Draw the Merges for Box MST
        self.draw_merge_overlay(c_mst, c_in_box_g, x_offset, y_offset, scale, ax1, colors, opacity)
        # Draw the Merges for Polygon MST
        self.draw_merge_overlay(c_poly_mst, c_in_poly_g, x_offset, y_offset, scale, ax2, colors, opacity)

        ax1.title.set_text('Merges using Boxes')
        ax2.title.set_text('Merges using Polygons')
        plt.axis('tight')
        # plt.axis('off')
        plt.show()
        plt.close()


    def draw_merge_overlay(self, mst, in_g, x_off, y_off, scale, ax, colors, opacity):
        
        visited_nodes = []
        for edge in mst.edges():
            poly_e1 = in_g.nodes[edge[0]]['poly']
            poly_e2 = in_g.nodes[edge[1]]['poly']
            cent_e1 = list(poly_e1.centroid.coords[0])
            cent_e2 = list(poly_e2.centroid.coords[0])
            # Check whether any of the node is a leaf node and set color for each node
            if mst.degree[edge[0]] == 1:
                colr1 = colors['end_pt_leaf']
            else:
                colr1 = colors['end_pt']
            if mst.degree[edge[1]] == 1:
                colr2 = colors['end_pt_leaf']
            else:
                colr2 = colors['end_pt']
            # Scale the points and adjust with the offset for the molecule excerpt
            # from the original image
            cent_e1[0], cent_e1[1] = (cent_e1[0]-x_off)*scale, (cent_e1[1]-y_off)*scale
            cent_e2[0], cent_e2[1] = (cent_e2[0]-x_off)*scale, (cent_e2[1]-y_off)*scale
            x = [cent_e1[0], cent_e2[0]]
            y = [cent_e1[1], cent_e2[1]]
            x1, y1 = [cent_e1[0]], [cent_e1[1]]
            x2, y2 = [cent_e2[0]], [cent_e2[1]]
            lbl_e1 = in_g.nodes[edge[0]]['lbl_type']
            lbl_e2 = in_g.nodes[edge[1]]['lbl_type']
            # Select line color based on node types
            if 'curve' in lbl_e1 or 'curve' in lbl_e2:
                color = colors['curve']
            else:
                color = colors['general']
            ax.plot(x,y, '-', color=color, linewidth=2, alpha=opacity['line'])
            # Draw the blobs if not already visited
            if edge[0] not in visited_nodes:
                ax.plot(x1,y1, '.', color=colr1, markersize=15, alpha=opacity['centroid'])
                visited_nodes.append(edge[0])
            if edge[1] not in visited_nodes:
                ax.plot(x2,y2, '.', color=colr2, markersize=15, alpha=opacity['centroid'])
                visited_nodes.append(edge[1])


    def create_poly_animation(self, all_pages_boxes, pdf_ids, cd_page_ids, ss_poly_msts, in_g_poly, img_dir):
        t_id = 10
        p_id = 2

        img = cv2.imread(os.path.join(img_dir, str(t_id+3)+'.png'),1)
        cd_page_ids = np.array(cd_page_ids)
        p_ids = np.where(cd_page_ids == t_id)[0]
        pdf_ids = np.array(pdf_ids)
        ss_poly_msts = np.array(ss_poly_msts, dtype=object)
        in_g_poly = np.array(in_g_poly, dtype=object)
        pdf_matches = pdf_ids[p_ids]
        page_id = pdf_matches[p_id][0]
        cluster_id = pdf_matches[p_id][1]

        min_x, min_y, max_x, max_y = all_pages_boxes[page_id][cluster_id]
        pad = 15
        mol_img = img[min_y-pad:max_y+pad, min_x-pad:max_x+pad]
        scale = 3
        width = int(mol_img.shape[1] * scale)
        height = int(mol_img.shape[0] * scale)
        dim = (width, height)
        mol_img = cv2.resize(mol_img, dim, interpolation=cv2.INTER_AREA)
        mol_img_cpy = copy.deepcopy(mol_img)
        x_offset = min_x - pad
        y_offset = min_y - pad

        p_poly_msts = ss_poly_msts[p_ids]
        p_in_poly_gs = in_g_poly[p_ids]
        c_poly_mst = p_poly_msts[p_id]
        c_in_poly_g = p_in_poly_gs[p_id]

        edges = []
        weights = []
        # Find the order of the tree construction
        # Sort the edges by weight first (get the weight)
        for edge_meta in c_poly_mst.edges(data=True):
            edges.append([edge_meta[0], edge_meta[1]])
            weights.append(edge_meta[2]['weight'])
        edges = np.array(edges, dtype=object)    
        weights = np.array(weights)
        sort_idxs = np.argsort(weights)
        sorted_edges = edges[sort_idxs]
        sorted_weights = weights[sort_idxs]

        # Create extended animation
        self.extended_animation(mol_img, sorted_edges, sorted_weights, c_poly_mst, c_in_poly_g, 
                            x_offset, y_offset, scale, t_id, p_id, dim)
        # Create basic animation
        self.basic_animation(mol_img_cpy, sorted_edges, sorted_weights, c_poly_mst, c_in_poly_g, 
                            x_offset, y_offset, scale, t_id, p_id, dim)

    def extended_animation(self, mol_img, sorted_edges, sorted_weights, c_poly_mst, c_in_poly_g, 
                            x_offset, y_offset, scale, t_id, p_id, dim):
        frames = [copy.deepcopy(mol_img)]
        color_blocks = {0:(255,0,0), 1:(0,255,0)}
        visited_nodes = []
        valid_edges = np.zeros(len(sorted_edges)).astype(np.int)
        parent_edges = {n:0 for n in c_poly_mst.nodes()}
        tot_edge_frames = len(sorted_edges)

        prev_nodes = [sorted_edges[0,0], sorted_edges[0,1]]
        count = 0
        while (count < tot_edge_frames):
            # Get the min distances first
            cur_valid_edges = [idx for idx, val in enumerate(valid_edges) if val == 0]
            cur_min = np.min(sorted_weights[cur_valid_edges])
            # Get all the Nodes for min weight
            min_ids = np.where(sorted_weights == cur_min)[0]
            visited_min_ids = np.zeros(len(min_ids))
            iters = 0

            while iters < len(min_ids):
                found_prev = False
                for idx_min, min_id in enumerate(min_ids):
                    if visited_min_ids[idx_min] == 0:
                        if sorted_edges[min_id,0] in prev_nodes or \
                                sorted_edges[min_id,1] in prev_nodes:
                            cur_edge = sorted_edges[min_id]
                            if cur_edge[0] in prev_nodes:
                                ord_nodes = [cur_edge[0],cur_edge[1]]
                            else:
                                ord_nodes = [cur_edge[1],cur_edge[0]]
                            # Check if both nodes already in tree (both in blue)
                            if cur_edge[0] in visited_nodes and cur_edge[1] in visited_nodes:
                                true_clr_idx = [0,0]
                                cond = True
                            else:
                                true_clr_idx = [0,1]
                                cond = False
                            for idx_n, node in enumerate(ord_nodes):
                                poly = c_in_poly_g.nodes[node]['poly']
                                points = np.array(poly.exterior.coords, dtype=np.int).reshape((-1,2))
                                points[:,0] = (points[:,0]-x_offset)*scale 
                                points[:,1] = (points[:,1]-y_offset)*scale
                                points = points.reshape((-1,1,2))
                                mol_img = cv2.polylines(mol_img, [points], isClosed=True, 
                                                            color=color_blocks[true_clr_idx[idx_n]], 
                                                            thickness=2+(parent_edges[node]*4))
                                frames.append(copy.deepcopy(mol_img))
                                visited_nodes.append(node)
                                if cond:
                                    parent_edges[node] += 1
                                elif idx_n == 0:
                                    parent_edges[node] += 1
                            prev_nodes = [cur_edge[0], cur_edge[1]]
                            visited_min_ids[idx_min] = 1
                            found_prev = True
                            break
                # If still no node found existing, add the closest
                if not found_prev:
                    for idx_min, min_id in enumerate(min_ids):
                        if visited_min_ids[idx_min] == 0:
                            cur_edge = sorted_edges[min_id]
                            if cur_edge[0] in visited_nodes:
                                ord_nodes = [cur_edge[0], cur_edge[1]]
                            else:
                                ord_nodes = [cur_edge[1], cur_edge[0]]
                            # Check if both nodes already in tree (both in blue)
                            if cur_edge[0] in visited_nodes and cur_edge[1] in visited_nodes:
                                true_clr_idx = [0,0]
                                cond = True
                            else:
                                true_clr_idx = [0,1]
                                cond = False
                            for idx_n, node in enumerate(ord_nodes):
                                poly = c_in_poly_g.nodes[node]['poly']
                                points = np.array(poly.exterior.coords, dtype=np.int).reshape((-1,2))
                                points[:,0] = (points[:,0]-x_offset)*scale 
                                points[:,1] = (points[:,1]-y_offset)*scale
                                points = points.reshape((-1,1,2))
                                mol_img = cv2.polylines(mol_img, [points], isClosed=True, 
                                                            color=color_blocks[true_clr_idx[idx_n]], 
                                                            thickness=2+(parent_edges[node]*4))
                                visited_nodes.append(node)
                                frames.append(copy.deepcopy(mol_img))

                                if cond:
                                    parent_edges[node] += 1
                                elif idx_n == 0:
                                    parent_edges[node] += 1
                            prev_nodes = [cur_edge[0], cur_edge[1]]
                            visited_min_ids[idx_min] = 1
                            break
                iters += 1
            valid_edges[min_ids] = 1
            count += len(min_ids)
        
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        out = cv2.VideoWriter('Anim_tid'+str(t_id)+'_pid'+str(p_id)+'.mp4', fourcc, 0.5, dim)

        for frame in frames:
            out.write(frame)
        cv2.destroyAllWindows()
        out.release()

    def basic_animation(self, mol_img, sorted_edges, sorted_weights, c_poly_mst, c_in_poly_g, 
                            x_offset, y_offset, scale, t_id, p_id, dim):
        frames = [copy.deepcopy(mol_img)]
        valid_edges = np.zeros(len(sorted_edges)).astype(np.int)
        nodes_thck = {n:0 for n in c_poly_mst.nodes()}
        tot_frames = len(sorted_edges)
        
        # Set the color of leaf nodes and other nodes
        node_colors = {}
        for node in c_poly_mst.nodes():
            if c_poly_mst.degree[node] == 1:
                node_colors[node] = (0,255,0)
            else:
                node_colors[node] = (255,0,0)

        prev_nodes = [sorted_edges[0,0], sorted_edges[0,1]]
        count = 0
        while (count < tot_frames):
            # Get the min distances first
            cur_valid_edges = [idx for idx, val in enumerate(valid_edges) if val == 0]
            cur_min = np.min(sorted_weights[cur_valid_edges])
            # Get all the Nodes for min weight
            min_ids = np.where(sorted_weights == cur_min)[0]
            visited_min_ids = np.zeros(len(min_ids))
            iters = 0
            while iters < len(min_ids):
                found_prev = False
                for idx_min, min_id in enumerate(min_ids):
                    if visited_min_ids[idx_min] == 0:
                        if sorted_edges[min_id,0] in prev_nodes or \
                                sorted_edges[min_id,1] in prev_nodes:
                            cur_edge = sorted_edges[min_id]
                            for node in cur_edge:
                                poly = c_in_poly_g.nodes[node]['poly']
                                points = np.array(poly.exterior.coords, dtype=np.int).reshape((-1,2))
                                points[:,0] = (points[:,0]-x_offset)*scale 
                                points[:,1] = (points[:,1]-y_offset)*scale
                                points = points.reshape((-1,1,2))
                                mol_img = cv2.polylines(mol_img, [points], isClosed=True, 
                                                            color=node_colors[node], 
                                                            thickness=2+(nodes_thck[node]*4))
                                nodes_thck[node] += 1
                            prev_nodes = [cur_edge[0], cur_edge[1]]
                            visited_min_ids[idx_min] = 1
                            found_prev = True
                            break
                # If still no node found existing, add the closest
                if not found_prev:
                    for idx_min, min_id in enumerate(min_ids):
                        if visited_min_ids[idx_min] == 0:
                            cur_edge = sorted_edges[min_id]
                            for node in cur_edge:
                                poly = c_in_poly_g.nodes[node]['poly']
                                points = np.array(poly.exterior.coords, dtype=np.int).reshape((-1,2))
                                points[:,0] = (points[:,0]-x_offset)*scale 
                                points[:,1] = (points[:,1]-y_offset)*scale
                                points = points.reshape((-1,1,2))
                                mol_img = cv2.polylines(mol_img, [points], isClosed=True, 
                                                            color=node_colors[node], 
                                                            thickness=2+(nodes_thck[node]*4))
                                nodes_thck[node] += 1
                            prev_nodes = [cur_edge[0], cur_edge[1]]
                            visited_min_ids[idx_min] = 1
                            break
                frames.append(copy.deepcopy(mol_img))
                iters += 1
            valid_edges[min_ids] = 1
            count += len(min_ids)
        
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        out = cv2.VideoWriter('BasicAnim_tid'+str(t_id)+'_pid'+str(p_id)+'.mp4', fourcc, 0.45, dim)

        for frame in frames:
            out.write(frame)
        cv2.destroyAllWindows()
        out.release()
        
