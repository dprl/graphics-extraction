###########################################################################
#
# modules/chemscraper/geometric_utils.py
#
# - Annotating character pos and line geometry (parallel/wedge/intersect)
#
###########################################################################
# Created by: Ming Creekmore, 28 July 2023
###########################################################################

from nx_debug_fns import *   # Includes networkx
import numpy as np
import math
import sys
import nx_merge
import networkx as nx
from shapely.geometry import Point, LineString, MultiLineString, Polygon
from operator import itemgetter
from modules.chemscraper.utils.process_mst import find_vertices
from debug_fns import *
from shapely.affinity import translate
from shapely.ops import nearest_points, unary_union, polygonize
# from modules.chemscraper.utils.lgeval_to_nx import get_midpoint_point, nearest_points

def get_midpoint_point( p1:Point, p2:Point) -> Point:
    return Point((p1.x+p2.x)/2, (p1.y+p2.y)/2)

# STRAIGHT_TOLERANCE = 20.0 # 20 degree difference permitted for lines connected in straight

##################################
# Basic Geometry Functions
##################################
class GeometricToolbox():
    def __init__(self, thresholds):
        self.PARALLEL_TOLERANCE = thresholds["PARALLEL_TOLERANCE"]
        self.LONGEST_LENGTHS_DIFF_RATIO = thresholds["LONGEST_LENGTHS_DIFF_RATIO"]
        self.HASHED_LENGTH_DIFF_THRESHOLD = thresholds["HASHED_LENGTH_DIFF_THRESHOLD"]
        self.NEG_CHARGE_LENGTH_TOLERANCE = thresholds["NEG_CHARGE_LENGTH_TOLERANCE"]

    def poly_to_line(self, poly):
        # approximate a polygon meant to represent a line in the source pdf as a line
        
        rect = poly.minimum_rotated_rectangle

        last = None
        dists = []

        for p in rect.exterior.coords:
            p = Point(p)
            if last is not None:
                dists.append(last.distance(p))
            last = p

        dists = np.flatnonzero(np.array(dists) == np.min(dists)) % 4

        if len(dists) == 1:
            dists = np.array([dists[0].item(), (dists[0] + 2) % 4])

        m1 = LineString([Point(rect.exterior.coords[dists[0].item()]), Point(rect.exterior.coords[dists[0].item() + 1])]).centroid
        m2 = LineString([Point(rect.exterior.coords[dists[1].item()]), Point(rect.exterior.coords[dists[1].item() + 1])]).centroid

        return LineString([m1, m2])


    def is_equidistance_parallel(self, line1, line2, offset):
        # Check if two parallel lines are equidistant from most points
        # Done by checking if a perpendicular line crosses both the lines

        converted_line1 = line1

        if line1.geom_type == 'Polygon':
            converted_line1 = self.poly_to_line(line1)

        left = converted_line1.parallel_offset(offset, 'left')
        right = converted_line1.parallel_offset(offset, 'right')

        mid_left = left.interpolate(0.5, normalized=True)
        mid_right = right.interpolate(0.5, normalized=True)

        perpendicular = LineString([mid_left, mid_right])
        return perpendicular.intersects(line1) and perpendicular.intersects(line2)

    def parallel_pair(self,  G ):
        def f(node1, node2 ):
            # ASSUMES that both nodes are lines.
            return abs( G.nodes[node1]['angle'] - G.nodes[node2]['angle'] ) < self.PARALLEL_TOLERANCE
            
        return f
    
    def collinear_pair(self,  G ):
        def f(node1, node2 ):
            # ASSUMES that both nodes are lines.
            # TODO are all of these checks needed?
            parallel_test = self.parallel_pair( G )
            is_straight_test = self.is_straight(G)
            dashed_line_test = self.can_be_dashed_line(G)

            is_parallel = parallel_test(node1, node2)
            is_straight = is_straight_test(node1, node2)
            is_similar_length = dashed_line_test(node1, node2)

            return is_parallel and is_straight and is_similar_length


        return f

    def in_sight(self, G):
        def f(node1, node2):
            line_node1 = LineString([tuple(G.nodes[node1]["verts"][0]),tuple(G.nodes[node1]["verts"][1])])
            line_node2 = LineString([tuple(G.nodes[node2]["verts"][0]),tuple(G.nodes[node2]["verts"][1])])

            if line_node1.length>line_node2.length:
                line1 = line_node1
                line2 = line_node2
            else:
                line2 = line_node1
                line1 = line_node2

            
            p1 = line1.interpolate(line1.project(Point(line2.coords[0])))
            p2 = line1.interpolate(line1.project(Point(line2.coords[1])))

            projected_line = LineString([p1,p2])
            are_in_sight = max(projected_line.length, line2.length)>0 and \
                (min(projected_line.length, line2.length)/max(projected_line.length, line2.length))>=self.LONGEST_LENGTHS_DIFF_RATIO
            return are_in_sight
        return f

    def is_straight(self, G):
        def f(node1, node2): 
            m1 = G.nodes[node1]['midpoint']
            m2 = G.nodes[node2]['midpoint']

            if m1[0] == m2[0]:
                slope = float('inf')
            else:
                slope = (m1[1]-m2[1]) / (m1[0]-m2[0])
            
            angle = math.degrees(math.atan(slope))
            # correct arctan result for vertical lines
            if angle < 0 and (90 - abs(angle)) < self.PARALLEL_TOLERANCE:
                angle = abs(angle)

            test = abs( G.nodes[node1]['angle'] - angle )
            return test < self.PARALLEL_TOLERANCE
            

        return f
    
    def can_be_dashed_line(self, G):
        def f(node1, node2):
            l1 = LineString([tuple(G.nodes[node1]["verts"][0]), tuple(G.nodes[node1]["verts"][1])])
            l2 = LineString([tuple(G.nodes[node2]["verts"][0]), tuple(G.nodes[node2]["verts"][1])])

            line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
            graph_view = nx.subgraph_view(G, filter_node=line_selector)
            lengths = list(nx.get_node_attributes(graph_view, 'length').values())

            mean_line_length = np.mean(lengths)

            return l1.length < self.NEG_CHARGE_LENGTH_TOLERANCE * mean_line_length and l2.length < self.NEG_CHARGE_LENGTH_TOLERANCE * mean_line_length

        return f

    def endpoint_intersect(self,  G ):
        # finds the 'intersection' (midpoint) between two nodes
        def f(node1, node2):
            # ASSUMED that both nodes are lines
            if 'verts' not in G.nodes[node1] or 'verts' not in G.nodes[node2]:
                return None
            verts1 = G.nodes[node1]['verts']
            verts2 = G.nodes[node2]['verts']
            closest = sys.maxsize
            for vert1 in verts1:
                for vert2 in verts2:
                    dis = Point(vert1).distance(Point(vert2))
                    if dis < closest:
                        intersection = self.find_midpoint([vert1, vert2])
                        closest = dis

            return intersection

        return f

    def find_midpoint(self,  vert_lst ):
        #ASSUMED vert_lst is a list of x,y coor pair
        count = 0
        x = 0
        y = 0
        for vert in vert_lst:
            x += vert[0]
            y += vert[1]
            count +=1
        return x/count, y/count

    def get_shorter_distance(self, G, n, intersect):
        # return the distance that is shortest between the verts and intersection
        # n is the node whose verts we need to access
        # intersect is the a x,y coor
        dis_vert1 = Point(G.nodes[n]['verts'][0]).distance(Point(intersect))
        dis_vert2 = Point(G.nodes[n]['verts'][1]).distance(Point(intersect))
        if dis_vert1 < dis_vert2:
            return dis_vert1
        else:
            return dis_vert2

    ##################################
    # Lines Geometry
    ##################################

    def annotate_line_geometry(self,  G ):
        for node in G.nodes:
            lbl = G.nodes[node]['lbl_cls']
            if lbl == 'Ln' or lbl == 'Wave':

                # ADAPTED from AD's chemscraper/utils/process_mst.py
                shape = G.nodes[node]["shape"]
                shape_obj = G.nodes[node]["shape_obj"]
                # polygon = G.nodes[node]['poly']
                # min_rect = polygon.minimum_rotated_rectangle
                node_verts = find_vertices(shape, shape_obj)

                # Slope = y2-y1/x2-x1
                if node_verts[1][0] == node_verts[0][0]:
                    slope = float('inf')  #
                else:
                    slope = (node_verts[1][1] - node_verts[0][1]) /   \
                            (node_verts[1][0] - node_verts[0][0])

                degrees = math.degrees(math.atan(slope))
                if degrees < 0:
                    if (90 - abs(degrees)) < self.PARALLEL_TOLERANCE: # correcting arctan result
                        degrees = abs(degrees)
                G.nodes[node]['slope'] = slope # TODO this may not be needed
                G.nodes[node]['angle'] = degrees
                G.nodes[node]['length'] = Point(node_verts[0]).distance(Point(node_verts[1]))
                G.nodes[node]['verts'] = node_verts
                G.nodes[node]['midpoint'] = self.find_midpoint([node_verts[0], node_verts[1]])

        return G

    def annotate_edges_parallel_lines(self,  G, M):
        # Add parallel annotation to edges between pairs of lines

        # Create view with subgraphs containing only lines
        nx.set_edge_attributes(G, False, "parallel")
        line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
        graph_view = nx.subgraph_view(G, filter_node=line_selector)
        line_selector = nx_merge.node_attr_match( M, 'lbl_cls', 'Ln' )
        mst_view = nx.subgraph_view(M, filter_node=line_selector)
        parallel_test = self.parallel_pair( G )
        # straight_test = is_straight(G)
        in_sight_test = self.in_sight(G)
        def probably_hash_edge(n1, n2):
            return any(map(lambda nei: nei not in (n1, n2) and parallel_test(nei, n2), mst_view.neighbors(n1))) and any(map(lambda nei: nei not in (n1, n2) and parallel_test(nei, n1), mst_view.neighbors(n2)))

        for (n1, n2) in graph_view.edges:
            # Label as true/false; all line-line edges given parallel attribute

            parallel = parallel_test(n1,n2)

            # Test if both lines are connected in straight line
            # or are actually parallel
            if parallel:
                if 'added_edge' in graph_view.edges[(n1, n2)]:
                    pass
                are_lines_in_sight = in_sight_test(n1,n2)
                # straight = straight_test(n1, G.nodes[n2]['midpoint'])
                # if not straight:
                if are_lines_in_sight:
                    if '-' in n1 or '-' in n2:
                        continue # Prevent checking '-' charges
                    if probably_hash_edge(n1, n2):
                        G.edges[(n1, n2)]['hash'] = True
                    G.edges[(n1,n2)]['parallel'] = True
                    G.edges[(n1,n2)]['parallel'] = True
            else:
                G.edges[(n1,n2)]['parallel'] = False

        return G

    def merge_collinear_line_endpoints(self,  G ):
        # Recompute endpoint 'vertices' for collinear lines. 
        # Store in 'merged_verts' node attribute

        # Select merged collinear line nodes (with member lines in contraction attribute)
        line_selector = nx_merge.node_attr_substring( G, 'contraction_label', 'Dash')
        graph_view = nx.subgraph_view(G, filter_node=line_selector)

        for node_label in graph_view.nodes:
            # Get contracted nodes
            node = graph_view.nodes[node_label]
            contracted_nodes = node['contraction']

            (c1, c2) = node['verts']
            (p1, p2) = ( Point(c1), Point(c2) )
            s_list = [LineString([p1, p2])]
            p_list = [p1, p2]

            for (_, c_node) in contracted_nodes.items():
                (cc1, cc2) = c_node['verts']
                (pc1, pc2) = ( Point(cc1), Point(cc2) )
                p_list.append(pc1)
                p_list.append(pc2)
                s_list.append(LineString([pc1, pc2]))

            xs = [p.x for p in p_list]
            ys = [p.y for p in p_list]
            xrange = max(xs) - min(xs)
            yrange = max(ys) - min(ys)
            
            # *mostly* horizontal
            if xrange >= yrange:
                p_list = sorted(p_list, key=lambda p: (p.x, p.y))
            # *mostly* vertical
            elif yrange >= xrange:
                p_list = sorted(p_list, key=lambda p: (p.y, p.x))
            else:
                assert(False)

            # Update verts property
            G.nodes[node_label]['merged_verts'] = [ [p_list[0].x, p_list[0].y], (p_list[-1].x, p_list[-1].y) ]

        return G

    def merge_parallel_line_endpoints(self,  G ):
        # Recompute endpoint 'vertices' for parallel lines.
        # Store in 'merged_verts' node attribute

        # Select merged parallel line nodes (with member lines in contraction attribute)
        line_selector = nx_merge.node_attr_substring( G, 'contraction_label', 'Ln||')
        graph_view = nx.subgraph_view(G, filter_node=line_selector)

        for node_label in graph_view.nodes:
            # Get contracted nodes
            node = graph_view.nodes[node_label]
            contracted_nodes = node['contraction']

            # Use endpoints of first line to compute 'closest endpoint' quickly.
            # ASSUMES that for multiple parallel lines, endpoints on one end
            # are closer together than to coordinates at the opposite end.
            (c1, c2) = node['verts']
            (p1, p2) = ( Point(c1), Point(c2) )
            p1_list = [ p1 ]
            p2_list = [ p2 ]

            for (_, c_node) in contracted_nodes.items():
                (cc1, cc2) = c_node['verts']
                (pc1, pc2) = ( Point(cc1), Point(cc2) )
                if p1.distance( pc1 ) < p1.distance( pc2 ):
                    p1_list.append( pc1 )
                    p2_list.append( pc2 )
                else:
                    p1_list.append( pc2 )
                    p2_list.append( pc1 )

            # Create 'LineString' (polyline) using point lists
            ls1 = LineString( p1_list )
            ls2 = LineString( p2_list )

            # Get midpoint using 50% of length along polylines
            # ASSUMES that endpoints are parallel so expected midpoint is computed
            end_p1 = ls1.interpolate(0.5, True)
            end_p2 = ls2.interpolate(0.5, True)

            # Update verts property
            G.nodes[node_label]['merged_verts'] = [ [end_p1.x, end_p1.y], (end_p2.x, end_p2.y) ]

        return G
    

    def annotate_edges_collinear_lines(self,  G):
        # Add collinear annotation to edges between pairs of lines
        
        # Create view with subgraphs containing only lines
        nx.set_edge_attributes(G, False, "collinear")
        line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
        graph_view = nx.subgraph_view(G, filter_node=line_selector)

        collinear_test = self.collinear_pair( G )

        for (n1, n2) in graph_view.edges:
            # Label as true/false; all line-line edges given collinear attribute
            
            collinear = collinear_test(n1, n2)
            
            if collinear:
                if 'added_edge' in graph_view.edges[(n1, n2)]:
                    pass
                G.edges[(n1,n2)]['collinear'] = True            
            else:
                G.edges[(n1,n2)]['collinear'] = False

        return G

    def annotate_line_edge_intersect(self,  G ):
        # add angle and intersection point between pairs of lines
        # ASSUMES all lines and verts identified properly
        # Create view with subgraphs containing only lines
        node_selector = nx_merge.node_attr_exclude(G, 'lbl_cls', 'Circle')
        node_selector2 = nx_merge.node_attr_exclude(G, 'paranthesis', True)
        graph_view = nx.subgraph_view(G, filter_node=node_selector)
        graph_view = nx.subgraph_view(graph_view, filter_node=node_selector2)
        endpoint_finder = self.endpoint_intersect( G )

        for (n1, n2) in graph_view.edges:
            if G.nodes[n1]['lbl_cls'] == 'Char' or G.nodes[n1]['lbl_cls'] == 'Charge':
                G.edges[(n1,n2)]['intersect'] = G.nodes[n1]['pos']
            elif G.nodes[n2]['lbl_cls'] == 'Char' or G.nodes[n2]['lbl_cls'] == 'Charge':
                G.edges[(n1,n2)]['intersect'] = G.nodes[n2]['pos']
            else:
                G.edges[(n1,n2)]['intersect'] = endpoint_finder(n1, n2)


        return G

    def annotate_circles(self, G):
        for node in G.nodes:
            lbl = G.nodes[node]['lbl_cls']

            if lbl == 'Circle':
                circle = G.nodes[node]['poly']
                neighbors = list(G.neighbors(node))
                neighbors_p = neighbors.copy()
                
                for i, n in enumerate(neighbors_p):
                    if 'linestring' in G.nodes[n] and G.nodes[n]['linestring'] is not None:
                        neighbors_p[i] = G.nodes[n]['linestring']
                    elif 'poly' in G.nodes[n] and G.nodes[n]['poly'] is not None:
                        neighbors_p[i] = G.nodes[n]['poly']
                    else:
                        continue

                # Picks the first available neighbor to connect the circle to.
                # Does not ensure correctness, for ex. circles not inside the molecule.
                for n in neighbors:
                    if 'circle_connection' not in G.nodes[n]:
                        G.nodes[n]['circle_connection'] = node
                        G.graph['circle'] = True
                        break

                # FIXME this unknowingly depended on the old non_parallel_line intersection logic
                # for p in polygonize(unary_union(neighbors_p)):
                #     if(p.contains(circle)):
                #         for n in neighbors:
                #             if 'circle_connection' not in G.nodes[n]:
                #                 G.nodes[n]['circle_connection'] = node
                #                 G.graph['circle'] = True
                #                 break

        return G

    ##################################
    # Wedge Logic
    ##################################

    def annotate_hash_lines(self,  G ):
        # ASSUMES parallel lines are done before
        # Add hash annotation to merged nodes that are hashed bonds
        line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
        graph_view = nx.subgraph_view(G, line_selector)
        
        for node in graph_view.nodes:
            if not 'contraction_label' in G.nodes[node] or graph_view.nodes[node]['contraction_label'].find('||') == -1:
                continue

            num_lines = int(graph_view.nodes[node]['contraction_label'].split('||')[1])

            # only 3+ parallel lines could possibly be hash bonds
            if num_lines > 3:
                G.nodes[node]['contraction_label'] = 'Hash'
            elif num_lines == 3:
                length_main = G.nodes[node]["length"]
                bbox_main = G.nodes[node]["box"]
                lengths_bbox = [(length_main, bbox_main)]
                for data in G.nodes[node]["contraction"].values():
                    length_current = data["length"]
                    bbox_current = data["box"]
                    lengths_bbox.append((length_current, bbox_current))
                lengths_bbox = sorted(lengths_bbox, key=lambda x: x[1][:2])
                line_lengths = np.array(list(map(itemgetter(0), lengths_bbox)))

                # Calculate the differences between consecutive line lengths
                length_diffs = np.diff(line_lengths)
                # Check if all differences exceed the threshold
                if (np.all(length_diffs > self.HASHED_LENGTH_DIFF_THRESHOLD) or
                        np.all(length_diffs < -self.HASHED_LENGTH_DIFF_THRESHOLD)):
                    G.nodes[node]['contraction_label'] = 'Hash'
                # if ((np.all(line_lengths[:-1] < line_lengths[1:])) or
                #         (np.all(line_lengths[:-1] > line_lengths[1:]))):
                #     import pdb; pdb.set_trace()
                #     G.nodes[node]['contraction_label'] = 'Hash'

        return G

            # if num_lines > 2:
            #     # Find neighbor's position or vertex that is closest
            #     neis = list(G.neighbors(node))
            #     if not len(neis) == 0:
            #         nei = neis[0]
            #         lbl = G.nodes[nei]['lbl_cls']
            #         if lbl == 'Char' or lbl == 'Charge':
            #             closest_point = G.nodes[nei]['pos']
            #         elif lbl in ['Ln', 'SW', 'Wave']:
            #             shape_obj = G.nodes[node]['shape_obj']
            #             verts = G.nodes[nei]['verts']
            #             closest_point = verts[0]
            #             if shape_obj.distance(Point(verts[1])) < shape_obj.distance(Point(verts[0])):
            #                 closest_point = verts[1]
            #         else:
            #             continue

            #         # Will be a hash if the angle between node line and line from point to midpoint is not parallel
            #         straight = straight_test(node, closest_point)
            #         if not straight:
            #             G.nodes[node]['contraction_label'] = 'Hash'


    def annotate_hash_verts(self,  G ):
        # ASSUMES annotate hash lines done before
        # change verts to represent the actual vertices of the hash bond
        line_selector = nx_merge.node_attr_match( G, 'contraction_label', 'Hash')
        graph_view = nx.subgraph_view(G, filter_node=line_selector)

        # Use line length to determine where it begins and ends
        # Shortest line begins, longest line ends
        for node in graph_view.nodes:

            min_len = G.nodes[node]['length']
            min_node = G.nodes[node]
            max_len = min_len
            max_node = min_node
            lines = G.nodes[node]['contraction']
            for ln in lines:
                cur_len = lines[ln]['length']
                if cur_len < min_len:
                    min_len = cur_len
                    min_node = lines[ln]
                elif cur_len > max_len:
                    max_len = cur_len
                    max_node = lines[ln]

            verts = min_node['verts']
            begin = self.find_midpoint([verts[0], verts[1]])
            verts = max_node['verts']
            end = self.find_midpoint([verts[0], verts[1]])
            G.nodes[node]['verts'] = [begin, end]

        # For hash wedges identified early (in the input graph before MST)
        # Use lenghts and points properties
        hash_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'HW')
        graph_view = nx.subgraph_view(G, filter_node=hash_selector)
        # Use line length to determine where it begins and ends
        # Shortest line begins, longest line ends
        for node in graph_view.nodes:
            line_lengths = G.nodes[node]["length"]
            points = G.nodes[node]["points"]
            sorted_lengths_enum = sorted(enumerate(line_lengths), key=itemgetter(1))
            sorted_lengths_idx = list(map(itemgetter(0), sorted_lengths_enum))
            begin = self.find_midpoint([points[0], points[1]])
            end = self.find_midpoint([points[-2], points[-1]])
            if sorted_lengths_idx[0] == 0:
                G.nodes[node]['verts'] = [begin, end]
            else:
                G.nodes[node]['verts'] = [end, begin]
        return G


    def get_vertexes_solid_wedge(self, original, polygon):
        convex = original.convex_hull
        if Point(polygon.exterior.coords[0]).distance(Point(polygon.exterior.coords[1])) < Point(polygon.exterior.coords[0]).distance(Point(polygon.exterior.coords[3])):
            p1 = get_midpoint_point(Point(polygon.exterior.coords[0]),Point(polygon.exterior.coords[1]))
            p2 = get_midpoint_point(Point(polygon.exterior.coords[2]),Point(polygon.exterior.coords[3]))
        else:
            p1 = get_midpoint_point(Point(polygon.exterior.coords[0]),Point(polygon.exterior.coords[3]))
            p2 = get_midpoint_point(Point(polygon.exterior.coords[1]),Point(polygon.exterior.coords[2]))

        line = LineString([p1,p2])

        p1_ = nearest_points(original,p1)[0]
        p2_ = nearest_points(original,p2)[0]
        try:
            points = LineString([p1_,p2_])
        except:
            import pdb;pdb.set_trace()


        midpoint = get_midpoint_point(p1,p2)
        shorter_line = LineString([get_midpoint_point(p1,midpoint),get_midpoint_point(midpoint,p2)])
        cd_length = points.length
        left = shorter_line.parallel_offset(cd_length / 2, 'left')
        right = shorter_line.parallel_offset(cd_length / 2, 'right')
        c1 = left.boundary.geoms[0]
        d1 = right.boundary.geoms[1]
        c2 = left.boundary.geoms[1]
        d2 = right.boundary.geoms[0]
        perpendicular1 = LineString([c1, d1])
        perpendicular2 = LineString([c2, d2])

        intersetion1 = perpendicular1.intersection(convex)
        intersetion2 = perpendicular2.intersection(convex)

        verts = None

        if intersetion1.length<intersetion2.length:
            if intersetion1.distance(Point(points.coords[0]))<intersetion1.distance(Point(points.coords[1])):
                verts= [points.coords[0],points.coords[1]]
            else:
                verts= [points.coords[1],points.coords[0]]
        else:
            if intersetion2.distance(Point(points.coords[0]))<intersetion2.distance(Point(points.coords[1])):
                verts= [points.coords[0],points.coords[1]]
            else:
                verts= [points.coords[1],points.coords[0]]
        return verts

    def annotate_solid_wedge_verts(self,  G ):
        # Find actual vertices of solid wedges
        sw_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'SW' )
        graph_view = nx.subgraph_view(G, sw_selector)

        for node in graph_view.nodes:
            cur_poly = G.nodes[node]['poly']
            min_rect = cur_poly.minimum_rotated_rectangle
            # x,y = min_rect.exterior.coords.xy
            # edge_dists = (Point(x[0], y[0]).distance(Point(x[1], y[1])), Point(x[1], y[1]).distance(Point(x[2], y[2])),
            #                 Point(x[2], y[2]).distance(Point(x[3], y[3])), Point(x[3], y[3]).distance(Point(x[0], y[0])))
            # # the edge with minimum length and its opposite side are the vertices
            # min_edge_idx1 = edge_dists.index(min(edge_dists))
            # min_edge_idx2 = (min_edge_idx1 + 2) % 4
            # verts1 = [[x[min_edge_idx1],y[min_edge_idx1]], [x[min_edge_idx1+1],y[min_edge_idx1+1]]]
            # verts2 = [[x[min_edge_idx2],y[min_edge_idx2]], [x[min_edge_idx2+1],y[min_edge_idx2+1]]]
            # begin = self.find_midpoint([verts1[0], verts1[1]])
            # end = self.find_midpoint([verts2[0], verts2[1]])

            verts = self.get_vertexes_solid_wedge(cur_poly, min_rect)
            G.nodes[node]['verts'] = verts
        return G

    ###########################
    # Char and Final Pos
    ###########################

    def merge_bbs(self, out_cbox, box):
        return [ min(out_cbox[0], box[0]),   # minx (left)
                min(out_cbox[1], box[1]),   # miny (top)
                max(out_cbox[2], box[2]),   # maxx (right)
                max(out_cbox[3], box[3]) ]  # maxy (bottom)

    def annotate_char_pos(self,  G ):
        # RZ: Add bounding boxes for merged strings
        char_selector = nx_merge.node_attrs_match(G, 'lbl_cls', 'Char', 'Charge')
        graph_view = nx.subgraph_view(G, filter_node=char_selector)
        for node_id in graph_view.nodes:
            # annotate floating
            if G.degree[node_id] == 0:
                G.nodes[node_id]['floating'] = True
                G.graph['floating'] = True

            n = G.nodes[node_id]
            if not 'pos' in n:
                pos_node = n
                # Need to find character that is closest to the line
                # this will serve as the actual position of the character
                # TODO this might not be the best positioning - difference between terminal and non-terminal
                distances = []
                if 'contraction' in n:
                    # Construct bounding box for all characters (full string)
                    out_cbox = n['box']
                    chars = n['contraction']
                    for c in chars:   # iterates over node ids (keys in dict)
                        out_cbox = self.merge_bbs(out_cbox, chars[c]['box'])
                    n['cbox'] = out_cbox

                    # Construct 'pos' (connection point position)
                    neis = list(G.neighbors(node_id))
                    if not len(neis) == 0:
                        nei_poly = G.nodes[neis[0]]['shape_obj']
                        min_dis = n['shape_obj'].distance(nei_poly)
                        chars = n['contraction']
                        distances = [(pos_node['lbl_type'], min_dis)]

                        for c in chars:
                            cur_dis = chars[c]['shape_obj'].distance(nei_poly)
                            distances.append((chars[c]['lbl_type'], cur_dis))
                            if cur_dis < min_dis:
                                min_dis = cur_dis
                                pos_node = chars[c]

                # Define connection position as centroid using position node
                x,y = pos_node['shape_obj'].centroid.xy
                center_point = [x[0],y[0]]
                G.nodes[node_id]['pos'] = center_point

        return G

    def get_guide_line(self, G, text):
        # TODO multiple chars and horizontal guide line
        line_selector = nx_merge.select_all_lines(G)
        graph_view = nx.subgraph_view(G, filter_node=line_selector)
        
        for node in graph_view.nodes:
            if (node, text) in G.edges:
                if G.nodes[node]['linestring'] is None or G.nodes[text]['poly'] is None:
                    continue
                if G.nodes[node]['linestring'].intersects(G.nodes[text]['poly']):
                    line_len = G.nodes[node]['linestring'].length
                    text_vlen = abs(G.nodes[text]['box'][3] - G.nodes[text]['box'][1])
                    text_hlen = abs(G.nodes[text]['box'][2] - G.nodes[text]['box'][0])
                else:
                    continue

                # HACK, should use parallel tolerance
                compare_to = 0
                coords = G.nodes[node]['linestring'].coords
                # print(node)
                # print(tuple(coords))
                if abs(coords[0][0] - coords[1][0]) < 1.0:
                    compare_to = text_vlen
                elif abs(coords[0][1] - coords[1][1]) < 1.0:
                    compare_to = text_hlen
                else:
                    continue
                
                if line_len <= compare_to * 2.5:
                    return G, node

            elif (text, node) in G.edges:
                assert(False)

        return G, None


    def label_correlate_adj_verts(self, G):
        # Get which neighbors go on the 1st vertex and 2nd vertex
        # Will "fix" vertices to have same position as calculated intersect point
        # ASSUME char pos done before this
        # ASSUME parallel lines, SW, and HW verts have been done before this
        # ASSUME annotate_line_edge_geometry done before this
        line_selector = nx_merge.select_all_lines(G)
        graph_view = nx.subgraph_view(G, filter_node=line_selector)

        # Used to determine which multi-intersections have been calculated already
        mult_visited = dict()

        for node in graph_view.nodes:
            vert1 = G.nodes[node]['verts'][0]
            vert2 = G.nodes[node]['verts'][1]
            adj = [[],[]]

            # sorting which neighbor goes to which vertex (1st, 2nd)
            # TODO: the order of neighbors is random and the correctness depends on
            # this order: for some order, parsing is correct and incorrect for others
            # since only the first atom's position is used
            # For now, use a specific order based on degree, distance and label to remove randomness

            # Neighbors with degree and distance from node
            neighbors = list(map(lambda x: (x, G.degree(x), G[node][x]['weight']),
                                 filter(lambda nei: G.nodes[nei]['lbl_cls'] != 'Annotation', G.neighbors(node))))
            # Sort by 1. degree, 2. distance, 3. label
            neighbors = list(map(itemgetter(0), sorted(neighbors, key=itemgetter(1, 2, 0))))

            for nei in neighbors:
                if nei == node:
                    continue
                if G.nodes[nei]['lbl_cls'] == 'Char' or G.nodes[nei]['lbl_cls'] == 'Charge':
                    char_pos = G.nodes[nei]['pos']
                    if Point(vert1).distance(Point(char_pos)) < Point(vert2).distance(Point(char_pos)):
                        adj[0].append(nei)
                    else:
                        adj[1].append(nei)
                elif 'Ln' in nei or 'SW' in nei or 'Wave' in nei or 'HW' in nei:
                    dis1 = self.get_shorter_distance(G, nei, vert1)
                    dis2 = self.get_shorter_distance(G, nei, vert2)
                    if dis1 < dis2:
                        adj[0].append(nei)
                    else:
                        adj[1].append(nei)

            # Correlating verts based on if multi-intersect or just one
            for i in range(len(adj)):
                lst = adj[i]
                if len(lst) == 1:
                    if G.nodes[lst[0]]['lbl_cls'] == 'Char' or G.nodes[lst[0]]['lbl_cls'] == 'Charge':
                        # If it is an in-char character replace line verts
                        if G.degree(lst[0]) > 1:
                            G.nodes[node]['verts'][i] = G.nodes[lst[0]]['pos']
                        # Replace the character pos
                        else:
                            G.nodes[lst[0]]['pos'] = G.nodes[node]['verts'][i]
                    else:
                        G.nodes[node]['verts'][i] = G.edges[(node, lst[0])]['intersect']
                # Assume if len(lst) greater than 1, then it must be multiple lines intersecting
                elif len(lst) > 1:
                    node_lst = set(lst)
                    node_lst.add(node)
                    node_lst = tuple(sorted(node_lst))
                    if node_lst not in mult_visited:
                        vert_lst = []
                        for n in lst: # TODO if problems may need to fix it so get all edges
                            vert_lst.append(G.edges[(node, n)]['intersect'])
                        intersect = self.find_midpoint(vert_lst)
                        mult_visited[node_lst] = intersect
                        G.nodes[node]['verts'][i] = intersect
                    else:
                        G.nodes[node]['verts'][i] = mult_visited[node_lst]

            G.nodes[node]['adj'] = adj
        return G

    def are_lines_parallel(self, line1:LineString, line2:LineString, threshold)->bool:

        l1p1 = Point(line1.coords[0])
        l1p2 = Point(line1.coords[1])

        l2p1 = Point(line2.coords[0])
        l2p2 = Point(line2.coords[1])

        if l1p1.distance(l2p1)<l1p1.distance(l2p2):
            l2_translated = translate(line2,xoff=l1p1.x-l2p1.x,yoff=l1p1.y-l2p1.y)
        else:
            l2_translated = translate(line2,xoff=l1p1.x-l2p2.x,yoff=l1p1.y-l2p2.y)

        l2_translated_p1 = Point(l2_translated.coords[0])
        l2_translated_p2 = Point(l2_translated.coords[1])

        if l2_translated_p1.distance(l1p1)>l2_translated_p2.distance(l1p1):
            reference_l2_translated = l2_translated_p1
        else:
            reference_l2_translated = l2_translated_p2

        third_side = LineString([reference_l2_translated, l1p2])
        a = line1.length
        b = l2_translated.length
        c = third_side.length
        try:
            C = math.acos((a*a+b*b-c*c)/(2*a*b))*180/math.pi
        except:
            return True

        difference = C

        return difference<threshold
