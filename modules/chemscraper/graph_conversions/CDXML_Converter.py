###########################################################################
#
# modules/chemscraper/graph_conversions/CDXML_Converter.py
#
###########################################################################
# 
# Originally Created 01/29/2023 (Abhisek Dey)
###########################################################################

# Debugging libraries (general, and for visualizing graphs)
from debug_fns import *
from modules.chemscraper.nx_debug_fns import gdescr, vgdescr
from collections import defaultdict

# Standard libraries
import os
import re
import copy
import numpy as np
from bs4 import BeautifulSoup as bsoup
from rdkit.Chem import rdchem
import networkx as nx
import math
from itertools import chain

# ChemScraper includes (at end: map-reduce)
from modules.chemscraper.conversions import CDXML_Soup_Container, \
    scale_bbox, spaced_digits, bbox_string, pos_string, point_size, \
    get_sequence_hydrogens_and_charge, strip_hydrogens_charges, \
    is_known_sequence, ELEMENTS, ABBREVS, CHARGES

from modules.utils.dprl_map_reduce import map_reduce, combine_dict_tuple, map_concat

from modules.chemscraper.parser_v2 import Side

################################################################
# CDXML Convert Class (visual graphs to CDXML files)
################################################################

def append_pt( size ):
    # RZ: ChemDraw ignores 'pt', molconvert raises exception and renders more nicely with this.
    return str(size) + 'pt' 

def p_write_molecule( in_list ):
    # Map functions must be defined at top-level
    ( page, no_id) = in_list
    CDXML_Converter.pr_cdxml_dict_REF[page][no_id].write_cdxml()
    
    # **Needs to be a list of items to be concatenated for map_concat
    return [ ]

class CDXML_Converter(object):

    pr_cdxml_dict_REF = None   # HACK for parallelization

    @staticmethod
    def init_cdxml_dict( dpr_vgraph_dict, doc, save_dir ):
        region_count = 0
        r_dict = dict()
        r_dict[doc] = dict()
        container_list = []
        for page in dpr_vgraph_dict[doc]:
            r_dict[doc][page] = dict()
            for (no_id, graph) in enumerate( dpr_vgraph_dict[doc][page] ):
                r_dict[doc][page][no_id] = CDXML_Soup_Container(doc, save_dir, page_idx=page, no_id=no_id)
                container_list.append( (page,no_id) )
                region_count += 1

        # Set class variable (HACK)
        CDXML_Converter.pr_cdxml_dict_REF = r_dict[doc]
        return (r_dict, container_list, region_count)

    def __init__(self, save_dir, dpr_vgraph_dict, doc, dpi=300,
                 write_pages_out=0, verbose=True, fixed_attach=False, gen_svg=False ) -> None:
        # Converter class to hold bsoup-based CDXML conversions of individual graphs, 
        # pages, and full documents.

        ##################################
        # Chemical information
        ##################################
        # Periodic table of the elements, abbreviation lookup table, bond order dictionary
        self.elements = ELEMENTS
        self.abbrevs = ABBREVS
        self.bond_order = {"Double":"2", "Triple":"3"}

        ##################################
        # Formatting and Scale Parameters
        ##################################
        # Text formatting attributes
        self.color = "0"   # Black ink
        self.font = "21"   # matches font in header font table in SAMPLE_CDXML
        self.face = "96"   # Base (plain) font for chemical formulas (renders sub/superscripts)

        self.size = "6"   # *May* only be used to set the carbon dot size.

        # Default formatting parameters (for text, as kwargs dictionary)
        self.kwargs_default_text = { 'color' : self.color, 'face' : self.face, 'font' : self.font }

        # CDXML Page Dimensions (ASSUMES Standard US letter 8.5 x 11 inches)
        # ALSO ASSUMES 300 dpi input coordinates
        self.canvas_x = 540  # 7.5 inches (1/2 inch margins) * 72 dots-per-inch (dpi)
        self.canvas_y = 720  # 10 inches  (1/2 inch margins) * 72 dpi
        self.canvas_size = None   # Defined by scale_coords()

        # Define coordinate scaling factors, (default is 300 dots-per-inch (dpi)
        self.page_height = 720
        self.page_y = 11 * dpi
        self.page_x = 8.5 * dpi
        self.scale_x = self.canvas_x / self.page_x
        self.scale_y = self.canvas_y /  self.page_y
        self.page_size = None   # Defined by scale_coords()


        ##################################
        # Data reference and output dir.
        ##################################
        # Record dpr dict and document/page/region number (dpr) for this object. 
        self.doc_name = doc
        self.page_length = len(dpr_vgraph_dict[doc])
        self.save_dir = save_dir + '/' + self.doc_name
        if gen_svg:
            self.save_dir = self.save_dir + '_SVG'      # Save SVG modification in separate directory

        self.dpr_vgraph_dict = dpr_vgraph_dict

        ##################################
        # Graph conversion variables 
        ##################################
        # Book-keeping:
        # Next node/bond id and Z position integer id
        # and dictonary to map node/bond names to integer id's
        self.id = 1000          # Numeric identifier; unique for nodes, bonds, fragments, and page
        self.z = 1              # Layer for rendering in ChemDraw
        self.id_dict = dict()   # Dictionary from node name to integer id
        self.new_frag = None    # Temp variable for CDXML fragments (used in other fns)
        self.deferred_nodes = []# nodes that need to be added to the end of a CDXML fragment (used in other fns)
        self.embed_named_structures = True # Whether to insert structures for known entities [ *HARD CODED ]
        
        self.metrics = { doc : dict() }     # Conversion metrics data structure
        self.unconverted = { doc : [] }     # Variables to record conversion errors
        self.region_count = 0

        self.gen_svg = gen_svg              # CDXML generation for SVG rendering (altered)

        ##################################
        # DEBUG for alt metrics
        # (for embedded graphs)
        ##################################
        self.fixed_attach = fixed_attach

        ##################################
        # Convert visual graph to CDXML
        ##################################
        # MOLECULES: Initialize soup container dictionaries (graphs, pages, document)
        # Create CDXML soup containers, write each molecule separately to disk.
        (self.dpr_cdxml_dict, self.soup_container_list, self.region_count) = \
                CDXML_Converter.init_cdxml_dict( dpr_vgraph_dict, doc, self.save_dir )

        if not gen_svg:
            # Normal operation
            self.create_graph_cdxml_containers()
            # Sequential for now -- speedup is not large for individual files using map_concat
            self.write_individual_cdxmls(parallel=False)
        else:
            # SVG modigications: Replace - by \-, remove nested fragment nodes/subgraphs
            self.create_graph_cdxml_containers(svg=gen_svg)
            self.write_individual_cdxmls(parallel=False)
            return  # STOP HERE, do not report stats, write pages, etc.

        ### Report and record metrics 
        unconverted_count = len( self.unconverted[doc] )
        if verbose:
            check('\nCDXML_Converter :: Generating CDXML for PDF', doc)
            check('* Initial MSTs', self.region_count)
            check('* MSTs converted to CDXML', self.region_count - unconverted_count )
            check('* Unconverted MSTs', unconverted_count )
            if unconverted_count > 0:
                ncheck('* Unconverted MSTs ( (page_id, region_id) )', self.unconverted[doc] )

        # Record in metrics datastructure
        self.metrics[ doc ][ 'Initial-MST-count' ] = self.region_count
        self.metrics[ doc ][ 'MSTs-converted-to-CDXML' ] = self.region_count - unconverted_count
        self.metrics[ doc ][ 'MSTs-not-converted-to-CDXML' ] = unconverted_count
        if unconverted_count > 0:
            self.metrics[ doc ][ 'MSTs-not-converted-to-CDXML-(page_id-region)-list' ] = self.unconverted[doc]
        else:
            self.metrics[ doc ][ 'MSTs-not-converted-to-CDXML-(page_id-region)-list' ] = []

        ##################################
        # Writing pages + full doc
        ##################################
        # SKIP writing page/full document result unless requested
        if write_pages_out > 0:
            # PAGES/DOC: Write page and full documents to disk
            page_save_dir = save_dir + '/' + doc + '_all_pages'
            page_template = CDXML_Soup_Container(doc, page_save_dir, page_length=1)
            doc_save_dir = save_dir + '/' + doc + '_full_cdxml'
            doc_template = CDXML_Soup_Container(doc, doc_save_dir, page_length = self.page_length)

            page_template.write_cdxml_fragments_txt( self.dpr_cdxml_dict )
            doc_template.write_cdxml_fragments_txt( self.dpr_cdxml_dict )


    ##################################
    # Utility functions
    ##################################
    def __str__(self):
        return self.soup_container.__str__()

    def default_node_tag_attributes(self):
        kwargs_node = dict()
        kwargs_node['n'] = dict()
        kwargs_node['t'] = dict()
        kwargs_node['s'] = self.kwargs_default_text

        return kwargs_node

    ### Conversion functions
    def create_graph_cdxml_containers( self, svg=False ):
        doc = self.doc_name
        for page in self.dpr_vgraph_dict[doc]:
            self.page = page
            for (no_id, _) in enumerate( self.dpr_vgraph_dict[doc][page] ):
                self.no_id = no_id
                try:
                    self.convert_graph( doc, page, no_id, svg=self.gen_svg )
                except Exception as err:
                    check('CDXML Error', ((doc, page, no_id), err))
                    self.unconverted[doc].append( (page, no_id ) )


    ### File I/O
    def write_individual_cdxmls( self, parallel=False ):
        doc = self.doc_name
        if not parallel:
            for page in self.dpr_cdxml_dict[doc]:
                for no_id in self.dpr_cdxml_dict[doc][page]:
                    # Write soup string using its 'container' method
                    soup_container = self.dpr_cdxml_dict[doc][page][no_id]
                    soup_container.write_cdxml()
        else:
            # Execute writes in parallel
            _  = map_concat( p_write_molecule, self.soup_container_list )

    ### Book-keeping for graph conversion
    def increment_id(self, nested=False):
        # Increment vertical position and next integer identifier for nodes/edges (bonds)
        self.id += 1

        if not nested:
            # Non-visible elements do not require a Z coordinate
            self.z += 1

    ### Page Geometry
    def get_node_coord_strings(self, n):
        # Return strings for node position and bounding box
        str_node_pos = pos_string( self.nx_graph, n)
        str_bb = bbox_string( self.nx_graph, n )

        return (str_node_pos, str_bb)

    def scale_coords(self, page_idx=0): 
        # Scale node positions to CDXML coord system from image dpi
        # Operates on networkx graph representation (i.e., parse output)

        # Collect coordinates and bounding boxes
        coords = []
        bboxes = []
        try:
            for n in sorted(self.nx_graph.nodes(data=True)):
                pos = n[1]['pos'] # dictionary in pair entry
                pos = np.array(pos)
                coords.append(pos)

                # RZ: Add bounding boxes: add placeholder for notes without a bbox
                if 'bbox' in n[1]:
                    bboxes.append( np.array( n[1]['bbox'] ))
                else:
                    # Placeholder: use position
                    # DEBUG: (RZ) This change seemed necessary to get correct maxY coords, possibly
                    bboxes.append( np.array( [pos[0], pos[1], pos[0], pos[1] ] ) )

        except AttributeError:
            print(self.nx_graph)
            exit(0)
        
        coords = np.array(coords).reshape((-1,2)).astype(float)
        bboxes = np.array(bboxes).reshape((-1,4)).astype(float)

        # Compute y-offset, then scale coordinates to 72dpi and apply 
        # add vertical translation if on 2nd+ pages (i.e., page_idx > 0)
        y_off = self.canvas_y * page_idx

        # Scale the coordinates
        coords[:,0] *= self.scale_x
        coords[:,1] *= self.scale_y + y_off

        if len(bboxes) == 0:
            raise Exception('No Sscraper bounding boxes found in the molecule')

        bboxes[:,0] *= self.scale_x
        bboxes[:,1] *= self.scale_y + y_off
        bboxes[:,2] *= self.scale_x
        bboxes[:,3] *= self.scale_y + y_off

        if len(bboxes) == 0:
            raise Exception("Empty MST")

        # RZ: Using bounding boxes rather than points to get canvas BB
        [ x_min, y_min, x_max, y_max ] = [  np.min(bboxes[:,0]), 
                                            np.min(bboxes[:,1]), 
                                            np.max(bboxes[:,2]), 
                                            np.max(bboxes[:,3]) ]
        
        
        # Update coordinates in graph nodes (networkx graph)
        for idx_n, n in enumerate(sorted(self.nx_graph.nodes())):
            self.nx_graph.nodes[n]['pos'] = list(coords[idx_n]) 

            if 'bbox' in self.nx_graph.nodes[n]:
                self.nx_graph.nodes[n]['bbox'] = list(bboxes[idx_n,:])

        # Used to write CDXML page attributes
        # Q. [Possible bug] is this the correct page_size?
        canvas_size = spaced_digits( [ x_min, y_min, x_max, y_max ] )
        page_size = spaced_digits( [ 0, 0, x_max, y_max ] )

        return (canvas_size, page_size)


    ##################################
    # ORIGINAL node functions
    ##################################
    def add_nested_fragment_node(self, n, embed_nested=True):
        (str_node_pos, str_bb) = self.get_node_coord_strings(n)
        int_node_ids = {}

        # Fragment if there are multiple elements/structures (like a chemical formula)
        # has more than one capital letter
        num_cap_letters = sum(1 for c in n if c.isupper())
        if num_cap_letters > 1:
            node_type = "Fragment"
        else:
            node_type = "Nickname"

        # Check whether label is to left of connection (to print string L->R)
        label_display = False
        if 'LabelDisplay' in self.nx_graph.nodes[n]:
            label_display = True

        # DEBUG: Force expansion LR if input graph create by gen_mol_graphs() (New evaluation)
        attach_side = Side.LEFT 
        if not self.fixed_attach:
            attach_side = self.nx_graph.nodes[n]['np_side']

        # Create outer node tag for fragment
        if not label_display:
            base_n_tag = self.soup.new_tag("n", 
                    NodeType=node_type, Z=str(self.z), id=str(self.id), 
                    p=pos_string( self.nx_graph, n))
        else:
            base_n_tag = self.soup.new_tag("n", 
                    NodeType=node_type, Z=str(self.z), id=str(self.id), 
                    p=pos_string( self.nx_graph, n), LabelDisplay="Right")

        self.id_dict[n] = self.id
        self.increment_id()

        # RZ: Add the text block before the fragment for improved readability
        base_t_tag = None
        if not str_bb == "Undefined":
            base_t_tag = self.soup.new_tag("t", BoundingBox=str_bb)  
        else:
            base_t_tag = self.soup.new_tag("t")
        #base_t_tag = self.soup.new_tag("t", BoundingBox=str_bb)
        base_s_tag = self.soup.new_tag("s", size=append_pt( point_size(self.nx_graph, n) ), 
                **self.kwargs_default_text)
        base_s_tag.string = n.split('_')[0]

        base_t_tag.append(base_s_tag)
        base_n_tag.append(base_t_tag)

        ##################################
        # Embed nested fragment
        ##################################
        # (RZ Feb 2024: Optionally skip embedding a fragment, 
        #   let tools expand names directly, e.g., ChemDraw) 
        # WARNING: prevents valid SMILES generation when abbreviations known
        if embed_nested:
            ### Create the internal fragment
            internal_fragment = self.soup.new_tag("fragment", id=str(self.id))
            self.increment_id(True)
            base_n_tag.append(internal_fragment)

            # Get the nodes of the abbreviation
            abbrev_name = n.split('_')[0]
            g = self.abbrevs[ abbrev_name ]
            nodes = g.nodes()
            #pcheck(abbrev_name + ' Graph', vgdescr(g))

            ##################################
            # Generate nodes/bonds for
            # named abbreviation 
            ##################################
            hidden_node_count = 0
            offset = 10 #points
            for node in nodes:
                pos = None

                # Create offset for nodes other than the connection point
                if not node.split('_')[0] == "*":
                    hidden_node_count += 1

                # RZ: ChemDraw seems to render nicer if we 
                # don't encode the original positions in abbreviated graphs.
                #if 'p' in g.nodes[node]:
                #    pos = g.nodes[node]['p']
                #else: [... indent next block below to restore ...]
                
                # RZ: Define nodes LR, RL, top-bottom, bottom-up depending
                #     on the bond line connection point on the label.
                pos_coords = [ c for c in map(float, str_node_pos.split()) ]
                if attach_side is Side.LEFT:
                    pos_coords[0] = pos_coords[0] + hidden_node_count * offset  
                elif attach_side is Side.RIGHT:
                    pos_coords[0] = pos_coords[0] - hidden_node_count * offset 
                elif attach_side is Side.BOTTOM:
                    pos_coords[1] = pos_coords[1] - hidden_node_count * offset  
                else: # attach_side is 'Top'
                    pos_coords[1] = pos_coords[1] + hidden_node_count * offset  

                pos = " ".join(map(str,pos_coords))

                # Add tags for carbons, external connection points, and atoms
                if node.split('_')[0] == 'C':
                    int_tag = self.soup.new_tag("n", Z=str(self.z), id=str(self.id), p=pos,
                            NeedsClean="yes") 
                elif node.split('_')[0] == "*":
                    # Place at label position (str_node_pos -- same as the label)
                    int_tag = self.soup.new_tag("n", ExternalConnectionNum="1", 
                                NodeType="ExternalConnectionPoint", Z=str(self.z),
                                id=str(self.id), p=str_node_pos, NeedsClean="yes") 
                else:
                    # ATOM Label (from abbreviation graph)
                    # need to determine if there is a charge
                    charge_char = False
                    ele = False

                    # Identify charge character if present
                    if '–' in node:
                        charge_char = '–'
                    elif '-' in node:
                        charge_char = '-'
                    elif '+' in node:
                        charge_char = '+'

                    # Identify charge value (+ve/-ve)
                    if charge_char:
                        split = node.split('_')[0]
                        split = split.split(charge_char)
                        ele = split[0]
                        num = split[1]
                        if num == "":
                            charge = 1
                        else:
                            charge = int(num)
                        if not charge_char == '+':
                            charge *= -1

                    # If no charge, then it should be an element
                    elif node.split('_')[0] in self.elements:
                        ele = node.split('_')[0]

                    # Generate node for element, with charge
                    if ele:
                        atm_no = str(rdchem.Atom(ele).GetAtomicNum())
                        if charge_char:
                            int_tag = self.soup.new_tag("n", Element=atm_no, 
                                NumHydrogens="0", Charge=charge, Z=str(self.z), 
                                id=str(self.id), p=pos, NeedsClean="yes") 
                        else:
                            int_tag = self.soup.new_tag("n", Element=atm_no, 
                                NumHydrogens="0", Z=str(self.z), id=str(self.id), 
                                p=pos, NeedsClean="yes") 
                        

                        # RZ DEBUG: KEEP!! 
                        # Removing font attributes causes molconvert to fail to generate SMILES
                        t_tag = self.soup.new_tag("t") 
                        s_tag = self.soup.new_tag("s", 
                                size=append_pt( 6 ), #point_size(self.nx_graph, n) ), 
                                **self.kwargs_default_text)
                        s_tag.string = ele
                        t_tag.append(s_tag)
                        int_tag.append(t_tag)
                
                # Store the internal id correspondences for filling in internal bonds
                int_node_ids[node] = self.id
                self.increment_id()
                
                # Add the node tag to the internal fragment
                try:
                    base_n_tag.fragment.append(int_tag)
                except UnboundLocalError:
                    print(f' The unmanned node is: {node}')
                    exit(0)

            # Write the bonds of abbreviation
            for bond in self.abbrevs[n.split('_')[0]].edges(data=True):
                b = int_node_ids[bond[0]]
                e = int_node_ids[bond[1]]
                int_b_tag = self.soup.new_tag("b", id=str(self.id), B=str(b), E=str(e), 
                                Z=str(self.z))
                self.increment_id()
                if bond[2]['type'] in self.bond_order.keys():
                    b_type = self.bond_order[bond[2]['type']]
                    int_b_tag['Order'] = b_type
                base_n_tag.fragment.append(int_b_tag)
                del int_b_tag

        
        self.new_frag.append(base_n_tag)



    ##################################
    # Creating new CDXML structures
    ##################################
    def add_cdxml_fragment(self, nested=False):
        # No Z attribute (coordinate) for nested fragments (nested=True)
        kwargs_fragment = dict()
        kwargs_fragment['id'] = str(self.id)
        if not nested:
            kwargs_fragment['Z'] = str(self.z)

        self.new_frag = self.soup.new_tag("fragment", **kwargs_fragment)
        self.increment_id(nested)

        return self.new_frag

    def add_cdxml_node_tag(self, node_id, text_label, text_size, parent_fragment, nested=False, 
            **kwargs):
        # Create a node tag using the passed arguments, and append it into the parent_fragment
        # (side effect is modified parent_fragment if parent_fragment is not None)
        # Tag attributes passed in kwards['n'], kwargs['t'], and kwards['s']

        # Get coordinate strings for position and bounding box
        (str_node_pos, str_bb) = self.get_node_coord_strings(node_id)

        # Create attributes; remove Z and pos if nested
        node_args = dict()
        node_args['id'] = str(self.id)
        if not nested:
            node_args['Z'] = str(self.z)
            node_args['p'] = str_node_pos

        # Add node LabelDisplay tag if string is marked as at left in the molecule.
        if 'LabelDisplay' in self.nx_graph.nodes[node_id]:
            kwargs['n']['LabelDisplay'] = 'Right'

        # Prepend the standard node attributes before passed attributes
        # Create node tag
        node_args.update( kwargs['n'] )
        kwargs['n'] = node_args
        node_tag = self.soup.new_tag("n", **kwargs['n'])

        # ONLY create text and style/string tags inside the node tag
        # if we have a labeled node (i.e., not a hidden carbon)
        if not text_label == None:

            # Add bounding box, if defined
            if not str_bb == "Undefined":
                # Note: Should be no side-effect for kwargs_t_tag;
                # dictionary elements copied to parameters.
                kwargs['t']['BoundingBox'] = str_bb
                   
            text_tag = self.soup.new_tag("t", **kwargs['t'])
            style_string_tag = self.soup.new_tag("s", size=append_pt( text_size ), **kwargs['s'])
            style_string_tag.string = text_label

            text_tag.append(style_string_tag)
            node_tag.append(text_tag)

        if 'annotation' in self.nx_graph.nodes[node_id]:
            kwargs['objecttag'] = dict()
            kwargs['objecttag']['TagType'] = 'Unknown'
            object_tag = self.soup.new_tag("objecttag", **kwargs['objecttag'])
            
            annotation =  self.nx_graph.nodes[node_id]['annotation'][1]
            ann_lbl_type = annotation['lbl_type']
            if ann_lbl_type.isdigit():
                object_tag['Name']='number'
                node_tag['AtomNumber'] = ann_lbl_type
            ann_node_pos = pos_string(self.nx_graph, node_id)
            if 'box' in annotation:
                kwargs['t']['BoundingBox'] = spaced_digits( scale_bbox(annotation['box'], self.scale_x, self.scale_y) )

            kwargs['t']['p'] = ann_node_pos
            
            text_tag = self.soup.new_tag("t", **kwargs['t'])

            style_string_tag = self.soup.new_tag("s", **kwargs['s'])
            style_string_tag.string = ann_lbl_type

            text_tag.append(style_string_tag)
            object_tag.append(text_tag)
            node_tag.append(object_tag)

        # Add node to fragment (molecule) tag, store numeric id for node name, increment the numeric node id.
        if parent_fragment != None:
            parent_fragment.append( node_tag )

        # Update node identifier book-keeping (id, Z, self.id_dict)
        self.id_dict[node_id] = self.id
        self.increment_id(nested)

        return node_tag

    ##################################
    # CDXML Node Generation Fns
    # (Hidden) Carbons, Atom, Unknown
    #################################

    def add_carbon_node(self, n):
        # Add carbon node ( hidden or single dot )
        kwargs_node = self.default_node_tag_attributes()

        # Text label and <t>, <s> attributes
        text_label = None
        if n.split('_')[0] == '•':
            text_label = '•'

        # Node tags: check for wedge bond;
        # add attribute to node tag dict if appropriate
        out_connections = self.nx_graph.neighbors(n)
        for out_con in out_connections:
            bnd_lbl = self.nx_graph.edges[(n, out_con)]['type']
            if bnd_lbl == 'S Wedge' or bnd_lbl == 'H Wedge':
                kwargs_node['n']['Geometry'] = "Tetrahedral"
                break

        _ = self.add_cdxml_node_tag(n, text_label, self.size, self.new_frag, **kwargs_node)

    def add_known_atom_node(self, n):
        # Add node for an atom
        # Default tag attributes
        kwargs_node = self.default_node_tag_attributes()
        
        # Text label and <t>, <s> attributes
        element = n.split('_')[0]
        if element == 'OH':
            element == 'O'

        text_size=point_size(self.nx_graph, n)
        text_label = element

        # Get atom number, update node attributes 
        atm_no = str(rdchem.Atom(element).GetAtomicNum())
        kwargs_node['n']['Element'] = atm_no
        kwargs_node['n']['NumHydrogens'] = 0
  
        _ = self.add_cdxml_node_tag(n, text_label, text_size, self.new_frag, **kwargs_node)

    def add_known_sequence_node(self, n):
        # Add node with a single atom accompanied by hydrogen (e.g., CH3) and optional charges
        try:
            #### Collect chemical information
            # Separate integer suffix from node name
            # Check whether removing hydrogens and charges leaves a single atom
            sequence = n.split('_')[0]
            element = strip_hydrogens_charges( sequence )
            if not element in self.elements:
                #pcheck('ELEMENT', element)
                raise Exception()  # Process as 'unknown', see below

            # Single atom if we reach this point; get atomic number
            atm_no = str(rdchem.Atom(element).GetAtomicNum())
            (total_hydrogens, count_charges) = get_sequence_hydrogens_and_charge( sequence )
        
            #### Generate attributes for tags
            kwargs_node = self.default_node_tag_attributes()

            # Text label and <t>, <s> attributes
            text_label = sequence
            text_size=point_size(self.nx_graph, n)

            # Set additional node attributes
            kwargs_node['n']['Element'] = atm_no
            kwargs_node['n']['NumHydrogens'] = str(total_hydrogens)
            kwargs_node['n']['Charge'] = str(count_charges)

            # Add node to fragment
            _ = self.add_cdxml_node_tag(n, text_label, text_size, self.new_frag, **kwargs_node)

        except Exception as e:            
            # Sequence has more than one atom other than hydrogen
            self.add_unknown_node(n)

    def add_circle(self, n):
        node = self.nx_graph.nodes[n]

        cx = node['pos'][0]
        cy = node['pos'][1]

        center_3d = spaced_digits([cx, cy, 0])

        squeeze = 7 # FIXME, is this needed?
        bbox = spaced_digits([(node['box'][2] * self.scale_x) - squeeze, cy, cx, cy])

        graphic = self.soup.new_tag("graphic", id=str(self.id), Z=str(self.z),
                        BoundingBox=bbox, GraphicType="Oval", OvalType="Circle",
                        Center3D=center_3d)
        
        self.increment_id()
        self.deferred_nodes.append(graphic)
        del graphic

    def add_unknown_node(self, n, label=None):
        # Default attributes
        kwargs_node = self.default_node_tag_attributes()
        
        # Text label and <t>, <s> attributes
        text_label = n.split('_')[0]
        if label:
            # e.g., used for SVG, to replace '-' by '\-'
            text_label = label
        text_size=point_size(self.nx_graph, n)

        # Additional node attributes (type, and display L-R)
        kwargs_node['n']['NodeType'] = 'Unspecified'

        # Add node to fragment
        _ = self.add_cdxml_node_tag(n, text_label, text_size, self.new_frag, **kwargs_node)

    ### Named/nicknamed nodes
    def add_nested_fragment_node_NEW(self, n):
        # Default attributes
        kwargs_node = self.default_node_tag_attributes()

        # Define outer node
        # Fragment if there are multiple elements/structures (like a chemical formula)
        # has more than one capital letter
        num_cap_letters = sum(1 for c in n if c.isupper())
        if num_cap_letters > 1:
            kwargs_node['n']['NodeType'] = "Fragment"
        else:
            kwargs_node['n']['NodeType'] = "Nickname"       

        # Text, <t>, <s> attributes
        text_label = n.split('_')[0]
        text_size = point_size(self.nx_graph, n)

        # Create outer node, store reference; &&
        # Create internal fragment and append inside outer node
        outer_node_tag = self.add_cdxml_node_tag(n, text_label, text_size, self.new_frag, **kwargs_node)
        internal_fragment = self.add_cdxml_fragment(nested=True)
        outer_node_tag.append(internal_fragment)

        #### Insert nodes of the abbreviation
        abbreviation = n.split('_')[0]
        nodes = self.abbrevs[ abbreviation ].nodes()
        (internal_node, int_node_ids) = (None, dict())

        for node in nodes:
            node_label = node.split('_')[0]

            # Default attributes, text string and size for internal nodes
            kwargs_node = self.default_node_tag_attributes()  
            text_label= None
            text_size = None

            if node_label == "*":  
                # Connection point
                kwargs_node['n']['NodeType'] = 'ExternalConnectionPoint'
                kwargs_node['n']['ExternalConnectionNum'] = '1'

            elif not node_label == 'C': 
                # Not a carbon or connection point
                (charge_char, element) = (None, None)
                if node_label in self.elements:
                    element = node_label
                else:
                    if '-' in node_label:
                        (charge_sign, charge_char) = (-1, '-')
                    elif '+' in node_label:
                        (charge_sign, charge_char) = (+1, '+')

                    if charge_char:
                        (element, number) = node_label.split(charge_char)
                        if number == "":
                            charge = 1
                        else:
                            charge = int(number) * charge_sign

                if element:
                    atm_no = str(rdchem.Atom(element).GetAtomicNum())
                    kwargs_node['n']['Element'] = atm_no
                    kwargs_node['n']['NumHydrogens'] = '0'
                    if charge_char:
                        kwargs_node['n']['Charge'] = charge
                    
                    text_label = element
                    text_size = point_size(self.nx_graph, n)

            try:
                internal_node = self.add_cdxml_node_tag(node, text_label, text_size, outer_node_tag.fragment, nested=True, **kwargs_node)
            except UnboundLocalError:
                print(f' The unmanned node is: {node}')
                exit(1) # With error code (non-zero)

        #### Write the bonds of abbreviation
        for bond in self.abbrevs[n.split('_')[0]].edges(data=True):
            b = int_node_ids[bond[0]]
            e = int_node_ids[bond[1]]
            int_b_tag = self.soup.new_tag("b", id=str(self.id), Z=str(self.z), B=str(b), E=str(e))
            self.increment_id()

            if bond[2]['type'] in self.bond_order.keys():
                b_type = self.bond_order[bond[2]['type']]
                int_b_tag['Order'] = b_type
            outer_node_tag.fragment.append(int_b_tag)
            #del int_b_tag

        #self.new_frag.append(base_n_tag)

    ##################################
    # Brackets / Markush Structures
    ##################################

    def add_brackets(self, n):
        # Add a bracket group
        # ASSUME that rest of nodes have been added
        node = self.nx_graph.nodes[n]
        
        # bracket usage, brackets on left and right.
        # Note that y needs to be reversed (upside down) for second bracket
        bracket_usage = node['bracket_usage']
        box1 = node['box1']
        box2 = node['box2']

        # Left bracket coordinates
        vertical_squeeze = 2 # points
        box_string1 = spaced_digits( [ (box1[2]* self.scale_x), 
                (box1[3]* self.scale_y) - vertical_squeeze, 
                (box1[2]* self.scale_x),
                (box1[1]* self.scale_y) + vertical_squeeze 
                ])

        # Create left bracket graphic
        graphic_id1 = str(self.id)
        graphic1 = self.soup.new_tag("graphic", id=graphic_id1, Z=str(self.z), 
                        BoundingBox=box_string1, GraphicType="Bracket", BracketType=node['bracket_type'])
        self.increment_id()

        # Right bracket coordinates
        box_string2 = spaced_digits([ box2[0]* self.scale_x,  
                (box2[1]* self.scale_y) + vertical_squeeze,
                box2[0]* self.scale_x, 
                (box2[3]* self.scale_y) - vertical_squeeze
                ]) 
        graphic_id2 = str(self.id)

        # Create right bracket graphic
        graphic2 = self.soup.new_tag("graphic", id=graphic_id2, Z=str(self.z), 
                        BoundingBox=box_string2, GraphicType="Bracket", BracketType=node['bracket_type'],
                        BracketUsage=bracket_usage)
        self.increment_id()

        if 'attach' in node:
            object_tag = self.soup.new_tag("objecttag", TagType="Unknown", Name="bracketusage")
            t_tag = self.soup.new_tag("t")
            s_tag = self.soup.new_tag("s", size=append_pt( str(int(self.size)/4*3) ), **self.kwargs_default_text)
            s_tag.string = node['attach']
            t_tag.append(s_tag)
            object_tag.append(t_tag)
            graphic2.append(object_tag)
        
        # Create bracket group and attachments
        inner = node['inner_objects']
        inner_obj_string = ""
        for obj in inner: #TODO fix bracket_attachments
            new_obj = self.id_dict[obj]
            inner_obj_string += str(new_obj) + " "
        
        bracket_group = self.soup.new_tag("bracketedgroup", id=str(self.id), BracketUsage=bracket_usage,
                        BracketedObjectIDs=inner_obj_string)
        self.increment_id(True)

        if bracket_usage == "MultipleGroup":
            bracket_group['RepeatCount'] = node['attach']
        elif bracket_usage == "SRU":
            bracket_group['SRULabel'] = node['attach']

        bracket_attach1 = self.soup.new_tag("bracketattachment", id=str(self.id), GraphicID=graphic_id1)
        self.increment_id(True)
        
        bracket_attach2 = self.soup.new_tag("bracketattachment", id=str(self.id), GraphicID=graphic_id2)
        self.increment_id(True)
        
        bracket_group.append(bracket_attach1)
        bracket_group.append(bracket_attach2)

        return (graphic1, graphic2, bracket_group)

    ################################################################
    # Visual Graph to CDXML Conversion
    ################################################################

    def create_node_tags( self, graph, create_nested_fragments=True, replace_dict={} ):
        # Creates tags for nodes
        # Returns whether a bracket was encountered
        #TODO charge
        bracket = False
        for n in graph.nodes:
            node_label = n.split('_')[0]

            # Add hidden carbon or dot carbon                
            if node_label  == 'C' or n.split('_')[0] =='•':
                self.add_carbon_node(n)
            
            # Add abbreviation/nickname node
            # SKIP if create_nested_fragments is False (e.g., for SVG generation)
            elif create_nested_fragments and node_label in self.abbrevs.keys():
                self.add_nested_fragment_node(n, self.embed_named_structures)
            
            # Add Atom Block
            elif node_label in self.elements:
                self.add_known_atom_node(n)
            
            # Add circle
            elif node_label == 'circle':
                self.add_circle(n)
            
            # Add bracket
            elif node_label == 'bracket':
                # Assume if there is a bracket, will only be one node
                self.new_frag = self.add_brackets(n)
                bracket = True

            # Add known sequence
            elif is_known_sequence(node_label):
                self.add_known_sequence_node(n)

            # Add unknown block/formula
            else:
                # RZ: Replace characters if patterns given, e.g., for molconvert SVG renders
                for pattern in replace_dict:
                    node_label = node_label.replace(pattern, replace_dict[pattern])
                self.add_unknown_node(n, label=node_label)

        return bracket

    def create_bond_tags( self, graph ):
        for bond in graph.edges(data=True):
            # Skip these
            # FIXME maybe do this step earlier?
            if bond[2]['type'] == 'circle_connection':
                continue

            # AKS: If directed edge (for wedge), use the correct direction
            edge = bond[2].get("dir_edge", bond)
            b = self.id_dict[edge[0]]
            e = self.id_dict[edge[1]]
            b_tag = self.soup.new_tag("b", id=str(self.id), B=str(b), E=str(e), Z=str(self.z))
            self.increment_id()

            # Double and triple bonds
            # Set bond tag attributes for bond type
            if bond[2]['type'] in self.bond_order.keys():
                b_type = self.bond_order[bond[2]['type']]
                b_tag['Order'] = b_type

            # Solid wedge
            elif bond[2]['type'] == 'S Wedge':
                b_tag['Display'] = "WedgeBegin"
                b_type = "-1"

            # Hashed wedge
            elif bond[2]['type'] == 'H Wedge':
                b_tag['Display'] = "WedgedHashBegin"
                b_type = "-1"

            # Wave bond
            elif bond[2]['type'] == 'Wave':
                b_tag['Display'] = "Wavy"
                b_type = "-1"

            # Dashed bond
            elif bond[2]['type'] == 'Dash':
                b_tag['Display'] = "Dash"
                b_type = "-1"

            # Single bond (default)
            else:
                b_type = None

            self.new_frag.append(b_tag)
            del b_tag
    
    def scale_points(self, scale_factor):
        # Scale node positions using a dictionary comprehension
        scaled_positions = {node: (x * scale_factor, y * scale_factor)
                            for node, (x, y) in nx.get_node_attributes(self.nx_graph, 'pos').items()}
        # Update the positions in the graph
        nx.set_node_attributes(self.nx_graph, scaled_positions, 'pos')

    def translate_points(self, yolo_bbox):
        translated_positions = {node: (x + yolo_bbox[0], y + yolo_bbox[1])
                            for node, (x, y) in nx.get_node_attributes(self.nx_graph, 'pos').items()}
        nx.set_node_attributes(self.nx_graph, translated_positions, 'pos')        

    def distance(self, p1, p2):            
        x1,y1 = p1
        x2,y2 = p2
        return math.sqrt((x2 - x1)**2 + (y2 - y1)**2) 

    def convert_graph(self, doc, page, no_id, svg=False):
        # Convert a single visual graph to a CDXML tag list
        self.nx_graph = self.dpr_vgraph_dict[doc][page][no_id] 
        # Scale points for graphs from visual parser (only SVG)
        scale_factor = self.nx_graph.graph.get('scale_factor', None)
        yolo_bbox = self.nx_graph.graph.get('yolo_bbox', None)        
        if not svg and yolo_bbox is not None:            
            self.translate_points(yolo_bbox)

        # if svg and scale_factor:
        #     self.scale_points(scale_factor)
        # if svg:
        #     self.scale_and_translate_points_to_1000X1000()


        self.z = 1   # reset to first layer; new molecule
        self.id_dict = {}    # Reset node name to integer identifier index

        # Create and initialize soup container
        # Page id -- set and increment id
        self.dpr_cdxml_dict[doc][page][no_id].soup.CDXML.page['id'] = str(self.id)      
        self.increment_id(True)
        
        self.soup_container = self.dpr_cdxml_dict[doc][page][no_id]
        self.soup = self.dpr_cdxml_dict[doc][page][no_id].soup

        # Scale the coords
        (self.canvas_size, self.page_size) = self.scale_coords()
        self.soup.CDXML['BoundingBox'] = self.canvas_size
        self.soup.CDXML.page['BoundingBox'] = self.page_size

        page_size = self.page_size.split()
        self.soup.CDXML.page['Width'] = str(page_size[2])
        self.soup.CDXML.page['Height'] = str(page_size[3])  

        # if directed, then cannot do connected components --
        #  Q. different function/conversion to undirected graph to address? 
        # ASSUME one graph
        if nx.is_directed(self.nx_graph):
            subgraphs = [self.nx_graph]
        else:
            subgraphs = [self.nx_graph.subgraph(c).copy() for c in nx.connected_components(self.nx_graph)]

        ### Main Conversion: CDXML fragments for molecules
        tags = list()
        bond_length_list = []
        for graph in subgraphs:
            if svg:                
                distances = [self.distance(graph.nodes[a]["pos"], graph.nodes[b]["pos"]) for a,b in list(graph.edges())]
                bond_length_list += distances

            # Create fragment for the molecule, and associated nodes and bonds
            # Creating nodes returns tags for bracket if present.
            self.new_frag = self.add_cdxml_fragment()  

            bracket = None
            if not self.gen_svg:
                bracket = self.create_node_tags( graph )
            else:
                bracket = self.create_node_tags( graph, create_nested_fragments=False, replace_dict = { '-': '\-' }  )
            self.create_bond_tags( graph )

            # Add all deferred nodes at end of current fragment
            for n in self.deferred_nodes:
                self.new_frag.append(n)
            self.deferred_nodes = []

            # Special case to handle brackets 
            # (in which case there is a list of items)
            if bracket:
                tags.extend(self.new_frag)
            else:
                tags.append(self.new_frag)
        
        if svg and len(bond_length_list)!=0:
            count_bonds = len(bond_length_list)
            self.dpr_cdxml_dict[doc][page][no_id].soup.CDXML["BondLength"] = str(sum(bond_length_list)/count_bonds)

        # Add tags to soup container
        self.soup_container.add_fragments(tags) 



################################################################
# CDXML_Bulk_Converter class for doc-page-region dictionary 
#   of visual graphs
################################################################

class CDXML_Bulk_Converter(object):
    def __init__(self, dpr_vgraph_dict, save_dir, metrics=None, dpi=300,
                 write_pages_out=0, verbose=True, fixed_attach=False, svg=False) -> None:
        # Class to manage and record results from bulk conversion of 
        # visual graphs to CDXML.
        self.save_dir = save_dir
        # self.metrics = { 'dud' : "PLACEHOLDER--REPLACE"}
        if metrics is not None:
            self.metrics = metrics
        else:
            self.metrics = defaultdict(dict)

        self.verbose = verbose

        # Input graphs and output CDXML text organized by document-page-region
        self.dpr_vgraph_dict = dpr_vgraph_dict
        self.converters = dict()
        self.dpr_cdxml_dict = None
        self.write_pages_out = write_pages_out

        self.fixed_attach = fixed_attach

        self.gen_svg=svg

    def generate_cdxml(self):
        # Generate CDXML for all documents
        for doc in self.dpr_vgraph_dict:
            self.converters[doc] = CDXML_Converter(self.save_dir,
                                                   self.dpr_vgraph_dict, doc,
                                                   write_pages_out=self.write_pages_out,
                                                   verbose=self.verbose,
                                                   fixed_attach=self.fixed_attach,
                                                   gen_svg=self.gen_svg) 
            # HACK -- store last result THIS WILL NOT WORK FOR MULTIPLE FILES
            self.dpr_cdxml_dict = self.converters[doc].dpr_cdxml_dict
            # self.metrics = self.converters[doc].metrics
            # AKS: Added support for multiple files
            self.metrics[doc].update(self.converters[doc].metrics[doc])



################################################################
# Generating CDXML files
################################################################

# Converts visual graphs to CDXML files (for ONE document currently)
def visual2cdxml(dpr_vgraph_dict, out_dir=None, metrics=None, 
        valid_dict={}, errors_dict={}, parallel=False, write_pages_out=0,
         verbose=True, fixed_attach=False, svg=False):

    all_cdxmls = dict()
    unconverted = dict()
    # Moving 'out' -- save_dir must change to modify by document.
    bulk_converter = CDXML_Bulk_Converter(dpr_vgraph_dict, out_dir,
                                          write_pages_out=write_pages_out,
                                          verbose=verbose, metrics=metrics, 
                                          fixed_attach=fixed_attach, svg=svg)  # Added for new metrics
    bulk_converter.generate_cdxml()

    total_molecules = 0

    # Report conversion numbers
    DEBUG_THIS = False # HACK
    if DEBUG_THIS:
        check('docs', [doc for doc in  bulk_converter.dpr_cdxml_dict])
        for doc in bulk_converter.dpr_cdxml_dict:
            check('pages', [page for page in bulk_converter.dpr_cdxml_dict[doc]])
            for page in bulk_converter.dpr_cdxml_dict[doc]:
                #check('no. molecules', len(bulk_converter.dpr_cdxml_dict[doc][page]))
                total_molecules += len(bulk_converter.dpr_cdxml_dict[doc][page])
            check('Total molecules', total_molecules)
    
    return bulk_converter


