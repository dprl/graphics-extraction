###########################################################################
#
# nx_merge.py
#   - routines for filtering nodes/edges and merging connected components
#
###########################################################################
# Created by: Richard Zanibbi, Jun 11 2023 10:25:54
###########################################################################

import networkx as nx
import copy
import functools
from nx_debug_fns import *

def node_attr_exclude( G, name, value ):
    # Match nodes with attribute value
    def f( n ):
        if name in G.nodes[n]:
            return G.nodes[n][name] != value
        else:
            return True
    return f

def node_attrs_exclude( G, name, value1, value2 ):
    # Match nodes with attribute value
    def f( n ):
        if name in G.nodes[n]:
            return G.nodes[n][name] != value1 and G.nodes[n][name] != value2
        else:
            return True
    return f

def node_attr_match( G, name, value ):
    # Match nodes with attribute value
    def f( n ):
        if name in G.nodes[n]:
            return G.nodes[n][name] == value
        else:
            return False
    return f

def node_attrs_match( G, name, value1, value2 ):
    # Match nodes with attribute value
    def f( n ):
        if name in G.nodes[n]:
            return G.nodes[n][name] == value1 or G.nodes[n][name] == value2
        else:
            return False
    return f

def node_attr_substring( G, name, value ):
    # Match nodes with attribute value substring
    def f( n ):
        if name in G.nodes[n]:
            return value in G.nodes[n][name]
        else:
            return False
    return f

def edge_attr_exclude( G, name, value ):
    def e( n1, n2 ):
        if name in G[n1][n2]:
            return G[n1][n2][name] != value
        else:
            return True
    return e


def floating_edge_candidate( G ):
    def e( n1, n2 ):
        if len(n1.split("_")[0]) == 1 or len(n2.split("_")[0]) == 1:
            return True
        else:
            return False
    return e

def edge_2attr_match( G, name1, value1, name2, value2 ):
    def e( n1, n2 ):
        if ((G.nodes[n1][name1] in value1 and G.nodes[n2][name2] in value2) or
                (G.nodes[n2][name1] in value1 and G.nodes[n1][name2] in value2)):
            return True
        else:
            return False
    return e

def edge_attr_match( G, name, value ):
    def e( n1, n2 ):
        if name in G[n1][n2]:
            return G[n1][n2][name] == value
        else:
            return False
    return e

def edge_attr_substring( G, name, value ):
    # Match edges with attribute value substring
    def e( n1, n2 ):
        if name in G[n1][n2]:
            return value in G[n1][n2][name] 
        else:
            return False
    return e

def select_main_atom(M):
    def f(nodes):
        for node in sorted(nodes):
            connected_nodes = dict(map(lambda x:(x, nx.get_node_attributes(M, "obj_type")[x]),
                                       list(M[node])))
            # If any of the connected node is not among the CCs, and not a
            # character, it should be the main atom connected to a line/bond
            if any(list(map(lambda x: x[0] not in nodes and x[1] != "Char",
                            connected_nodes.items()))):
                return node
        return None
    return f

def select_left_topmost( G ):
    # Returns binary comparator for min_x coordinates
    # in bounding box ('box')
    def f(node_1, node_2 ):
        minx_1 = G.nodes[node_1]['box'][0]
        minx_2 = G.nodes[node_2]['box'][0]

        if minx_1 == minx_2:
            miny_1 = G.nodes[node_1]['box'][1]
            miny_2 = G.nodes[node_2]['box'][1]

            if miny_1 <= miny_2:
                return node_1
            else:
                return node_2

        if minx_1 <= minx_2:
            return node_1
        else:
            return node_2

    return f

def select_leftmost( G ):
    # Returns binary comparator for min_x coordinates
    # in bounding box ('box')
    def f(node_1, node_2 ):
        minx_1 = G.nodes[node_1]['box'][0]
        minx_2 = G.nodes[node_2]['box'][0]

        if minx_1 <= minx_2:
            return node_1
        else:
            return node_2

    return f

def select_max_degree( G ):
    # Return node with larger degree
    def f(node_1, node_2):
        out_node = node_1
        if G.degree[node_1] < G.degree[node_2]:
                # NOT TRIED: incorporating line length as another constraint
               # or G.nodes[node_1]['length'] < G.nodes[node_2]['length']:
            out_node = node_2
        return out_node

    return f

def select_all_lines(G):
    def f(n):
        lbl = G.nodes[n]['lbl_cls']
        if lbl in ['Ln', 'Wave', 'SW', 'HW']:
            return True
        return False
        
    return f

def select_no_lines( G ):
    def f(n):
        lbl = G.nodes[n]['lbl_cls']
        if lbl in ['Ln', 'Wave', 'SW', 'HW']:
            return False
        return True
    return f

def contract_ccs( graph_view, G, btselect=min, btselect_alt=None ):
    # By default, assume node labels are integers, use smallest id 
    # as node that CCs are contracted to
    CCs = list(nx.connected_components(graph_view))

    # Return a copy -- no in-place edits
    H = copy.deepcopy(G)

    removed_nodes_parents = {}
    for cc in CCs:
        cc_list = list(cc)

        # Select node to be target/parent for CC group ('collapse to this')
        # Folds a binary selection function (e.g., min) across node list
        if btselect_alt:
            target = btselect_alt(cc_list)
        if btselect_alt is None or not target:
            target = functools.reduce( btselect, cc_list )
        cc_list.remove( target )
    
        for n in cc_list:
            removed_nodes_parents[n] = target
            H = nx.contracted_nodes(H, target, n, copy=True)

    if H.graph.get("removed_nodes_parent", None):
        H.graph["removed_nodes_parent"].update(removed_nodes_parents)
    else:
        H.graph["removed_nodes_parent"] = removed_nodes_parents
    return H

def select_and_contract_nodes( G, f_node, f_edge=None, btselect=min,
                              btselect_alt=None ):

    if not f_edge:
        graph_view = nx.subgraph_view(G, filter_node=f_node )
    else:
        graph_view = nx.subgraph_view(G, filter_node=f_node, filter_edge=f_edge )

    # H is a modified copy of G.
    H = contract_ccs( graph_view, G, btselect, btselect_alt )

    return H

##################################
# Unit test
##################################

def nx_merge_test():

    G = nx.Graph()
    G.add_edges_from([
         (1,2),
         (1,3),
         (1,4),
         (3,4),
         (4,5)
        ])

    G.nodes[1]['type'] = 'char'
    G.nodes[2]['type'] = 'char'
    nx.set_node_attributes(G, {3: 'line', 4: 'line', 5: 'line'}, name='type')  

    G[1][2]['etype'] = 'cool'
    G[3][4]['etype'] = 'cool'

    f_node = node_attr_match( G, 'type', 'char' )
    f_node_line = node_attr_match( G, 'type', 'line' )
    f_edge = edge_attr_match( G, 'etype', 'cool' )


    x = nx.subgraph_view(G, filter_node=f_node)
    yy = contract_ccs(x, G, max )
    L = [
            (G, 'Original'),
            (x, 'Filtered for Chars (1,2)'),
            (yy, 'Contracted Chars (1,2) in Source Graph (G)'),
            (G, 'Original Graph G AFTER contraction'),
        ]

    draw_vgtlist( L )
    draw_vgtlist( L, file_name='test_multi.pdf')

    #----

    # Experiments with filters and contracting nodes
    y = nx.contracted_nodes(G, 1, 2)
    z = nx.subgraph_view(G, filter_edge=f_edge)
    a = nx.subgraph_view(G, filter_node=f_node, filter_edge=f_edge)
    b = nx.contracted_nodes(a,1,2)

    # Experiments with connected components and contracting nodes
    c = list(nx.connected_components(z))
    #show_gdescr(z)
    #print("Connected components:", c)

    d = contract_ccs( z, z, max )
    e = nx.subgraph_view(G, filter_node=f_node_line)
    f = contract_ccs( e, e, max )

    F = [
            (G, 'Original graph'),
            (x,'Filtered for characters'),
            (e, 'Line nodes only'),
            (y,'Contracting (1,2)'),
            (G, 'Original graph AFTER contracting (1,2)'),
            (z, 'Filtered for "cool" edges'),
            (a, 'Filtered for "char" nodes, "cool" edges'),
            (d, 'Contracted CCs after filtering for "cool" edges'),
            (b, 'Filter (char nodes, cool edges) + contract (1,2)'),
            (f, 'Contract CCs for Line nodes only')
        ]                    

    draw_vgtlist( F )

if __name__ == '__main__':
    nx_merge_test()
