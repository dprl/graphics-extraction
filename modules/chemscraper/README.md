## Manual Installation (Conda)

```console
foo@bar:~/.../chem_data_alignment$ conda create -n cext python==3.6.9
foo@bar:~/.../chem_data_alignment$ conda activate cext
(cext) foo@bar:~/.../chem_data_alignment$ pip install -r requirements.txt  
```

### Marvin Suite Installation (Ubuntu) (For Exporting SMILES of extracted Molecules in TSV)
**Pre-Requisite:** Make sure you have Oracle Java (NOT OpenJDK) installed on your system and the `$JAVA_HOME` env variable contains the path to Java. For me, it was `'/usr/'`. You can download the .deb package from [here](https://www.oracle.com/java/technologies/downloads/).

* Sign-up for a ChemAxon Account [here](https://account.chemaxon.com/register)
* You can then download and install the Marvin .deb package from [here](https://chemaxon.com/products/marvin/download#deb) and obtain a non-commercial license 
    * If you get `JAVA Not Found` or `JAVA version not supported error` make sure the right Java is being used - you can check which java is being used by `sudo update-alternatives --config java`

## Testing Extraction and Alignment

For now, all the raw data required for testing has been included in the repository. An example command to run and see 
example visualizations is:

```console
foo@bar:~/.../chem_data_alignment$ python one_pdf_test.py --viz_matched 1 --viz_visual_graph 1 --viz_mol 1 --wr_lg 0
```

The different args that can be used are as follows:

* `ss_xml` - Symbol Scraper XML output for PDF. Default: `data/all_or100.09.tables.xml`
* `cdxmls` - Folder containing the CDXMLs for the PDF. Default: `Table_cdxml_files/Table_cdxml_files`
* `img_dir` - Directory containing 300 DPI images of the PDF. Default: `data/images_300dpi/or100.09.tables`
* `pdf_path` - PDF file
* `viz_matched` - `0` or `1`
* `viz_visual_graph` - `0` or `1`
* `viz_mol` - `0` or `1`
* `wr_lg` - Generate Train and Test LG files. `0` or `1`
* `wr_tsv` - Generate a TSV for all molecules. `0` or `1`
* `save_dir` - Directory to save the generated TSV or LG files. _Note:_ LG files are split into train and test images and LGs
* `cond` - Extract reaction conditions for all the reactions. The molecules are split into a roughly 80-20 split between train-test. Seed for reproducibility is located at `write_data()` in the main code (`one_pdf_test.py`). `0` or `1`