import pandas as pd
from glob import glob
import os

def group1_():
    path = "GRID_SEARCH_IJDAR_REVIEW"
    csv_list = glob(os.path.join(path,"*","smiles_meta.csv"))
    pandas = []
    for csv_file in csv_list:
        pandas.append(pd.read_csv(csv_file))
    concatenated = pd.concat(pandas,ignore_index=True).drop(columns=["error"])
    just_exact_matches = concatenated[["PARALLEL_TOLERANCE","CLOSE_NONPARALLEL_ALPHA","CLOSE_CHAR_LINE_ALPHA","levenshtein_normalized"]]
    just_exact_matches = just_exact_matches[just_exact_matches.levenshtein_normalized == 1.0]
    grouped = just_exact_matches.groupby(["PARALLEL_TOLERANCE","CLOSE_NONPARALLEL_ALPHA","CLOSE_CHAR_LINE_ALPHA"]).agg({'levenshtein_normalized':'count'})
    grouped = grouped.reset_index()
    grouped.columns = ["PARALLEL_TOLERANCE","CLOSE_NONPARALLEL_ALPHA","CLOSE_CHAR_LINE_ALPHA","exact_matches"]
    grouped = grouped.sort_values(by=["exact_matches"], ascending=False)

def group2():
    path = "GROUP2_GRID"
    csv_list = glob(os.path.join(path,"*","smiles_meta.csv"))
    pandas = []
    for csv_file in csv_list:
        pandas.append(pd.read_csv(csv_file))
    concatenated = pd.concat(pandas,ignore_index=True).drop(columns=["error"])
    just_exact_matches = concatenated[["LONGEST_LENGTHS_DIFF_RATIO","NEG_CHARGE_Y_POSITION","NEG_CHARGE_LENGTH_TOLERANCE","levenshtein_normalized"]]
    just_exact_matches = just_exact_matches[just_exact_matches.levenshtein_normalized == 1.0]
    grouped = just_exact_matches.groupby(["LONGEST_LENGTHS_DIFF_RATIO","NEG_CHARGE_Y_POSITION","NEG_CHARGE_LENGTH_TOLERANCE"]).agg({'levenshtein_normalized':'count'})
    grouped = grouped.reset_index()
    grouped.columns = ["LONGEST_LENGTHS_DIFF_RATIO","NEG_CHARGE_Y_POSITION","NEG_CHARGE_LENGTH_TOLERANCE","exact_matches"]
    grouped = grouped.sort_values(by=["exact_matches"], ascending=False)
    import pdb;pdb.set_trace()

def group3():
    path = "DEBUG/GROUP_3"
    csv_list = glob(os.path.join(path,"*","smiles_meta.csv"))
    pandas = []
    for csv_file in csv_list:
        pandas.append(pd.read_csv(csv_file))
    
    concatenated = pd.concat(pandas,ignore_index=True).drop(columns=["error"])
    just_exact_matches = concatenated[["COS_PRUNE","REMOVE_ALPHA","Z_TOLERANCE","levenshtein_normalized"]]
    just_exact_matches = just_exact_matches[just_exact_matches.levenshtein_normalized == 1.0]
    grouped = just_exact_matches.groupby(["COS_PRUNE","REMOVE_ALPHA","Z_TOLERANCE"]).agg({'levenshtein_normalized':'count'})
    grouped = grouped.reset_index()
    grouped.columns = ["COS_PRUNE","REMOVE_ALPHA","Z_TOLERANCE","exact_matches"]
    grouped = grouped.sort_values(by=["exact_matches"], ascending=False)
    import pdb;pdb.set_trace()

def group4():
    path = "DEBUG/GRID_PARAMS_CORRECTED"
    csv_list = glob(os.path.join(path,"*","smiles_meta.csv"))
    pandas = []
    for csv_file in csv_list:
        pandas.append(pd.read_csv(csv_file))
    
    concatenated = pd.concat(pandas,ignore_index=True).drop(columns=["error"]) 
    just_exact_matches = concatenated[concatenated.levenshtein_normalized == 1.0]   
    grouped = just_exact_matches.groupby(["relative_thickness","implicit_hydrogens_visible","label_mode"]).agg({'levenshtein_normalized':'count'})
    grouped = grouped.reset_index()
    import pdb;pdb.set_trace()

def dataset_for_visual():
        
    dataset = pd.read_csv("outputs/test_indigo_uspto_for_visual/smiles_meta_test_indigo_uspto_for_visual.csv")    
    # just_exact_matches = concatenated[["LONGEST_LENGTHS_DIFF_RATIO","NEG_CHARGE_Y_POSITION","NEG_CHARGE_LENGTH_TOLERANCE","levenshtein_normalized"]]
    just_exact_matches = dataset[dataset.levenshtein == 0]
    
    final = just_exact_matches[["Unnamed: 0","ground_truth"]]
    final.columns = ["ID","SMILES"]        
    final.to_csv("test_indigo_uspto_for_visual_exact_matches.csv",index=False)
    print(len(final.index))
    

def main():
    group4()

    
    
    

if __name__ == "__main__":
    main()
