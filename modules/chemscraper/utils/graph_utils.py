import numpy as np
import networkx as nx

# Get the boxes from all nodes and sort order for a graph
# (assumes nodes will be sorted to always preserve initial ordering)
def sorted_order(g):
    # Sort the nodes with x_axis first
    boxes = []
    objs = []
    for n in sorted(g.nodes()):
        bbox = g.nodes[n]['box']
        obj = g.nodes[n]['obj']
        boxes.append(bbox)
        objs.append(obj)
    boxes = np.array(boxes)
    objs = np.array(objs)
    order = np.argsort(boxes[:, 0])

    return order, boxes, objs