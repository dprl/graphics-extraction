
# RZ
from debug_fns import *

import numpy as np
import networkx as nx
from shapely.geometry import Point, Polygon, LineString
import copy
import time
import math
from modules.chemscraper.utils.viz_utils import viz_translated_cd

class PostProcess_Ops():
    def __init__(self, idx, page_id, initial_mst, dir_mst, adj_matrix, fc_graph, cd_graph) -> None:
        # RZ Addition
        self.idx = idx
        self.page_id = page_id

        self.mst = initial_mst
        self.dir_mst = dir_mst
        self.adj_matrix = adj_matrix
        self.fc_graph = fc_graph
        self.cd_graph = cd_graph
        
        # Define the possible line signatures types from SS
        self.line_signatures = ['Ln', 'Wave', 'Circle', 'SW', 'HW']
        self.new_signatures = ['Sing', 'Doub', 'Trip', 'Wave', 'Circle', 'SW', 'HW']
        self.line_type = {1:'Single', 2:'Double', 3:'Triple', 4:'Wave',
                            5:'S Wedge', 6: 'H Wedge'}
        self.comp_line_type = {'Sing':'Single', 'Doub':'Double', 'Trip':'Triple', 'Wave':'Wave',
                               'SW': 'S Wedge', 'HW':'H Wedge'}
    
    def find_centroid(self):
        all_n_cents = {}
        val = True
        all_polys = {}
        for node in self.mst.nodes(data=True):
            # TODO: Ignoring Circle cases for now
            if node[0].split('_')[0] == 'Circle' or node[0].split('_')[0] == 'Wave':
                val = False
            if '+' in node or '-' in node:
                val = False
            # Check for the dot unicode
            for char in node[0]:
                if ord(char) == 8226:
                    val = False
                    break
            if not val:
                break
            poly = node[1]['poly']
            all_polys[node[0]] = poly
            cent = poly.centroid
            all_n_cents[node[0]] = cent
        
        self.all_n_cents = all_n_cents
        self.all_polys = all_polys
        
        return val
    
    def find_char_clusters(self):
        vi_ind_char_nodes = []
        char_clusters = []
        for node in self.mst.nodes():
            # MOD: Compensate for not in order characters (for Pipeline mode update)
            cur_box = self.mst.nodes[node]['box']
            cur_neis = self.mst.neighbors(node)
            ln_cnt = 0
            chr_cnt = 0
            cor_char = False
            for n in cur_neis:
                if n.split('_')[0] in self.line_signatures:
                    ln_cnt += 1
                else:
                    chr_cnt += 1
                    # Check if the neighbor is left or right of cur node
                    n_box = self.mst.nodes[n]['box']
                    if n_box[0] > cur_box[0]:
                        cor_char = True
                
            if (ln_cnt == 0 and chr_cnt == 1) and cor_char:
                valid_start = True
            elif ln_cnt <= 2 and chr_cnt == 0:
                valid_start = True
            elif ln_cnt == 1 and chr_cnt == 1 and cor_char:
                valid_start = True
            else:
                valid_start = False
            if node.split('_')[0] not in self.line_signatures and \
                    node not in vi_ind_char_nodes and valid_start:
                visited_char_nodes = []        
                one_char_nodes, one_vi_ind_char_nodes = self.custom_bfs(self.mst, node, 
                                                    visited_char_nodes, vi_ind_char_nodes)
                char_clusters.append(one_char_nodes)
                vi_ind_char_nodes += one_vi_ind_char_nodes
        self.char_clusters = char_clusters


    def custom_bfs(self, mst, node, visited_char_nodes, vi_ind_char_nodes):
        # print(f'BFS Called on node: {node}')
        visited_char_nodes.append(node)
        vi_ind_char_nodes.append(node)
        # Get the neighbors of the node
        neighbors = mst.neighbors(node)
        # print('Neighbors')
        nxt_node = ''
        for n in neighbors:
            # print(n)
            # Check if node has been visited before OR not a bond line
            if n not in vi_ind_char_nodes:
                if n.split('_')[0] not in self.line_signatures:
                    nxt_node = n
                    # print(f'Next Node: {nxt_node}')
                    break
        
        if len(nxt_node) != 0:
            visited_char_nodes, vi_ind_char_nodes = self.custom_bfs(mst, nxt_node, 
                                                        visited_char_nodes,
                                                        vi_ind_char_nodes)
        return visited_char_nodes, vi_ind_char_nodes
    

    def in_ring_clusters(self, p, idx):
        in_ring_bool = [0]* len(self.char_clusters)
        for idx_clus, clus in enumerate(self.char_clusters):
            # Only one character long clusters CAN be an in-ring
            if len(clus) > 1:
                continue
            else:
                ln_neighbors = 0
                n_iter = self.mst.neighbors(clus[0])
                for nei in n_iter:
                    if nei.split('_')[0] in self.line_signatures:
                        ln_neighbors += 1
                if ln_neighbors >= 2:
                    in_ring_bool[idx_clus] = 1
        self.in_ring_bool = in_ring_bool
        # if p == 8 and idx == 15:
        #     print(self.char_clusters)
        #     print(self.in_ring_bool)
        #     # print(self.mst.edges())
        #     exit(0)
    
    
    def find_cluster_lines(self):
        c_clus_bonds = {}
        all_clus_lines = []
        for idx_clus, clus in enumerate(self.char_clusters):
            # Check if the cluster is in-ring or not
            if not self.in_ring_bool[idx_clus]:
                found = False
                lines = []
                for node in clus:
                    # There should only be one line neighbor out of all the char nodes
                    n_iter = self.mst.neighbors(node)
                    for nei in n_iter:
                        # If it is THE line, get the centroid distance of both
                        if nei.split('_')[0] in self.line_signatures:
                            cent_line = self.all_n_cents[nei]
                            cent_node = self.all_n_cents[node]
                            dist = cent_node.distance(cent_line)
                            lines.append(nei)
                            if nei not in all_clus_lines:
                                all_clus_lines.append(nei)

                            # Now find if there are any extra connections
                            for n in self.all_n_cents.keys():
                                if n != node and n!= nei and n.split('_')[0] in self.line_signatures:
                                    cur_cent = self.all_n_cents[n]
                                    cur_dist = cent_node.distance(cur_cent)
                                    if dist-5 <= cur_dist <= dist+5:
                                        lines.append(n)
                                        if n not in all_clus_lines:
                                            all_clus_lines.append(n)
                            found = True
                            break
                    if found:
                        break
                # NOTE: Ignoring floating clusters for now
                if not len(lines) == 0:
                    c_clus_bonds[idx_clus] = lines
        self.c_clus_bonds = c_clus_bonds
        self.all_clus_lines = all_clus_lines


    def find_same_main_bond_lines(self):
        same_bond_lines = []
        all_same_bond_lines = []
        for node in self.mst.nodes():
            if (node not in self.all_clus_lines and \
                    node not in all_same_bond_lines) or \
                        ([node] in self.char_clusters):
                if [node] in self.char_clusters:
                    if self.in_ring_bool[self.char_clusters.index([node])] == 1:
                        all_same_bond_lines.append(node)
                        same_bond_lines.append([node])
                else:
                    if node.split('_')[0] in self.line_signatures:
                        all_same_bond_lines.append(node)
                        cent_node = self.all_n_cents[node]
                        max_dist = 15
                        # for nei in node_neighs:
                            # nei_cent = all_n_cents[nei]
                            # comp_dist = cent_node.distance(nei_cent)
                            # if comp_dist > max_dist:
                            #     max_dist = comp_dist
                        # Now check if any other line node falls within the
                        # min_dist of being the same bond
                        # same_bond = [node]
                        found = False
                        for idx_sb, same_bond in enumerate(same_bond_lines):
                            for bnd in same_bond:
                                bnd_cent = self.all_n_cents[bnd]
                                comp_node_dist = bnd_cent.distance(cent_node)
                                if comp_node_dist <= max_dist:
                                    same_bond_lines[idx_sb].append(node)
                                    found = True
                                    break
                            if found:
                                break
                        if not found:
                            same_bond_lines.append([node])

        self.same_bond_lines = same_bond_lines
        self.all_same_bond_lines = all_same_bond_lines
    

    def strip_id(self):
        final_char_clusters = []
        for char_clus in self.char_clusters:
            # Strip the IDs
            cur_clus = []
            for c in char_clus:
                cur_clus.append(c.split('_')[0])
            final_char_clusters.append(''.join(cur_clus))
        self.final_char_clusters = self.char_clusters
        self.char_clusters = final_char_clusters
    

    def id_duplicate(self):
        if len(set(self.char_clusters)) != len(self.char_clusters):
                import collections
                duplicates = [clus for clus, count in collections.Counter(self.char_clusters).items()
                                    if count > 1]
                dup_dict = {dup:0 for dup in duplicates}
                new_clusters = []
                for clus in self.char_clusters:
                    if clus in duplicates:
                        new_clusters.append(clus+'_'+str(dup_dict[clus]))
                        dup_dict[clus] += 1
                    else:
                        new_clusters.append(clus)
                self.char_clusters = new_clusters
    

    ''' 
        Find the vertices for a specific MST graph
        For cluster lines, select mean, else select the non-floating line
    '''
    def find_largest_side_vertices(self):

        # Get the vertices for the main bond lines first
        all_vertices = {}
        for lines in self.same_bond_lines:
            if lines[0].split('_')[0] in self.line_signatures:
                if len(lines) == 1:
                    cur_poly = self.all_polys[lines[0]]
                else: # Find the non floating line and its vertices
                    found = False
                    for idx_line, line in enumerate(lines):
                        edges = self.mst.edges(line, data=True)
                        weights = []
                        for edge in edges:
                            weights.append(edge[2]['weight'])
                        min_weights = [w for w in weights if w < 0.5]
                        if len(min_weights) != 0:
                            found = True
                            break
                    if not found:
                        check(f'>> polygon_ops.py: WARNING - defaulting to first line\n   No connecting main lines for {lines} in MST (page_id, idx)', (self.page_id, self.idx))
                        cur_poly = self.all_polys[lines[0]]
                    else:
                        cur_poly = self.all_polys[lines[idx_line]]

                min_rect = cur_poly.minimum_rotated_rectangle
                if isinstance(min_rect, LineString):
                    x,y = min_rect.coords.xy
                    final_verts = [[x[0],y[0]],[x[1],y[1]]]
                else:
                    final_verts = self.find_vertices(min_rect)
                for line in lines:
                    all_vertices[line] = final_verts
            else: # In main chain character
                cur_poly = self.all_polys[lines[0]]
                centroid = [cur_poly.centroid.coords.xy[0][0], cur_poly.centroid.coords.xy[1][0]]
                all_vertices[lines[0]] = [centroid, centroid]
            
        
        # Now get the mean of lines connecting sub-atom clusters
        # Get the clus list in iterable order (from the dict)
        all_clus_list = []
        for clus_id in self.c_clus_bonds.keys():
            bonds = self.c_clus_bonds[clus_id]
            all_clus_list.append(bonds)
        
        for clus_lines in all_clus_list:
            if clus_lines[0].split('_')[0] == 'SW' or clus_lines[0].split('_')[0] == 'HW':
                final_verts = self.find_wedge_verts(clus_lines[0])
            else:
                polys = []
                for clus_line in clus_lines:
                    cur_poly = self.all_polys[clus_line]
                    min_rect = cur_poly.minimum_rotated_rectangle
                    if isinstance(min_rect, LineString):
                        x,y = min_rect.coords.xy
                        final_verts = [[x[0],y[0]],[x[1],y[1]]]
                    else:
                        final_verts = self.find_vertices(min_rect)
                    polys.append(final_verts)
                # mean_vert1 = [0,0]
                # mean_vert2 = [0,0]
                # for poly in polys:
                #     mean_vert1[0] += poly[0][0]
                #     mean_vert1[1] += poly[0][1]
                #     mean_vert2[0] += poly[1][0]
                #     mean_vert2[1] += poly[1][1]
                # assert len(polys) != 0, f'0 for {all_clus_list} with cluster bonds \n {self.c_clus_bonds} \n with char clusters {self.char_clusters} \n with nodes: \n {self.mst.nodes()}'
                # mean_vert1 = np.array(mean_vert1)/len(polys)
                # mean_vert2 = np.array(mean_vert2)/len(polys)
                vert1s = []
                vert2s = []
                for poly in polys:
                    vert1s.append(poly[0])
                    vert2s.append(poly[1])
                vert1s = np.array(vert1s).reshape((-1,2))
                vert2s = np.array(vert2s).reshape((-1,2))
                vert1 = np.mean(vert1s, axis=0)
                vert2 = np.mean(vert2s, axis=0)
                final_verts = [list(vert1), list(vert2)]

            for clus_line in clus_lines:
                all_vertices[clus_line] = final_verts
        
        self.vert_polys = all_vertices
    

    '''
        From the minimum rectangle, find the vertices of the largest side,
        Order the vertices top-down or left-right
    '''
    def find_vertices(self, min_rect):
        x,y = min_rect.exterior.coords.xy
        edges = (Point(x[0], y[0]).distance(Point(x[1], y[1])), Point(x[1], y[1]).distance(Point(x[2], y[2])))
        max_edge_idx = edges.index(max(edges))
        vertices = [[x[max_edge_idx], y[max_edge_idx]],[x[max_edge_idx+1], y[max_edge_idx+1]]]
        found_x = False
        if vertices[0][0] < vertices[1][0]:
            final_verts = [vertices[0], vertices[1]]
            found_x = True
        elif vertices[0][0] > vertices[1][0]:
            final_verts = [vertices[1], vertices[0]]
            found_x = True
        
        if not found_x:
            if vertices[0][1] > vertices[1][1]:
                final_verts = [vertices[0], vertices[1]]
            elif vertices[0][1] < vertices[1][1]:
                final_verts = [vertices[1], vertices[0]]
            
        return final_verts
    
    def find_wedge_verts(self, node):
        cur_poly = self.all_polys[node]
        min_rect = cur_poly.minimum_rotated_rectangle
        x,y = min_rect.exterior.coords.xy
        edge_dists = (Point(x[0], y[0]).distance(Point(x[1], y[1])), Point(x[1], y[1]).distance(Point(x[2], y[2])),
                      Point(x[2], y[2]).distance(Point(x[3], y[3])), Point(x[3], y[3]).distance(Point(x[0], y[0])))
        min_edge_idx1 = edge_dists.index(min(edge_dists))
        min_edge_idx2 = (min_edge_idx1 + 2) % 4
        verts1 = [[x[min_edge_idx1],y[min_edge_idx1]], [x[min_edge_idx1+1],y[min_edge_idx1+1]]]
        verts2 = [[x[min_edge_idx2],y[min_edge_idx2]], [x[min_edge_idx2+1],y[min_edge_idx2+1]]]
        verts1_mid = [((verts1[0][0]+verts1[1][0])/2)+2, ((verts1[0][1]+verts1[1][1])/2)+2]
        verts2_mid = [((verts2[0][0]+verts2[1][0])/2)+2, ((verts2[0][1]+verts2[1][1])/2)+2]
        vertices = [verts1_mid, verts2_mid]

        return vertices
        

    def char_only_bonds(self):
        bonds = []
        clus_ids = sorted(list(self.c_clus_bonds.keys()))
        for i in range(len(clus_ids)):
            for j in range(i+1,len(clus_ids)):
                cur_bond_lines = sorted(self.c_clus_bonds[i])
                comp_bond_lines = sorted(self.c_clus_bonds[j])
                # Check if all bonds match
                if cur_bond_lines == comp_bond_lines:
                    bonds.append([i,j,len(cur_bond_lines), cur_bond_lines[0]])
                else: # Check for individual matches: Occurs due to MST construction (single-double)
                    cur_bond_lines_set = set(cur_bond_lines)
                    comp_bond_lines_set = set(comp_bond_lines)
                    common_bonds = cur_bond_lines_set & comp_bond_lines_set
                    if len(common_bonds):
                        bonds.append([i,j,len(common_bonds),common_bonds[0]])
        self.bonds = bonds


    def char_only_cdxml_graph(self):
        for bond in self.bonds:            
            # Check if the type is line first
            if bond[-1].split('_')[0] == 'Ln':
                num_lines = bond[2]
                b_type = self.line_type[num_lines]
            
            self.cd_graph.add_edge(self.char_clusters[bond[0]], self.char_clusters[bond[1]], 
                                type=b_type)
            self.cd_graph.nodes[self.char_clusters[bond[0]]]['pos'] = self.vert_polys[bond[-1]][0]
            self.cd_graph.nodes[self.char_clusters[bond[1]]]['pos'] = self.vert_polys[bond[-1]][1]
        return self.cd_graph
    

    def connect_mst_edges(self):
        mst_copy = copy.deepcopy(self.mst)
        all_clus_ids = sorted(list(self.c_clus_bonds.keys()))
        for clus_id in all_clus_ids:
            lines = self.c_clus_bonds[clus_id]
            # If one line, then check if it has 2 line neighbors
            # If it has check if both have an edge
            if len(lines) == 1:
                nei_iter = mst_copy.neighbors(lines[0])
                neis = [n for n in nei_iter]
                val_neis = []
                for nei in neis:
                    if nei.split('_')[0] in self.line_signatures:
                        val_neis.append(nei)
                # Add edge if no edge present
                for i in range(len(val_neis)):
                    for j in range(i+1, len(val_neis)):
                        if not mst_copy.has_edge(val_neis[i], val_neis[j]):
                            poly1 = self.mst.nodes[val_neis[i]]['poly']
                            poly2 = self.mst.nodes[val_neis[j]]['poly']
                            wt = poly1.distance(poly2)
                            mst_copy.add_edge(val_neis[i], val_neis[j], weight=wt)
        self.mst = mst_copy


    def find_main_lines(self):
        main_lines = []
        for nd in self.mst.nodes():
            if nd not in self.all_clus_lines and nd.split('_')[0] in self.line_signatures:
                main_lines.append(nd)
        # print(f'Main Lines: {main_lines}')
        # print(f'Same Bond Lines: {same_bond_lines}')
        # Start from a random main-line, NOT A CHARACTER NODE!
        if self.same_bond_lines[0][0].split('_')[0] in self.line_signatures:
            nxt_visit_lines_id = 0
        else:
            for idx_sb, same_bond in enumerate(self.same_bond_lines):
                if same_bond[0].split('_')[0] in self.line_signatures:
                    nxt_visit_lines_id = idx_sb
                    break
        
        self.main_lines = main_lines
        self.nxt_visit_lines_id = nxt_visit_lines_id

    def get_new_line_type(self, bond):
        # self.line_signatures = ['Ln', 'Wave', 'Circle', 'SW', 'HW']
        # self.new_signatures = ['Sing', 'Doub', 'Trip', 'Wave', 'Circle', 'SW', 'HW']
        if len(bond) == 1:
            if bond[0].split('_')[0] == 'Ln':
                return 'Sing'
            elif bond[0].split('_')[0] == 'Wave':
                return 'Wave'
            elif bond[0].split('_')[0] == 'SW':
                return 'SW'
            elif bond[0].split('_')[0] == 'HW':
                return 'HW'
        else:
            return self.new_signatures[len(bond)-1]
    
    def compress_graph(self, p, idx):
        mst_copy = copy.deepcopy(self.mst)

        # Compress all characters
        # Remove all char_clusters
        for node in self.mst.nodes():
            if node.split('_')[0] not in self.line_signatures:
                nei_iter = self.mst.neighbors(node)
                neis = [n for n in nei_iter]
                nei_lns = [n for n in neis if n.split('_')[0] in self.line_signatures]
                if len(nei_lns) <= 1:
                    mst_copy.remove_node(node)
        lntype_cnt = {typ:1 for typ in self.new_signatures}
        
        # Remove the clusters, cluster_line connection
        # Add the new cluster node and character node
        for key in self.c_clus_bonds.keys():
            bond_chars = self.char_clusters[key]
            bond = self.c_clus_bonds[key]
            # TODO: Remove this check for wave bond errors (HACKY FIX)
            # found_wave = False
            # for idx_bnd, b in enumerate(bond):
            #     if b.split('_')[0] == 'Wave':
            #         found_wave = True
            #         break
            # if found_wave:
            #     print(self.c_clus_bonds)
            #     print(self.same_bond_lines)
            #     exit(0)
            # if found_wave:
            #     wv_bnd = [bond[idx_bnd]]
            #     for b in bond:
            #         if b.split('_')[0] != 'Wave':
            #             found_dup = False
            #             for tmp_key in self.c_clus_bonds.keys():
            #                 if key != tmp_key:
            #                     tmp_bonds = self.c_clus_bonds[tmp_key]
            #                     if b in tmp_bonds:
            #                         found_dup = True
            #                         break
            #             if not found_dup:
            #                 self.same_bond_lines.append()

            bond_poly = self.mst.nodes[bond[0]]['poly']
            bond_verts = self.vert_polys[bond[0]]
            new_b_type = self.get_new_line_type(bond)
            # Get valid neis of the bond
            neis = []
            for b in bond:
                # if mst_copy.has_node(b):
                nei_iter = mst_copy.neighbors(b)
                b_neis = [n for n in nei_iter if n not in bond and n not in neis]
                neis.extend(b_neis)
                # else:
                #     print(self.c_clus_bonds)
                #     print(self.char_clusters)
                #     print(f'Page: {p}, idx: {idx}')
                #     exit(0)
            # Remove the previous bonds
            for b in bond:
                mst_copy.remove_node(b)
            # Add the new node
            new_node = new_b_type + '_' + str(lntype_cnt[new_b_type])
            lntype_cnt[new_b_type] += 1
            mst_copy.add_node(new_node, poly=bond_poly, verts=bond_verts, orig=bond)
            # Add the existing neighbors for this node
            for n in neis:
                mst_copy.add_edge(new_node, n)
            # Add the character cluster
            mst_copy.add_edge(new_node, bond_chars)
        
        # Remove the main lines and replace them with their type
        for bond in self.same_bond_lines:
            if bond[0].split('_')[0] in self.line_signatures:
                new_b_type = self.get_new_line_type(bond)
                new_node = new_b_type + '_' + str(lntype_cnt[new_b_type])
                lntype_cnt[new_b_type] += 1
            else:
                new_node = bond[0]
            bond_poly = self.mst.nodes[bond[0]]['poly']
            bond_verts = self.vert_polys[bond[0]]
            # Get the consequent line only neighbors of this node
            neis = []
            for b in bond:
                nei_iter = mst_copy.neighbors(b)
                b_neis = [n for n in nei_iter if n not in bond and n not in neis]
                neis.extend(b_neis)
            # Remove the bond lines
            for b in bond:
                mst_copy.remove_node(b)
            # Add the new node
            mst_copy.add_node(new_node, poly=bond_poly, verts=bond_verts, orig=bond)
            for n in neis:
                mst_copy.add_edge(new_node,n)
        # If there is any remaining char from previous nodes change it
        # Arising due to vert_poly not populated
        prev_nodes = []
        prev_box = []
        for node in mst_copy.nodes(data=True):
            if node[0].split('_')[0] not in self.new_signatures:
                try:
                    box = node[1]['box']
                    prev_box.append(box)
                    prev_nodes.append(node[0])
                except KeyError:
                    continue
        if len(prev_nodes):
            for idx_node, prev_node in enumerate(prev_nodes):
                nei_iter = mst_copy.neighbors(prev_node)
                neis = [n for n in nei_iter]
                prev_node_poly = mst_copy.nodes[prev_node]['poly']
                box = prev_box[idx_node]
                prev_node_vert = [[box[0], box[1]], [box[2], box[3]]]
                mst_copy.remove_node(prev_node)
                mst_copy.add_node(prev_node, poly=prev_node_poly, verts=prev_node_vert, 
                                  orig=prev_node)
                for n in neis:
                    mst_copy.add_edge(prev_node, n)

        self.comp_graph = copy.deepcopy(mst_copy)

    def find_new_adj(self):
        adj_mat = {}
        for node in self.comp_graph.nodes():
            # Get the neighbors of this node
            nei_iter = self.comp_graph.neighbors(node)
            neis = [n for n in nei_iter]
            adj_mat[node] = neis
        self.new_adj = adj_mat

    def find_all_pos(self, positions, line_graph):
        all_pos = copy.deepcopy(positions)
        for node in line_graph.nodes():
            verts = line_graph.nodes[node]['verts']
            for vert in verts:
                vert_pt = Point(vert)
                found = False
                for pos in all_pos:
                    if isinstance(pos, Polygon):
                        dist = vert_pt.distance(pos)
                    else:
                        dist = vert_pt.distance(Point(pos))
                    if dist <= 6.0:
                        found = True
                        break
                if not found:
                    all_pos.append(vert)

        return all_pos
        
    def find_node_pos(self, vert, positions, in_chars):
        # If vert close to in-char
        cur_vert = Point(vert)
        found = False
        for pos in positions:
            comp_vert = Point(pos)
            dist = cur_vert.distance(comp_vert)
            if dist <= 2:
                found = True
                break
        if found:
            return pos
        else: 
            return None
    
    
    # Find the intersection positions between all lines/chars
    def find_line_sets(self):
        # Find only the line nodes
        ln_nodes = [n for n in self.comp_graph.nodes() if n.split('_')[0] in self.new_signatures]
        line_subgraph = self.comp_graph.subgraph(ln_nodes)
        
        # Find the in-chain char clusters
        in_chars = []
        in_char_pos = []
        for node in self.new_adj.keys():
            if node.split('_')[0] not in self.new_signatures:
                neis = self.new_adj[node]
                lines = [n for n in neis if n.split('_')[0] in self.new_signatures]
                if len(lines) > 1:
                    in_chars.append(node)
                    poly = self.comp_graph.nodes[node]['poly']
                    in_char_pos.append(poly)
        
        # Get all the unique positions (add the in-char pos first)
        positions = [in_char_pos[i] for i in range(len(in_chars))]
        in_char_pos_dict = {in_chars[i]: in_char_pos[i] for i in range(len(in_chars))}
        positions = self.find_all_pos(positions, line_subgraph)
        line_verts = {node:[] for node in line_subgraph.nodes()}
        
        # Set the scope of the downstream vars
        self.positions = positions
        self.inchar_pos_dict = in_char_pos_dict
        self.line_verts = line_verts

        # added_bonds = {}
        # same_bond_lines = copy.deepcopy(self.same_bond_lines)
        # for bond in same_bond_lines:
        #     # Find all the other main line neis for this bond line
        #     neis = []
        #     for ln in bond:
        #         neis.extend(self.adj_matrix[ln][0])
        #         neis.extend(self.adj_matrix[ln][1])
        #     neis = [n for n in neis if n not in bond and n in self.all_same_bond_lines]

        #     # Add meet points if not already there
        #     added_neis = []
        #     ln_ids = []
        #     for ln_id in bond:
        #         if ln_id.split('_')[0] in self.line_signatures:
        #             int_id = int(ln_id.split('_')[-1])
        #             ln_ids.append(int_id)
        #         else:
        #             ln_ids.append(ln_id)
        #     for n in neis:
        #         if n not in added_neis:
        #             cur_nei_ids = []
        #             nei_bond = self.find_same_main_lines(n)
        #             for nei_id in nei_bond:
        #                 if nei_id.split('_')[0] in self.line_signatures:
        #                     nei_int_id = int(nei_id.split('_')[-1])
        #                     cur_nei_ids.append(nei_int_id)
        #                 else:
        #                     cur_nei_ids.append(nei_id)
        #             both_ids = ln_ids + cur_nei_ids
        #             act_id = tuple(set(both_ids))
        #             if act_id not in list(added_bonds.keys()):
        #                 added_neis.extend(nei_bond)
        #                 new_key_lines = copy.deepcopy(bond)
        #                 new_key_lines.extend(nei_bond)
        #                 added_bonds[act_id] = new_key_lines
        # self.main_sets = added_bonds
        
    def find_true_pos(self, pos, node):
        if isinstance(pos, Polygon):
            pos_pt = pos
        else:
            pos_pt = Point(pos)
        for cur_pos in self.positions:
            if isinstance(cur_pos, Polygon):
                cur_dist = pos_pt.distance(cur_pos)
            else:
                cur_dist = pos_pt.distance(Point(cur_pos))
            if cur_dist <= 6:
                return cur_pos
        raise Exception(f'Could not find closest vertex for node {node} \
                        for lexical graph: \n {self.comp_graph}')

    
    def find_nei_vert(self, node, nei):
        # If node a in-char node
        if node.split('_')[0] not in self.new_signatures:
            node_pos = self.comp_graph.nodes[node]['poly']
            found_pos = self.find_true_pos(node_pos,node)
            vert_id = 0
        else:
            # Find the pos
            node_verts = self.comp_graph.nodes[node]['verts']
            nei_vert1 = self.comp_graph.nodes[nei]['verts'][0]
                
            nd_v1 = Point(node_verts[0])
            nd_v2 = Point(node_verts[1])
            nei_comp = Point(nei_vert1)
            dist1 = nd_v1.distance(nei_comp)
            dist2 = nd_v2.distance(nei_comp)
            if dist1 < dist2:
                vert_id = 0
            else:
                vert_id = 1
            found_pos = node_verts[vert_id]
        
        true_pos = self.find_true_pos(found_pos, node)
        return vert_id, true_pos

    
    def build_pos_graph(self):

        # Find only the line nodes with in-chars
        ln_nodes = [n for n in self.comp_graph.nodes() if n.split('_')[0] in self.new_signatures]
        in_chars = list(self.inchar_pos_dict.keys())
        ln_nodes.extend(in_chars)
        line_subgraph = self.comp_graph.subgraph(ln_nodes)
        carbon_id = 1
        
        # Get ALL POSSIBLE carbon IDs with their positions
        # I.E. End points of all lines AND centroid of in-chars 
        # excluding those line endpoints that join a in-char
        pos2nodeid = {}
        for node in line_subgraph.nodes():
            if node.split('_')[0] in self.new_signatures:
                vert1 = line_subgraph.nodes[node]['verts'][0]
                vert2 = line_subgraph.nodes[node]['verts'][1]
                vert1_pos = self.find_true_pos(vert1, node)
                vert2_pos = self.find_true_pos(vert2, node)
                if not isinstance(vert1_pos, Polygon):
                    if tuple(vert1_pos) not in pos2nodeid.keys():
                        pos2nodeid[tuple(vert1_pos)] = 'C_'+str(carbon_id)
                        carbon_id += 1
                if not isinstance(vert2_pos, Polygon):
                    if tuple(vert2_pos) not in pos2nodeid.keys():
                        pos2nodeid[tuple(vert2_pos)] = 'C_'+str(carbon_id)
                        carbon_id += 1
            else:
                poly = line_subgraph.nodes[node]['poly']
                true_pos = self.find_true_pos(poly, node)
                x,y = true_pos.centroid.xy
                true_pos = [x[0],y[0]]
                pos2nodeid[tuple(true_pos)] = node

        # Add all the lines (with some temp carbon nodes) and in-chars
        added_pos = []
        for node in line_subgraph.nodes():
            if node.split('_')[0] in self.new_signatures:
                pos1 = line_subgraph.nodes[node]['verts'][0]
                true_pos1 = self.find_true_pos(pos1,node)
                pos2 = line_subgraph.nodes[node]['verts'][1]
                true_pos2 = self.find_true_pos(pos2,node)
                if isinstance(true_pos1, Polygon):
                    x,y = true_pos1.centroid.xy
                    tmp_pos = [x[0],y[0]]
                    c_node1 = pos2nodeid[tuple(tmp_pos)]
                else:
                    c_node1 = pos2nodeid[tuple(true_pos1)]
                if isinstance(true_pos2, Polygon):
                    x,y = true_pos2.centroid.xy
                    tmp_pos = [x[0],y[0]]
                    c_node2 = pos2nodeid[tuple(tmp_pos)]
                else:
                    c_node2 = pos2nodeid[tuple(true_pos2)]
                if true_pos1 not in added_pos and not isinstance(true_pos1, Polygon):
                    self.cd_graph.add_node(c_node1, pos=true_pos1)
                    added_pos.append(true_pos1)
                if true_pos2 not in added_pos and not isinstance(true_pos2, Polygon):
                    self.cd_graph.add_node(c_node2, pos=true_pos2)
                    added_pos.append(true_pos2)
                cnct_nodes = True
                if isinstance(true_pos1, Polygon) or isinstance(true_pos2, Polygon):
                    cnct_nodes = False
                # Add the edge if not a line connecting to a in-char node
                if cnct_nodes:
                    b_type = self.comp_line_type[node.split('_')[0]]
                    self.cd_graph.add_edge(c_node1, c_node2, type=b_type)
                
                # Find the verts the neighbors connected to keep record
                # (For LATER adding the character groups)
                nei_iter = line_subgraph.neighbors(node)
                neis = [n for n in nei_iter]
                for nei in neis:
                    vert_id, _ = self.find_nei_vert(node, nei)
                    if vert_id not in self.line_verts[node]:
                        self.line_verts[node].append(vert_id)
                
            else: # For the in_Char Nodes
                pos = line_subgraph.nodes[node]['poly']
                true_pos = self.find_true_pos(pos, node)
                x,y = true_pos.centroid.xy
                true_pos = [x[0],y[0]]
                char_node = pos2nodeid[tuple(true_pos)]
                # Add the in-char node
                self.cd_graph.add_node(char_node, pos=true_pos)

                # Connect all the neighbors for this in-char node
                nei_iter = line_subgraph.neighbors(node)
                neis = [n for n in nei_iter]
                for nei in neis:
                    nei_vert1 = Point(line_subgraph.nodes[nei]['verts'][0])
                    nei_vert2 = Point(line_subgraph.nodes[nei]['verts'][1])
                    true_pos_vert = Point(true_pos)
                    dist1 = true_pos_vert.distance(nei_vert1)
                    dist2 = true_pos_vert.distance(nei_vert2)
                    if dist1 > dist2:
                        vert_id = 0
                    else:
                        vert_id = 1
                    
                    b_type = self.comp_line_type[nei.split('_')[0]]
                    nei_vert = line_subgraph.nodes[nei]['verts'][vert_id]
                    true_nei_pos = self.find_true_pos(nei_vert, nei)
                    if isinstance(true_nei_pos, Polygon):
                        x,y = true_nei_pos.centroid.xy
                        true_nei_pos = [x[0],y[0]]
                        char_nei = pos2nodeid[tuple(true_nei_pos)]
                        # Add the in-char 2 in-char edge if it does not exist
                        if not (self.cd_graph.has_edge(char_node, char_nei) or 
                                self.cd_graph.has_edge(char_nei, char_node)):
                            self.cd_graph.add_edge(char_nei, char_node, type=b_type)
                    else:
                        nei_node_id = pos2nodeid[tuple(true_nei_pos)]
                        # Add the line nei node
                        self.cd_graph.add_edge(nei_node_id, char_node, type=b_type)
        
        # Now replace some carbons with its char node
        for node in self.comp_graph.nodes():
            if node.split('_')[0] not in self.new_signatures:
                neis = self.new_adj[node]
                if len(neis) == 1:
                    cur_nei_vert_id = self.line_verts[neis[0]][0]
                    oth_vert_id = 0 if cur_nei_vert_id == 1 else 1
                    oth_vert = self.comp_graph.nodes[neis[0]]['verts'][oth_vert_id]
                    true_pos = self.find_true_pos(oth_vert, neis[0])

                    # Find the temp carbon with this pos
                    for tmp_node in self.cd_graph.nodes(data=True):
                        pos = tmp_node[1]['pos']
                        if pos == true_pos:
                            found_car_node = tmp_node[0]
                            break
                    
                    # Find the neighbor(s) of this node and the bond type
                    for edge in self.cd_graph.edges(data=True):
                        if found_car_node in edge:
                            found_edge = edge
                            break
                    oth_node = found_edge[0] if found_edge[1] == found_car_node else found_edge[1]
                    b_type = found_edge[2]['type']
                    # Delete the temp carbon
                    self.cd_graph.remove_node(found_car_node)

                    # Add the character group and its edge
                    self.cd_graph.add_node(node, pos=true_pos)
                    self.cd_graph.add_edge(oth_node, node, type=b_type)
                    
        return self.cd_graph


# nei_iter = line_subgraph.neighbors(node)
# neis = [n for n in nei_iter]
# for nei in neis:
# vert_id, pos = self.find_nei_vert(node, nei)
# poly = False
# if isinstance(pos, Polygon):
# x,y = pos.centroid.xy
# pos = [x[0],y[0]]
# poly = True

# # Add the pos to the id dict
# if tuple(pos) not in pos2nodeid.keys():
# if not poly:
#     if vert_id not in self.line_verts[node]:
#         self.line_verts[node].append(vert_id)

    def one_main_line_graph(self):
        cur_lines = self.same_bond_lines[0]
        carbon_b_type = self.line_type[len(cur_lines)]
        carbon1 = 'C_'+str(1)
        carbon2 = 'C_'+str(2)
        self.cd_graph.add_node(carbon1, pos = tuple(self.vert_polys[cur_lines[0]][0]))
        self.cd_graph.add_node(carbon2, pos = tuple(self.vert_polys[cur_lines[0]][1]))
        self.cd_graph.add_edge(carbon1, carbon2, type=carbon_b_type)
        vertex1 = Point(self.vert_polys[cur_lines[0]][0])
        vertex2 = Point(self.vert_polys[cur_lines[0]][1])


        # Find the clusters
        all_clusters = self.char_clusters
        for idx_clus, cluster in enumerate(all_clusters):
            clus_b_num = len(self.c_clus_bonds[idx_clus])
            if clus_b_num == 1 and self.c_clus_bonds[idx_clus][0] == 'Wave':
                clus_b_type = self.line_type[4]
            elif clus_b_num == 1 and self.c_clus_bonds[idx_clus][0] == 'SW':
                clus_b_type = self.line_type[5]
            else:
                try:
                    clus_b_type = self.line_type[clus_b_num]
                except KeyError:
                    print(clus_b_num)
                    print(self.c_clus_bonds[idx_clus])
                    print(self.c_clus_bonds)
                    print(self.char_clusters)
                    print(self.mst.edges())
                    exit(0)
            dist1 = self.all_n_cents[cur_lines[0]].distance(
                        Point(self.vert_polys[self.c_clus_bonds[idx_clus][0]][0]))
            dist2 = self.all_n_cents[cur_lines[0]].distance(
                        Point(self.vert_polys[self.c_clus_bonds[idx_clus][0]][1]))
            if dist1 > dist2:
                self.cd_graph.add_node(cluster, pos = 
                                            tuple(self.vert_polys[self.c_clus_bonds[idx_clus][0]][0]))
                vert1_dist = vertex1.distance(Point(self.vert_polys[self.c_clus_bonds[idx_clus][0]][1]))
                vert2_dist = vertex2.distance(Point(self.vert_polys[self.c_clus_bonds[idx_clus][0]][1]))
            else:
                self.cd_graph.add_node(cluster, pos = tuple(self.vert_polys[self.c_clus_bonds[idx_clus][0]][1]))
                vert1_dist = vertex1.distance(Point(self.vert_polys[self.c_clus_bonds[idx_clus][0]][0]))
                vert2_dist = vertex2.distance(Point(self.vert_polys[self.c_clus_bonds[idx_clus][0]][0]))
            if vert1_dist < vert2_dist:
                self.cd_graph.add_edge(carbon1, cluster, type=clus_b_type)
            else:
                self.cd_graph.add_edge(carbon2, cluster, type=clus_b_type)
        
        return self.cd_graph

    
    def find_loop(self):
        loop = False
        valid_cycles = []
        cycle_iter = nx.simple_cycles(self.dir_mst)
        all_cycles = [cycle for cycle in cycle_iter if len(cycle) > 2]
        all_cycles.sort(key=len)

        # Check if a valid circle using endpoints (from adj matrix)
        all_cycle_nodes = []
        for cycle in all_cycles:
            valid = True
            for node in cycle:
                nei_left = self.adj_matrix[node][0]
                nei_right = self.adj_matrix[node][1]
                if len(list(set(cycle).intersection(nei_left))) > 1 or \
                    len(list(set(cycle).intersection(nei_right))) > 1:
                    valid = False
                    break
            tmp_valid_cycles = [set(cyc) for cyc in valid_cycles]
            if valid and not len(list(set(cycle).intersection(all_cycle_nodes))) > 1 and \
                set(cycle) not in tmp_valid_cycles:
                valid_cycles.append(cycle)
                all_cycle_nodes.extend(cycle)
                loop = True

        self.loop = loop
        self.valid_cycles = [list(valid_cycle) for valid_cycle in valid_cycles]

    def get_intersections(self, bond):
        inters = []
        inters_tuple = []
        all_lines = []
        for inter_id in self.main_sets.keys():
            lines = self.main_sets[inter_id]
            if set(lines).intersection(set(bond)) == set(bond):
                diff = set(lines).difference(set(bond))
                inters.append(self.find_same_main_lines(list(diff)[0]))
                inters_tuple.append(inter_id)
                all_lines.append(lines)

        return inters, inters_tuple, all_lines
    
    # For no cycle cases ONLY
    def new_main_graph(self, p, idx):
        carbon_dict = {}
        carbon_verts = {}
        carbon_id = 1

        # Complete the main chain first
        added_inters = []
        for bond in self.same_bond_lines:
            bond_vert1 = Point(self.vert_polys[bond[0]][0])
            bond_vert2 = Point(self.vert_polys[bond[0]][1])
            # Get all intersections of this line
            inters, inters_tuple, all_lines = self.get_intersections(bond)
            for i in range(len(inters)):
                if inters_tuple[i] not in added_inters:
                    added_inters.append(inters_tuple[i])
                    inter_vert = Point(self.vert_polys[inters[i][0]])
                    vert1_dist = bond_vert1.distance(inter_vert)
                    vert2_dist = bond_vert2.distance(inter_vert)
                    if vert1_dist < vert2_dist:
                        vert_id = 0
                    else: 
                        vert_id = 1
                    pos = self.vert_polys[bond[0]][vert_id]
                    # Check if pos already present, if yes, graph the carbon id
                    # TODO
                    if bond[0].split('_')[0] in self.line_signatures:
                        cur_carbon = 'C_'+str(carbon_id)
                        carbon_id += 1
                        self.cd_graph.add_node(cur_carbon, pos=pos)
                    else:
                        cur_carbon = bond[0]
                        self.cd_graph.add_node(cur_carbon, pos=pos)
                    carbon_verts[cur_carbon] = pos
                    carbon_dict[tuple(all_lines[i])] = cur_carbon
                    


            
            


    def main_graph(self, p, idx):
        prev_visited_lines = []
        cur_carbon_id = 1
        visited_carbon_dict = {}
        vis_ca_dict_nxt_id = {}
        visited_clus_ids = []
        while len(prev_visited_lines) < len(self.same_bond_lines):
            # Get the carbon ID
            # NOTE: For the first iteration add two carbons:
            # Add the second carbon to visi_carbon_dict
            cur_lines = self.same_bond_lines[self.nxt_visit_lines_id]
            # if idx == 25:
            #     print(f'Cur Lines: {cur_lines}')
            # Account for Solid Wedge Lines
            # TODO: Also for dashed wedge 
            if len(cur_lines) == 1 and cur_lines[0].split('_')[0] == 'SW':
                carbon_b_type = self.line_type[5]
                # if idx == 25:
                #     print(f'Bond type for lines inside SW cond: {cur_lines} is {carbon_b_type}')
            else:
                carbon_b_type = self.line_type[len(cur_lines)]
                # if idx == 25:
                #     print(f'Bond type for lines inside normal cond: {cur_lines} is {carbon_b_type}')
            if len(prev_visited_lines) == 0:
                if not self.loop:
                    carbon1 = 'C_'+str(1)
                    carbon2 = 'C_'+str(2)
                    # Poition C_1 with min x value
                    verts = self.vert_polys[cur_lines[0]]
                    x_verts = [verts[0][0], verts[0][1]]
                    strt_idx = x_verts.index(min(x_verts))
                    end_idx = 1 if strt_idx == 0 else 0
                    self.cd_graph.add_node(carbon1, pos = tuple(self.vert_polys[cur_lines[0]][strt_idx]))
                    self.cd_graph.add_node(carbon2, pos = tuple(self.vert_polys[cur_lines[0]][end_idx]))
                    self.cd_graph.add_edge(carbon1, carbon2, type=carbon_b_type)
                    # if idx == 25:
                    #     print(f'Cur Edge Added: {carbon1},{carbon2}, {carbon_b_type}')
                    carbon_id = carbon2
                    visited_carbon_dict[tuple(cur_lines)] = carbon_id
                    vis_ca_dict_nxt_id[tuple(cur_lines)] = 1
                    cur_carbon_id += 2
                else: # Have to find out which vertex will be first based on next one
                    carbon_id = 'C_'+str(cur_carbon_id)
                    visited_carbon_dict[tuple(cur_lines)] = carbon_id
                    cur_carbon_id += 1
            else:
                carbon_id = visited_carbon_dict[tuple(cur_lines)]
           
            
            # hidden carbon line or character
            # Find a neighbor of the current line/char
            found = False
            for line in cur_lines:
                nei_iter = self.mst.neighbors(line)
                neis = [n for n in nei_iter]
                # if idx == 28:
                    # print(f'Neighbors for line: {line} are: {neis}')
                for nei in neis:
                    if nei not in cur_lines and nei in self.all_same_bond_lines:
                        nxt_bond_lines = self.find_same_main_lines(nei)
                        # if idx == 28:
                            # print(f'Found nxt bond lines: {nxt_bond_lines}')
                            # print(f'With neighbor: {nei}')
                        if nxt_bond_lines not in prev_visited_lines[-2:]:
                            # Find out the first hidden carbon vertex
                            if len(prev_visited_lines) == 0 and self.loop:
                                # Find the minx miny of neighbor and cur bond
                                nxt_vertices = self.vert_polys[nxt_bond_lines[0]]
                                nxt_verts_y = [nxt_vertices[0][1], nxt_vertices[1][1]]
                                nxt_verts_x = [nxt_vertices[0][0], nxt_vertices[1][0]]
                                nxt_min_y = min(nxt_verts_y)
                                nxt_min_x = min(nxt_verts_x)
                                cur_vertices = self.vert_polys[cur_lines[0]]
                                cur_verts_y = [cur_vertices[0][1], cur_vertices[1][1]]
                                cur_verts_x = [cur_vertices[0][0], cur_vertices[1][0]]
                                cur_min_y = min(cur_verts_y)
                                cur_min_y_idx = cur_verts_y.index(cur_min_y)
                                cur_max_y_idx = 0 if cur_min_y_idx==1 else 1
                                cur_min_x = min(cur_verts_x)
                                cur_min_x_idx = cur_verts_x.index(cur_min_x)
                                cur_max_x_idx = 0 if cur_min_x_idx==1 else 1
                                if cur_min_y > nxt_min_y:
                                    c_pos = cur_vertices[cur_max_y_idx]
                                    nxt_idx = cur_min_y_idx
                                elif cur_min_y < nxt_min_y:
                                    c_pos = cur_vertices[cur_min_y_idx]
                                    nxt_idx = cur_max_y_idx
                                elif cur_min_y == nxt_min_y: 
                                    if cur_min_x < nxt_min_x:
                                        c_pos = cur_vertices[cur_min_x_idx]
                                        nxt_idx = cur_max_x_idx
                                    else:
                                        c_pos = cur_vertices[cur_max_x_idx]
                                        nxt_idx = cur_min_x_idx
                                self.cd_graph.add_node(carbon_id, pos=c_pos)
                                if idx == 43:
                                    print(carbon_id)
                                    print(c_pos)
                                vis_ca_dict_nxt_id[tuple(cur_lines)] = nxt_idx

                            found_loop = False

                            # Check if the line already visited first (coonect loop), if yes, 
                            # get the correct carbon ID of the line
                            if nxt_bond_lines in prev_visited_lines: # Loop found
                                found_loop = True
                                comp_carbon_id = visited_carbon_dict[tuple(nxt_bond_lines)]

                            else: # New Hidden Carbon Found/Char Found
                                if nxt_bond_lines[0].split('_')[0] not in self.line_signatures:
                                    comp_carbon_id = nxt_bond_lines[0]
                                    visited_carbon_dict[tuple(nxt_bond_lines)] = comp_carbon_id
                                else:
                                    comp_carbon_id = 'C_'+str(cur_carbon_id)
                                    visited_carbon_dict[tuple(nxt_bond_lines)] = comp_carbon_id
                                    cur_carbon_id += 1
                            # if idx == 27:
                            #     print(f'Comp Carbon ID: {comp_carbon_id}')
                            if not found_loop:
                                # Next vertex ID of current line
                                cur_nxt_vert_id = vis_ca_dict_nxt_id[tuple(cur_lines)]
                                cur_nxt_vert = Point(self.vert_polys[cur_lines[0]][cur_nxt_vert_id])
                                dist1 = cur_nxt_vert.distance(Point(self.vert_polys[nxt_bond_lines[0]][0]))
                                dist2 = cur_nxt_vert.distance(Point(self.vert_polys[nxt_bond_lines[0]][1]))
                                # if idx == 27:
                                #     print(f'Dist1: {dist1}')
                                #     print(f'Dist2: {dist2}')
                                
                                if dist1 < dist2:
                                    nxt_vert_id = 1
                                else:
                                    nxt_vert_id = 0
                                vis_ca_dict_nxt_id[tuple(nxt_bond_lines)] = nxt_vert_id
                                # Also confirm if the current vertex is right (for carbon line branchings)
                                cur_vert1 = Point(self.vert_polys[cur_lines[0]][0])
                                cur_vert2 = Point(self.vert_polys[cur_lines[0]][1])
                                dist_vert1 = Point(self.vert_polys[nxt_bond_lines[0]][nxt_vert_id]).distance(cur_vert1)
                                dist_vert2 = Point(self.vert_polys[nxt_bond_lines[0]][nxt_vert_id]).distance(cur_vert2)
                                if dist_vert1 < dist_vert2:
                                    cur_vert_re_id = 0
                                else:
                                    cur_vert_re_id = 1
                                if cur_vert_re_id != cur_nxt_vert_id:
                                    carbon_id = 'C_'+str(int(carbon_id.split('_')[-1])-1)

                                # !!!! NOTE !!!!
                                # For molecules with rings, take current line as position and bond type
                                # Else take next line for no loops
                                if self.loop:
                                    self.cd_graph.add_node(comp_carbon_id, pos = tuple(self.vert_polys[cur_lines[0]][cur_nxt_vert_id]))
                                else:
                                    self.cd_graph.add_node(comp_carbon_id, pos=tuple(self.vert_polys[nxt_bond_lines[0]][nxt_vert_id]))
                            
                            # TODO: Add here when hashed bonds captured    
                            if not self.loop:
                                if nxt_bond_lines[0].split('_')[0] == 'SW':
                                    carbon_b_type = self.line_type[5]
                                else:
                                    carbon_b_type = self.line_type[len(nxt_bond_lines)]
                                
                            self.cd_graph.add_edge(carbon_id, comp_carbon_id, type=carbon_b_type)
                            # if idx == 25:
                            #     print(f'Cur Edge Added in 2nd: {carbon_id},{comp_carbon_id}, {carbon_b_type}')
                            
                            found = True
                            break
                if found:
                    break

            # Now that the next line carbon has been added, try to find a cluster
            # Find if there are any clusters connected to the lines
            if vis_ca_dict_nxt_id[tuple(cur_lines)] == 0:
                cur_c_vert_id = 1
                nxt_c_vert_id = 0
            else:
                cur_c_vert_id = 0
                nxt_c_vert_id = 1
            
            for line in cur_lines:
                nei_iter = self.mst.neighbors(line)
                neis = [n for n in nei_iter]
                clusters = self.find_connected_clusters(neis, cur_lines)
                                
                for cluster in clusters:
                    if cluster[-1] not in visited_clus_ids:
                        # ----- TODO: Change this also 3D bond info when avail
                        dist1 = self.all_n_cents[cur_lines[0]].distance(Point(self.vert_polys[self.c_clus_bonds[cluster[-1]][0]][0]))
                        dist2 = self.all_n_cents[cur_lines[0]].distance(Point(self.vert_polys[self.c_clus_bonds[cluster[-1]][0]][1]))
                        if dist1 > dist2:
                            self.cd_graph.add_node(cluster[0], pos = tuple(self.vert_polys[self.c_clus_bonds[cluster[-1]][0]][0]))
                            con_vertex = Point(self.vert_polys[self.c_clus_bonds[cluster[-1]][0]][1])
                        else:
                            self.cd_graph.add_node(cluster[0], pos = tuple(self.vert_polys[self.c_clus_bonds[cluster[-1]][0]][1]))
                            con_vertex = Point(self.vert_polys[self.c_clus_bonds[cluster[-1]][0]][0])
                        if cluster[2] == 'Wave':
                            b_type = self.line_type[4]
                        elif cluster[2] == 'SW':
                            b_type = self.line_type[5]
                        else:
                            b_type = self.line_type[cluster[1]]
                        # Find the cluster line distance with both the main line vertices
                        con_vert_dist1 = Point(self.vert_polys[cur_lines[0]][cur_c_vert_id]).distance(con_vertex)
                        con_vert_dist2 = Point(self.vert_polys[cur_lines[0]][nxt_c_vert_id]).distance(con_vertex)
                        if len(prev_visited_lines) == 0 and not self.loop:
                            if con_vert_dist1 < con_vert_dist2:
                                self.cd_graph.add_edge(carbon1, cluster[0], type=b_type)
                            else:
                                self.cd_graph.add_edge(carbon2, cluster[0], type=b_type)
                        elif self.loop: # Loop Case - Current and Next Carbon
                            carbon_id_num = int(carbon_id.split('_')[-1])
                            nxt_carbon_id = 'C_'+str(carbon_id_num+1)
                            if con_vert_dist1 < con_vert_dist2:
                                self.cd_graph.add_edge(carbon_id, cluster[0], type=b_type)
                            else:
                                self.cd_graph.add_edge(nxt_carbon_id, cluster[0], type=b_type)
                        else: # No loop Case - Previous and Current
                            cur_vert_noloop = self.vert_polys[nxt_bond_lines[0]][nxt_vert_id]
                            nxt_vert_noloop_id = 0 if nxt_vert_id == 1 else 1
                            nxt_vert_noloop = self.vert_polys[nxt_bond_lines[0]][nxt_vert_noloop_id]
                            cur_vert_dist1 = Point(cur_vert_noloop).distance(con_vertex)
                            cur_vert_dist2 = Point(nxt_vert_noloop).distance(con_vertex)
                            carbon_id_num = int(carbon_id.split('_')[-1])
                            prev_carbon_id = 'C_'+str(carbon_id_num-1)
                            if cur_vert_dist1 > cur_vert_dist2:
                                self.cd_graph.add_edge(prev_carbon_id, cluster[0], type=b_type)
                            else:
                                self.cd_graph.add_edge(carbon_id, cluster[0], type=b_type)
                        
                        # print('Cluster Edge Added')
                        # print(cd_graph.edges())
                        visited_clus_ids.append(cluster[-1])
            # print(f'Cur Edges at finding clusters: {cd_graph.edges(data=True)}')

            
            # Next visit will be this neighbor
            try:
                self.nxt_visit_lines_id = self.same_bond_lines.index(nxt_bond_lines)
            except ValueError:
                print(self.same_bond_lines)
                print(nxt_bond_lines)
                # print(idx)
                exit(0)
            except UnboundLocalError:
                return 'N/A'
            prev_visited_lines.append(cur_lines)
        return self.cd_graph
    

    def find_connected_clusters(self, neis, cur_lines):
        clusters = []
        clus_ids = sorted(list(self.c_clus_bonds.keys()))
        for n in neis:
            if n not in cur_lines:
                for clus_id in clus_ids:
                    lines = self.c_clus_bonds[clus_id]
                    if n in lines:
                        # ------TODO: When 3D bond identified FOR CLUSTERS lines, 
                        # apart from curve add it here!!!-------
                        if len(lines) == 1 and lines[0].split('_')[0] == 'Wave':
                            clusters.append([self.char_clusters[clus_id], 1, 'Wave', clus_id])
                        elif len(lines) == 1 and lines[0].split('_')[0] == 'SW':
                            clusters.append([self.char_clusters[clus_id], 1, 'SW', clus_id])
                        else:
                            clusters.append([self.char_clusters[clus_id], len(lines), 'Ln', clus_id])
        return clusters


    def find_same_main_lines(self, cur_line):
        for same_bond in self.same_bond_lines:
            if cur_line in same_bond:
                return same_bond
        return []
    
    def find_correct_nei(self, loop_nodes, neis, added_same_bonds, p, pdf_idx):
        # Find the correct loop node
        valid_neis = [self.find_same_main_lines(n) for n in neis]
        v_neis = []
        for n in valid_neis:
            if n not in v_neis and len(n):
                v_neis.append(n)
        cur_added = []
        for idx_nei, nei in enumerate(v_neis):
            nxt_iter_nei = nei
            # if p == 2 and pdf_idx == 1:
            #     print(f'For Nei: {nxt_iter_nei}')
            while True:
                all_neis = []
                # if p == 2 and pdf_idx == 1:
                #     print(f'Next iter: {nxt_iter_nei}')
                for same_nei in nxt_iter_nei:
                    # try:
                    nxt_iter = self.mst.neighbors(same_nei)
                    # except nx.NetworkXError:
                    #     print('Exception outs')
                    #     print(same_nei)
                    #     print(valid_neis)
                    #     print(v_neis)
                    #     print(nxt_iter_nei)
                    #     print(self.mst.nodes())
                    #     print(p, pdf_idx)
                    #     exit(0)

                    # if p == 1 and pdf_idx == 9:
                    #     tmp_iter = self.mst.neighbors(same_nei)
                    #     tmp_neis = [n for n in tmp_iter]
                    #     print(f'For node {same_nei}: all neighbors: {tmp_neis}')
                    #     # print(self.mst.edges())
                    #     tmp_neis = [n for n in tmp_neis if n not in added_same_bonds]
                    #     print(f'After not in added same bonds: {tmp_neis}')
                    #     tmp_neis = [n for n in tmp_neis if n not in self.line_signatures]
                    #     print(f'After not in line signatures: {tmp_neis}')
                    #     tmp_neis = [n for n in tmp_neis if n in self.find_same_main_lines(n)]
                    #     # print(f'Same main lines are: {self.find_same_main_lines(n)}')
                    #     print(f'After not in same main lines: {tmp_neis}')
                    #     tmp_neis = [n for n in tmp_neis if n not in cur_added]
                    #     print(f'After not in cur_added: {tmp_neis}')
                    #     tmp_neis = [n for n in tmp_neis if self.find_same_main_lines(n) not in v_neis]
                    #     print(f'After not in v_neis: {tmp_neis}')
                    #     tmp_neis = [n for n in tmp_neis if n not in self.all_clus_lines]
                    #     print(f'After not in all_clus_lines: {tmp_neis}')
                    nxt_neis = [n for n in nxt_iter if n not in added_same_bonds and
                                n.split('_')[0] in self.line_signatures and
                                n in self.find_same_main_lines(n) and 
                                n not in cur_added and self.find_same_main_lines(n) not in v_neis
                                and n not in self.all_clus_lines]
                    all_neis.extend(nxt_neis)
                # if p == 2 and pdf_idx == 1:
                #     print(f'Cur all_neis: {all_neis}')
                if not len(all_neis):
                    cur_added = []
                    break
                else:
                    for all_n in all_neis:
                        if all_n in loop_nodes:
                            # if p == 2 and pdf_idx == 1:
                            #     print(f'Found loop node: {all_n} for node: {nxt_iter_nei}')
                            try:
                                return neis.index(nei[0])
                            except ValueError:
                                return neis.index(nei[1])
                    else:
                        nxt_iter_nei = all_neis
                        cur_added.extend(nxt_iter_nei)
            cur_added = []
        # raise Exception(f'No connecting loop node found for MST with:\
        #                 nodes: {self.mst.nodes()}. P: {p}, idx: {pdf_idx}')
        return 'N/A'
                
    
    
    # In- Case of Loops
    def refined_processing(self, p, pdf_idx):
        # ------STEP-1------
        # First get all the loop nodes
        loop_nodes = []
        for cycle in self.valid_cycles:
            for nd in cycle:
                if nd not in loop_nodes:
                    loop_nodes.append(nd)
        
        # ------STEP-2------
        # Find all the peripheral clusters with character clusters
        # print(self.final_char_clusters)
        # print(self.char_clusters)
        # print(self.c_clus_bonds)
        # exit(0)
        self.peripherals = []
        self.peripheral_nxt = []
        added_same_bonds = []
        added_same_bonds_cid = {}
        carbon_id = 1
        for clus_idx in self.c_clus_bonds.keys():
            ln_node = self.c_clus_bonds[clus_idx]
            # Set the char cluster node position (nearest edge of clus line)
            char_source = self.mst.nodes[self.final_char_clusters[clus_idx][0]]['poly']
            clus_ln_vert1 = Point(self.vert_polys[ln_node[0]][0])
            clus_ln_vert2 = Point(self.vert_polys[ln_node[0]][1])
            vert1_dist = clus_ln_vert1.distance(char_source)
            vert2_dist = clus_ln_vert2.distance(char_source)
            if vert1_dist < vert2_dist:
                chars_pos_id = 0
                nxt_pos_id = 1
            else:
                chars_pos_id = 1
                nxt_pos_id = 0
            chars_pos = self.vert_polys[ln_node[0]][chars_pos_id]
            # Add the char cluster
            cur_carbon_id = self.char_clusters[clus_idx]
            self.cd_graph.add_node(cur_carbon_id, pos=tuple(chars_pos))
            cur_node_pos = Point(self.vert_polys[ln_node[0]][chars_pos_id])
            cur_node = ln_node[0]
            cur_cls_added_nodes = []
            cur_seen_neis = []
            iter_cnt = 1
            # if p == 2 and pdf_idx == 1:
            #     print(f'For cluster: {self.char_clusters[clus_idx]}')
            while True:
                n_iter = self.mst.neighbors(cur_node)
                if p == 2 and pdf_idx == 1:
                    print(f'Cur node: {cur_node}')
                nei = [n for n in n_iter if n not in loop_nodes and n not in self.all_clus_lines
                        and n.split('_')[0] in self.line_signatures and 
                        n not in self.find_same_main_lines(cur_node) and
                        n not in added_same_bonds and n not in cur_seen_neis]
                # if p == 2 and pdf_idx == 1:
                #     print(f'Cur neis: {nei}')

                # In case of more than 1 char cluster touching with the same
                # line to a ring, just add the current node and break
                ini_n_iter = self.mst.neighbors(cur_node)
                ini_neis = [n for n in ini_n_iter if n in added_same_bonds
                            and n not in cur_cls_added_nodes]
                if len(ini_neis): # This means a branch off a peripheral cluster
                    # Find the carbon ID for this already added nei
                    prev_cid = added_same_bonds_cid[tuple(self.find_same_main_lines(ini_neis[0]))][0]
                    prev_pos = added_same_bonds_cid[tuple(self.find_same_main_lines(ini_neis[0]))][1]
                    cur_vert = self.mst.nodes[cur_node]['poly'].centroid
                    vert1_nei = Point(self.vert_polys[ini_neis[0]][0])
                    vert2_nei = Point(self.vert_polys[ini_neis[0]][1])
                    vert1_nei_dist = vert1_nei.distance(cur_vert)
                    vert2_nei_dist = vert2_nei.distance(cur_vert)
                    if vert1_nei_dist < vert2_nei_dist:
                        vert_id = 0
                    else:
                        vert_id = 1
                    tmp_nxt_cid = 'C_' + str(int(prev_cid.split('_')[-1])+1)
                    bond_type = self.find_bond_type(cur_node)
                    if vert_id == 0 and prev_pos == 0:
                        self.cd_graph.add_edge(tmp_nxt_cid, cur_carbon_id, type=bond_type)
                    elif vert_id == 0 and prev_pos == 1:
                        self.cd_graph.add_edge(prev_cid, cur_carbon_id, type=bond_type)
                    elif vert_id == 1 and prev_pos == 0:
                        self.cd_graph.add_edge(prev_cid, cur_carbon_id, type=bond_type)
                    else:
                        self.cd_graph.add_edge(tmp_nxt_cid, cur_carbon_id, type=bond_type)
                    if p == 2 and pdf_idx == 4: 
                        print(f'In repeat char, Adding edge: {prev_cid} with {cur_carbon_id}')
                    break

                # If no neighbors found, that means it is connected to one of the ring lines
                if not len(nei):
                    self.peripherals.append(cur_node)
                    self.peripheral_nxt.append(cur_carbon_id)
                    break
                else: # More lines to add before reaching a ring
                    if len(nei) > 1: # possible branchings from node
                        # if p == 2 and pdf_idx == 1:
                        #     print(f'Finding cor nei for {cur_node} and neighbors: {nei}')
                        nei_id = self.find_correct_nei(loop_nodes, nei, added_same_bonds, p, pdf_idx)
                        if nei_id == 'N/A':
                            return 'N/A'
                        # if p == 1 and pdf_idx == 9:
                        #     print(nei[nei_id])
                        #     print(self.mst.nodes())
                        #     exit(0)
                    else:
                        nei_id = 0
                    # if p == 2 and pdf_idx == 1:
                    #     print(f'Nxt nei: {nei[nei_id]}')
                    nxt_node_pos = self.vert_polys[cur_node][nxt_pos_id]
                    nxt_carbon_id = 'C_'+str(carbon_id)
                    self.cd_graph.add_node(nxt_carbon_id, pos=nxt_node_pos)
                    bond_type = self.find_bond_type(cur_node)
                    self.cd_graph.add_edge(nxt_carbon_id, cur_carbon_id, type=bond_type)
                    # Set parameters for next iteration
                    cur_nd_pt = Point(nxt_node_pos)
                    nxt_nd_pt1 = Point(self.vert_polys[nei[nei_id]][0])
                    nxt_nd_pt2 = Point(self.vert_polys[nei[nei_id]][1])
                    pt1_dist = nxt_nd_pt1.distance(cur_nd_pt)
                    pt2_dist = nxt_nd_pt2.distance(cur_nd_pt)
                    if pt1_dist > pt2_dist:
                        nxt_pos_id = 0
                    else:
                        nxt_pos_id = 1
                    
                    added_same_bonds_cid[tuple(self.find_same_main_lines(nei[nei_id]))] = [nxt_carbon_id, nxt_pos_id]
                    carbon_id += 1
                    cur_node = nei[nei_id]
                    cur_carbon_id = nxt_carbon_id
                    cur_seen_neis.extend(nei)
                    added_same_bonds.extend(self.find_same_main_lines(nei[nei_id]))
                    cur_cls_added_nodes.extend(self.find_same_main_lines(nei[nei_id]))
                iter_cnt += 1
        if p == 2 and pdf_idx == 6:
            print(f'Cur nodes: {self.cd_graph.nodes()}')
            # print(self.cd_graph.edges())
            # print(self.char_clusters)
            # print(self.c_clus_bonds)
            # print(self.peripherals)
            # exit(0)
        # ------STEP-3------
        # Now find floating main lines/chars if any 
        # (Without a terminal char cluster)
        # Determine all the accounted for main lines till now
        cur_added_main_lines = []
        for bond in added_same_bonds:
            cur_added_main_lines.append(bond)
        
        added_same_loop_lines = []
        for cycle in self.valid_cycles:
            for nd in cycle:
                bond_lines = self.find_same_main_lines(nd)
                if bond_lines not in added_same_loop_lines:
                    cur_added_main_lines.extend(bond_lines)
                    added_same_loop_lines.extend(bond_lines)
        
        floating_lines = set(self.all_same_bond_lines).difference(set(cur_added_main_lines))
        # if p == 2 and pdf_idx == 6:
        #     print(f'Found float lines: {floating_lines}')
        #     exit(0)
        if len(floating_lines):
            # Find the same floating bond lines
            same_floating_lines = []
            for line in floating_lines:
                for same_bond in self.same_bond_lines:
                    if line in same_bond and same_bond not in same_floating_lines:
                        same_floating_lines.append(same_bond)
            
            # Find valid start points (from a loop line or a pheripher cluster line)
            valid_starts = {}
            both = []
            float2cluster = []
            for floating_bond in same_floating_lines:
                neis = []
                for line in floating_bond:
                    nei_iter = self.mst.neighbors(line)
                    cur_neis = [n for n in nei_iter]
                    neis.extend(cur_neis)
                for nei in neis:
                    if nei in added_same_loop_lines: # If touching a cycle line
                        # Check if any neigh also connected to a peri cycle line
                        fnd_both = False
                        for tmp_nei in neis:
                            if tmp_nei in added_same_bonds:
                                both.append([floating_bond, nei, tmp_nei])
                                fnd_both = True
                                break
                        if not fnd_both:
                            valid_starts[nei] = floating_bond
                            self.peripherals.append(floating_bond[0])
                        break
                    elif nei in added_same_bonds: # If touching peripheral cluster line
                        # Check if any neigh also connected to a loop line
                        fnd_both = False
                        for tmp_nei in neis:
                            if tmp_nei in added_same_loop_lines:
                                both.append([floating_bond, tmp_nei, nei])
                                fnd_both = True
                                break
                        if not fnd_both:
                            valid_starts[nei] = floating_bond
                            float2cluster.append(floating_bond[0])
                        break
            # if p == 2 and pdf_idx == 6:
            #     print(f'Valid Starts: {valid_starts}')
            #     print(f'Float2cluster: {float2cluster}')
            #     print(f'Peripherals: {self.peripherals}')
            #     print(f'Both: {both}')
            #     exit(0)

            # Complete the peripheral floating clusters
            added_floating_lines = []
            for valid_start_node in valid_starts.keys():
                added_floating_lines.extend(valid_starts[valid_start_node])
                nxt_bond = valid_starts[valid_start_node]
                # Add the first node
                vert1 = self.vert_polys[nxt_bond[0]][0]
                vert2 = self.vert_polys[nxt_bond[0]][1]
                source_pt = self.vert_polys[valid_start_node][0]
                vert1_dist = Point(vert1).distance(Point(source_pt))
                vert2_dist = Point(vert2).distance(Point(source_pt))
                if vert1_dist > vert2_dist:
                    cur_pos_id = 0
                else:
                    cur_pos_id = 1
                cur_pos = self.vert_polys[nxt_bond[0]][cur_pos_id]
                cur_carbon_id = 'C_'+str(carbon_id)
                self.cd_graph.add_node(cur_carbon_id, pos=cur_pos)
                carbon_id += 1
                nxt_pos_id = cur_pos_id
                
                iter_cnt = 1
                while True:
                    neis = []
                    for bond_line in nxt_bond:
                        n_iter = self.mst.neighbors(bond_line)
                        cur_neis = [n for n in n_iter if n not in cur_added_main_lines 
                                    and n not in added_floating_lines and
                                    n not in nxt_bond and n not in self.all_clus_lines]
                        neis.extend(cur_neis)
                    
                    if not len(neis): # No more neighbors to add, break
                        temp_n_iter = self.mst.neighbors(nxt_bond[0])
                        tmp_neis = [n for n in temp_n_iter if n in added_same_bonds]
                        if len(tmp_neis): # Floating to peri clus line connection
                            clus_info = added_same_bonds_cid[tuple(self.find_same_main_lines(tmp_neis[0]))]
                            clus_carb_id = clus_info[0]
                            if int(clus_carb_id.split('_')[-1]) == 1:
                                clus_precarb_id = clus_carb_id
                            else:
                                clus_precarb_id = 'C_' + str(int(clus_carb_id.split('_')[-1])-1)
                            prev_vert_id = clus_info[1]
                            prev_vert1 = Point(self.vert_polys[tmp_neis[0]][0])
                            prev_vert2 = Point(self.vert_polys[tmp_neis[0]][1])
                            cur_vert = Point(self.vert_polys[nxt_bond[0]][nxt_pos_id])
                            dist1 = cur_vert.distance(prev_vert1)
                            dist2 = cur_vert.distance(prev_vert2)
                            if dist1 < dist2:
                                cur_id = 0
                            else:
                                cur_id = 1
                            bond_type = self.find_bond_type(nxt_bond[0])
                            carbon_id += 1
                            if cur_id == prev_vert_id:
                                self.cd_graph.add_edge(clus_carb_id, cur_carbon_id, type=bond_type)
                            else:
                                self.cd_graph.add_edge(clus_precarb_id, cur_carbon_id, type=bond_type)
                        else:
                            if iter_cnt == 1:
                                self.peripheral_nxt.append('New')
                            else:
                                self.peripheral_nxt.append(peripheral_nxt)
                        break
                    else:
                        # Find the next bond_type
                        if cur_carbon_id.split('_')[0] != 'C':
                            for n_idx, n in enumerate(neis):
                                if n.split('_')[0] in self.line_signatures:
                                    nxt_id = n_idx
                                    break
                        else:
                            nxt_id = 0
                        bond_type = self.find_bond_type(neis[nxt_id])
                        found_bond_line = self.find_same_main_lines(neis[nxt_id])
                        if not len(found_bond_line):
                            vert1 = self.mst.nodes[neis[0]]['poly'].centroid
                            vert2 = self.mst.nodes[neis[0]]['poly'].centroid
                            self.vert_polys[neis[0]] = [vert1, vert2]
                        else:
                            vert1 = Point(self.vert_polys[neis[nxt_id]][0])
                            vert2 = Point(self.vert_polys[neis[nxt_id]][1])
                        cur_pos_vert = Point(cur_pos)
                        vert1_dist = vert1.distance(cur_pos_vert)
                        vert2_dist = vert2.distance(cur_pos_vert)
                        if vert1_dist > vert2_dist:
                            nxt_pos_id = 0
                        else:
                            nxt_pos_id = 1
                        nxt_pos = self.vert_polys[neis[nxt_id]][nxt_pos_id]
                        if not len(found_bond_line):
                            nxt_carbon_id = neis[0]
                            found_bond_line = [neis[0]]
                        else:
                            nxt_carbon_id = 'C_'+str(carbon_id)
                            carbon_id += 1
                        if p == 2 and pdf_idx == 6:
                            print(f'Adding next carbon id: {nxt_carbon_id}')
                        self.cd_graph.add_node(nxt_carbon_id, pos=nxt_pos)
                        if p == 2 and pdf_idx == 6:
                            print(self.cd_graph.nodes())
                        self.cd_graph.add_edge(cur_carbon_id, nxt_carbon_id, type=bond_type)

                        # Now set up for next iteration
                        cur_carbon_id = nxt_carbon_id
                        cur_pos = nxt_pos
                        nxt_bond = found_bond_line
                        added_floating_lines.extend(found_bond_line)
                        if iter_cnt == 1:
                            peripheral_nxt = nxt_carbon_id
                        iter_cnt += 1
            
            # Now add any floating lines from an added peripeheral cluster line

        
        # ------STEP-4------
        # Now that all clusters have been added and recorded
        # Finally complete the cycles connecting the peripheral edges when it comes
        # NOTE: self.peripherals have ALL the peripheral starting edges
        added_neis = []
        # Ensure all cycles are clockwise
        new_cycles = []
        for cycle in self.valid_cycles:
            sum_factor = 0
            cycle_copy = copy.deepcopy(cycle)
            for idx_node in range(len(cycle)):
                next_idx = (idx_node+1) % len(cycle_copy)
                x2,y2 = self.mst.nodes[cycle[next_idx]]['poly'].centroid.xy
                x2, y2 = x2[0], y2[0]
                x1,y1 = self.mst.nodes[cycle[idx_node]]['poly'].centroid.xy
                x1, y1 = x1[0], y1[0]
                factor = (x2-x1)*(y2-y1)
                sum_factor += factor
            if sum_factor < 0:
                cycle_copy.reverse()
                new_cycles.append(cycle_copy)
            else:
                new_cycles.append(cycle_copy)
        self.valid_cycles = new_cycles

        # Create a dictionary for storing the cycle carbon_ids
        ln_cid = {}

        from collections import deque
        changed_vals = []
        prev_cycle = []
        common_idxs = []
        common_nodes = []

        for idx_cyc, cycle in enumerate(self.valid_cycles):
            common = set(cycle).intersection(set(prev_cycle))
            if len(common):
                # Get index of common
                cmn_idx = cycle.index(list(common)[0])
                cycle_queue = deque(cycle)
                cycle_queue.rotate(-cmn_idx)
                changed_vals.append([idx_cyc, cycle_queue])
                common_idxs.append(idx_cyc)
                common_nodes.append(list(common)[0])
            else:
                # Ensure that the first and last node is also not a character,
                # In that case rotate twice
                if cycle[0].split('_')[0] not in self.line_signatures and \
                    cycle[-1].split('_')[0] not in self.line_signatures:
                    cycle_queue = deque(cycle)
                    cycle_queue.rotate(-2)
                    changed_vals.append([idx_cyc, cycle_queue])
                elif cycle[-1].split('_')[0] not in self.line_signatures:
                    cycle_queue = deque(cycle)
                    cycle_queue.rotate(-1)
                    changed_vals.append([idx_cyc, cycle_queue])
            prev_cycle = cycle

        for change in changed_vals:
            self.valid_cycles[change[0]] = list(change[1])

        for idx_cycle, cycle in enumerate(self.valid_cycles):
            start_cid = carbon_id
            idx_nd = 0
            if p == 2 and pdf_idx == 1:
                print(f'For cycle: {cycle}')
            while idx_nd < len(cycle):
                # Find the cur carbon position before iterating
                ln1 = cycle[idx_nd]

                # IF THIS IS A CHAR NODE SKIP NEXT LINE! (OTHERWISE YOU ADD AN EXTRA CARBON!)
                if ln1.split('_')[0] not in self.line_signatures:
                    try:
                        cur_pos = self.vert_polys[ln1][0]
                    except KeyError:
                        x,y = self.mst.nodes[ln1]['poly'].centroid.xy
                        cur_pos = [x[0], y[0]]
                    self.cd_graph.add_node(ln1, pos=cur_pos)
                    nxt_ln_id = (idx_nd+1) % len(cycle)
                    bond_type = self.find_bond_type(cycle[nxt_ln_id])
                    nxt_carbon_id = 'C_'+str(carbon_id)
                    
                    nxt_idx = (idx_nd + 2) % len(cycle)
                    if cycle[nxt_idx] in ln_cid.keys(): # This means that the node ends at the cycle start node and there is a record for this
                        # if p == 2 and pdf_idx == 1:
                            # print(f'Adding penulimate node {idx_nd} with starting node {0} for nodes: {cycle}')
                        nxt_prev_cid = ln_cid[cycle[nxt_idx]][0]
                        nxt_nxt_prev_cid = 'C_'+str(int(nxt_prev_cid.split('_')[-1])+1)
                        nxt_prev_pos = ln_cid[cycle[nxt_idx]][1]
                        nxt_prev_oth_pos = 1 if nxt_prev_pos == 0 else 0
                        vert1_pt = Point(self.vert_polys[cycle[nxt_idx]][nxt_prev_pos])
                        vert2_pt = Point(self.vert_polys[cycle[nxt_idx]][nxt_prev_oth_pos])
                        cur_pt = Point(cur_pos)
                        vert1_dist = vert1_pt.distance(cur_pt)
                        vert2_dist = vert2_pt.distance(cur_pt)
                        if vert1_dist < vert2_dist:
                            clos_id = nxt_prev_pos
                        else:
                            clos_id = nxt_prev_oth_pos
                        if clos_id == nxt_prev_pos:
                            if p == 2 and pdf_idx ==1:
                                print(f'Adding edge with same pos in char case: {ln1} with {nxt_prev_cid} for nxt_idx: {cycle[nxt_idx]}')
                            self.cd_graph.add_edge(ln1, nxt_prev_cid, type=bond_type)
                        else:
                            if p == 2 and pdf_idx ==1:
                                print(f'Adding edge with diff pos char case: {ln1} with {nxt_nxt_prev_cid} for nxt_idx: {cycle[nxt_idx]}')
                            self.cd_graph.add_edge(ln1, nxt_nxt_prev_cid, type=bond_type)
                    else:
                        # if p == 2 and pdf_idx == 1:
                            # print(f'Adding intermediate node {idx_nd} with starting node {(idx_nd+2) % len(cycle)} for nodes: {cycle}')
                        self.cd_graph.add_edge(ln1, nxt_carbon_id, type=bond_type)
                    
                    idx_nd = idx_nd + 2

                else:
                    if idx_nd == len(cycle)-1:
                        ln2 = cycle[0]
                    else:
                        ln2 = cycle[idx_nd+1]

                    ln1_vert1 = Point(self.vert_polys[ln1][0])
                    ln1_vert2 = Point(self.vert_polys[ln1][1])
                    if ln2.split('_')[0] in self.line_signatures:
                        ln2_vert = Point(self.vert_polys[ln2][0])
                    else:
                        ln2_vert = self.mst.nodes[ln2]['poly'].centroid
                    vert1_dist = ln1_vert1.distance(ln2_vert)
                    vert2_dist = ln1_vert2.distance(ln2_vert)
                    if vert1_dist > vert2_dist:
                        cur_pos_id = 0
                    else:
                        cur_pos_id = 1

                    cur_pos = self.vert_polys[cycle[idx_nd]][cur_pos_id]
                    bond_type = self.find_bond_type(cycle[idx_nd])

                    # If line already visited in another cycle (concentric cycle case),
                    # Find the correct carbon ID
                    if cycle[idx_nd] in ln_cid.keys():
                        found = True
                        prev_found = False
                        prev_carb_id = ln_cid[cycle[idx_nd]][0]
                        prev_pos_id = ln_cid[cycle[idx_nd]][1]
                        if cycle[idx_nd].split('_')[0] in self.line_signatures:
                            if prev_pos_id == cur_pos_id:
                                cur_carbon_id = int(prev_carb_id.split('_')[-1])
                            else:
                                cur_carbon_id = int(prev_carb_id.split('_')[-1])+1
                        else:
                            cur_carbon_id = cycle[idx_nd]
                            cur_pos = prev_pos_id
                    elif idx_nd > 0 and cycle[idx_nd-1] in common_nodes and idx_cycle in common_idxs:
                        found = False
                        prev_found = True
                        prev_carb_id = ln_cid[cycle[idx_nd-1]][0]
                        prev_pos_id = ln_cid[cycle[idx_nd-1]][1]
                        cur_vert_pt = Point(self.vert_polys[cycle[idx_nd]][0])
                        prev_vert1 = Point(self.vert_polys[cycle[idx_nd-1]][0])
                        prev_vert2 = Point(self.vert_polys[cycle[idx_nd-1]][1])
                        vert1_dist = prev_vert1.distance(cur_vert_pt)
                        vert2_dist = prev_vert2.distance(cur_vert_pt)
                        if vert1_dist < vert2_dist:
                            prev_vert_id = 0
                        else:
                            prev_vert_id = 1
                        if prev_vert_id == prev_pos_id:
                            cur_carbon_id = int(prev_carb_id.split('_')[-1])
                        else:
                            cur_carbon_id = int(prev_carb_id.split('_')[-1])+1
                    else:
                        found = False
                        prev_found = False
                        if cycle[idx_nd].split('_')[0] in self.line_signatures:
                            cur_carbon_id = carbon_id
                            ln_cid[cycle[idx_nd]] = ['C_'+str(cur_carbon_id), cur_pos_id]
                        else:
                            cur_carbon_id = cycle[idx_nd]
                            ln_cid[cur_carbon_id] = [cur_carbon_id, cur_pos_id]

                    # Add the current node:
                    if cycle[idx_nd].split('_')[0] in self.line_signatures:
                        if not found and not prev_found:
                            self.cd_graph.add_node('C_'+str(cur_carbon_id), pos=cur_pos)
                        # if p == 2 and pdf_idx == 1:
                        #     print(f"Adding node: {'C_'+str(carbon_id)} with pos: {cur_pos} for MST node: {cycle[idx_nd]}")
                        #     print(self.cd_graph.nodes(data=True))
                        
                            carbon_id += 1
                        char_node = False
                    else: # Add character node
                        self.cd_graph.add_node(cur_carbon_id, pos=cur_pos)
                        # if p == 2 and pdf_idx == 1:
                        #     print(f"Adding node: {cycle[idx_nd]} with pos: {cur_pos} for MST node: {cycle[idx_nd]}")
                        #     print(self.cd_graph.nodes(data=True))
                        char_node = True
                    
                    # Add the edge between this node and the next node
                    # Last iter connect with starting carbon
                    if idx_nd == len(cycle)-1:
                        if not char_node:
                            self.cd_graph.add_edge('C_'+str(cur_carbon_id), 'C_'+str(start_cid), type=bond_type)
                        else:
                            self.cd_graph.add_edge(cur_carbon_id, 'C_'+str(start_cid), type=bond_type)
                    else:
                        if cycle[idx_nd+1] in ln_cid.keys():
                            nxt_prev_cid = ln_cid[cycle[idx_nd+1]][0]
                            nxt_prev_pos = ln_cid[cycle[idx_nd+1]][1]
                            cur_pos_pt = Point(self.vert_polys[cycle[idx_nd]][cur_pos_id])
                            nxt_vert1 = Point(self.vert_polys[cycle[idx_nd+1]][0])
                            nxt_vert2 = Point(self.vert_polys[cycle[idx_nd+1]][1])
                            vert1_dist = cur_pos_pt.distance(nxt_vert1)
                            vert2_dist = cur_pos_pt.distance(nxt_vert2)
                            if vert1_dist < vert2_dist:
                                nxt_vert_id = 0
                            else:
                                nxt_vert_id = 1
                            if nxt_prev_cid.split('_')[0] not in self.line_signatures:
                                nxt_carbon_id = nxt_prev_cid
                            else:
                                if nxt_vert_id == nxt_prev_pos:
                                    nxt_carbon_id = nxt_prev_cid
                                else:
                                    nxt_carbon_id = 'C_'+str(int(nxt_prev_cid.split('_')[-1])+1)
                            if p == 2 and pdf_idx == 1:
                                print(f'Adding nxt char edge: {cur_carbon_id} to  {nxt_carbon_id}')
                            if not char_node:
                                self.cd_graph.add_edge('C_'+str(cur_carbon_id), nxt_carbon_id, type=bond_type)
                            else:
                                self.cd_graph.add_edge(cur_carbon_id, nxt_carbon_id, type=bond_type)
                        else:
                            if cycle[idx_nd+1].split('_')[0] in self.line_signatures:
                                nxt_carbon_id = carbon_id
                                nxt_char_node = False
                                if found:
                                    connect = False
                                else:
                                    connect = True
                            else: # Character node
                                nxt_carbon_id = cycle[idx_nd+1]
                                nxt_char_node = True
                                connect = True
                            if connect:
                                if p == 2 and pdf_idx == 1:
                                    print(f'Adding edge {cur_carbon_id} to  {nxt_carbon_id}')
                                    print(f'Prev found: {prev_found}')
                                    print(f'found: {found}')
                                if not nxt_char_node:
                                    if not char_node:
                                        self.cd_graph.add_edge('C_'+str(cur_carbon_id), 'C_'+str(nxt_carbon_id), type=bond_type)
                                    else:
                                        self.cd_graph.add_edge(cur_carbon_id, 'C_'+str(nxt_carbon_id), type=bond_type)
                                else:
                                    if not char_node:
                                        self.cd_graph.add_edge('C_'+str(cur_carbon_id), nxt_carbon_id, type=bond_type)
                                    else:
                                        self.cd_graph.add_edge(cur_carbon_id, nxt_carbon_id, type=bond_type)

                    # Add any peripheral clusters for this line
                    n_iter = self.mst.neighbors(cycle[idx_nd])
                    valid_neighs = [n for n in n_iter if n in self.peripherals and
                                    n not in added_neis]
                    peri_idxs = [self.peripherals.index(n) for n in valid_neighs]
                    added_neis.extend(valid_neighs)
                    

                    for idx_nei, valid_n in enumerate(valid_neighs):
                        same_lines = self.find_same_main_lines(valid_n)
                        if not len(same_lines):
                            for idx in self.c_clus_bonds.keys():
                                bonds = self.c_clus_bonds[idx]
                                if valid_n in bonds:
                                    same_lines = bonds
                                    break
                        valid_n_cent = self.mst.nodes[valid_n]['poly'].centroid
                        vert1_dist = ln1_vert1.distance(valid_n_cent)
                        vert2_dist = ln1_vert2.distance(valid_n_cent)
                        peri_bond_type = self.find_bond_type(valid_n)
                        try:
                            peri_cnctn = self.peripheral_nxt[peri_idxs[idx_nei]]
                        except IndexError:
                            return 'N/A'
                            # if p == 8 and pdf_idx == 1:
                            #     return 'N/A'
                            # else:
                            # print(self.peripherals)
                            # print(self.peripheral_nxt)
                            # print(valid_starts)
                            # print(self.mst.nodes())
                            # print(self.char_clusters)
                            # print(self.c_clus_bonds)
                            # print(self.valid_cycles)
                            # print(f'p: {p}, idx: {pdf_idx}')
                            # exit(0)
                        if peri_cnctn == 'New':
                            peri_cnctn = 'C_'+str(carbon_id)
                            carbon_id += 1
                        try:
                            if vert1_dist < vert2_dist:
                                if cur_pos_id == 0:
                                    if char_node:
                                        self.cd_graph.add_edge(cur_carbon_id, peri_cnctn, type=peri_bond_type)
                                    else:
                                        self.cd_graph.add_edge('C_'+str(cur_carbon_id), peri_cnctn, type=peri_bond_type)
                                        

                                else:
                                    if char_node:
                                        self.cd_graph.add_edge(nxt_carbon_id, peri_cnctn, type=peri_bond_type)
                                    else:
                                        self.cd_graph.add_edge('C_'+str(nxt_carbon_id), peri_cnctn, type=peri_bond_type)
                            else:
                                if cur_pos_id == 0:
                                    if char_node:
                                        self.cd_graph.add_edge(nxt_carbon_id, peri_cnctn, type=peri_bond_type)
                                    else:
                                        self.cd_graph.add_edge('C_'+str(nxt_carbon_id), peri_cnctn, type=peri_bond_type)
                                else:
                                    if char_node:
                                        self.cd_graph.add_edge(cur_carbon_id, peri_cnctn, type=peri_bond_type)
                                    else:
                                        self.cd_graph.add_edge('C_'+str(cur_carbon_id), peri_cnctn, type=peri_bond_type)
                        except TypeError:
                            print(peri_cnctn)
                            print(self.char_clusters)
                            print(self.c_clus_bonds)
                            print(self.cd_graph.nodes())
                            print(self.cd_graph.edges())
                            exit(0)
                    
                    # Now check if any Both exists
                    if len(floating_lines):
                        for meta in both:
                            if meta[1] == cycle[idx_nd]:
                                peri_cent = self.mst.nodes[meta[1]]['poly'].centroid
                                peri_bond_type = self.find_bond_type(meta[0][0])
                                clus_info = added_same_bonds_cid[tuple(self.find_same_main_lines(meta[2]))]
                                clus_carb_id = clus_info[0]
                                clus_nxtcarb_id = 'C_' + str(int(clus_carb_id.split('_')[-1])+1)
                                prev_vert_id = clus_info[1]
                                vert1 = Point(self.vert_polys[meta[2]][0])
                                vert2 = Point(self.vert_polys[meta[2]][1])
                                vert1_dist = vert1.distance(peri_cent)
                                vert2_dist = vert2.distance(peri_cent)
                                if vert1_dist > vert2_dist:
                                    if prev_vert_id == 0:
                                        peri_carbid = clus_carb_id
                                    else:
                                        peri_carbid = clus_nxtcarb_id
                                else:
                                    if prev_vert_id == 0:
                                        peri_carbid = clus_nxtcarb_id
                                    else:
                                        peri_carbid = clus_carb_id
                                
                                clus_ln_cent = self.mst.nodes[meta[2]]['poly'].centroid
                                vert1_dist = ln1_vert1.distance(clus_ln_cent)
                                vert2_dist = ln1_vert2.distance(clus_ln_cent)
                                if vert1_dist < vert2_dist:
                                    if cur_pos_id == 0:
                                        loop_carbid = 'C_' + str(cur_carbon_id)
                                    else:
                                        loop_carbid = 'C_' + str(nxt_carbon_id)
                                else:
                                    if cur_pos_id == 0:
                                        loop_carbid = 'C_' + str(nxt_carbon_id)
                                    else:
                                        loop_carbid = 'C_' + str(cur_carbon_id)
                                self.cd_graph.add_edge(peri_carbid, loop_carbid, type=peri_bond_type)

                    idx_nd += 1
        
                    
        # print(self.cd_graph.nodes())
        # print(self.cd_graph.edges())
        # viz_translated_cd(cd_graph=self.cd_graph)
        # exit(0)
        return self.cd_graph


                





    

    def find_bond_type(self, cur_line):
        lines = self.find_same_main_lines(cur_line)
        num_lines = len(lines)
        if num_lines: # Main line
            if num_lines == 1:
                if lines[0].split('_')[0] == 'Ln':
                    return self.line_type[1]
                elif lines[0].split('_')[0] == 'Wave':
                    return self.line_type[4]
                elif lines[0].split('_')[0] == 'SW':
                    return self.line_type[5]
                elif lines[0].split('_')[0] == 'HW':
                    return self.line_type[6]
            else:
                return self.line_type[num_lines]
        else: # Cluster line
            for idx in self.c_clus_bonds.keys():
                bonds = self.c_clus_bonds[idx]
                if cur_line in bonds:
                    if len(bonds) == 1:
                        if bonds[0].split('_')[0] == 'Ln':
                            return self.line_type[1]
                        elif bonds[0].split('_')[0] == 'Wave':
                            return self.line_type[4]
                        elif bonds[0].split('_')[0] == 'SW':
                            return self.line_type[5]
                        elif bonds[0].split('_')[0] == 'HW':
                            return self.line_type[6]
                    else:
                        return self.line_type[len(bonds)]

            return self.line_type[1]
