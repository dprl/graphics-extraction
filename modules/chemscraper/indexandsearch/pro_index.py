from modules.protables.ProTables import *
import os
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem
from pdf_annotate import PdfAnnotator, Location, Appearance


class Molecule:
    def __init__(self, smi_str, vector, doc_name, page_number, bbox):
        self.smi_str = smi_str
        self.vector = vector
        self.doc_name = doc_name
        self.page_number = page_number
        self.bbox = bbox


def annotate_pdf(pdf_file_path, results_list, top_k):
    k = 0
    a = PdfAnnotator(pdf_file_path)
    for molecule_result in results_list:
        if k > top_k:
            break
        candidate = molecule_result[1]
        score = molecule_result[0]
        k += 1
        bbox = [b*72/300 for b in candidate.bbox]

        a.add_annotation(
            'square',
            Location(x1=bbox[0], y1=792-bbox[3], x2=bbox[2], y2=792-bbox[1], page=candidate.page_number+1),
            Appearance(stroke_color=(1, 0, 0), stroke_width=1),
        )
        a.add_annotation(
            'text',
            Location(x1=bbox[0], y1=792 - bbox[3], x2=bbox[2], y2=792 - bbox[1], page=candidate.page_number+1),
            Appearance(content=str(k), font_size=10,  fill=(0,0,0)),
        )
    a.write('./modules/chemscraper/data/annotated.pdf')


if __name__ == "__main__":

    database = []

    chem_pro = read_pro_tsv(os.path.join("modules", "chemscraper", "output_pros", "ecfp_data_2.tsv"))
    for doc in chem_pro[0]:
        for page_n, page in enumerate(doc[1]):
            for fr in page:
                for smi in fr[1]:
                    if smi[5] == 'smi' or smi[5] == 'SMI':
                        #print(f"attempting to load {smi[4]} into database")
                        try:
                            m = Chem.MolFromSmiles(smi[4])
                            fp = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits=1024)

                            # fp = AllChem.GetMorganFingerprint(m, 2)
                            arr = np.zeros((0,), dtype=np.int8)
                            DataStructs.ConvertToNumpyArray(fp, arr)
                            molecule = Molecule(
                                smi_str=smi[4],
                                vector=fp,
                                doc_name=doc[0],
                                page_number=page_n,
                                bbox=[smi[0], smi[1], smi[2], smi[3]])
                            database.append(molecule)
                        except Exception as e:
                            print("failed to load smile", smi[4])
                            print(e)
    print("")
    print(" --------- performing search --------- ")
    print("")
    search_strings = ["O=C(OC)C1=CC=C(Br)C=C1"]

    for search_string in search_strings:

        m = Chem.MolFromSmiles(search_string)
        fp = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits=1024)
        search_results = []
        for candidate in database:
            search_results.append((DataStructs.TanimotoSimilarity(fp, candidate.vector), candidate))

        search_results_sorted = sorted(search_results, key=lambda x: x[0], reverse=True)
        print("Results for SMILES query:\n", search_string, "\n")
        for rank, result in enumerate(search_results_sorted):
            candidate = result[1]
            score = result[0]
            doc_name = os.path.basename( candidate.doc_name.replace('\n', '') )
            print(""
                  f"({rank+1}) {candidate.smi_str}"
                  f"\n      Tanimoto similarity: {score:.4f},"
                  f"\n      Doc:{doc_name} Page: {candidate.page_number}  BBox: {candidate.bbox}\n")

            annotate_pdf("./modules/chemscraper/data/or100.09.tables.pdf", search_results_sorted, 9)
        print(" --------- ")
        print("Results for a SMILES query are shown above (scroll up).")
        print("To see top-10 results annotated in the source PDF, see:")
        print("")
        print("    modules/chemscraper/data/annotated.pdf")

