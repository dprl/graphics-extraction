###########################################################################
#
# modules/chemscraper/markush.py
#
###########################################################################
# Created by: Ming Creekmore, 28 July 2023
###########################################################################
import nx_merge
import networkx as nx
from shapely.geometry import Polygon, Point, LineString, box
# TODO: Change this to use thresholds similar to others - parallel lines, char lines
CLOSE_EDGE_BRACKET_PERCENT = .16

###################################
# Bracket Logic
###################################

def pair_bracket_nodes(G):
    # creates edges to determine which brackets go together
    bracket_selector = nx_merge.node_attr_match( G, 'paranthesis', True )
    graph_view = nx.subgraph_view(G, filter_node=bracket_selector)
    n_data = list(graph_view.nodes.data())
    if len(n_data) == 0:
        return G
    else:
        G.graph['bracket'] = True
    # sort by leftmost
    n_data = sorted(n_data, key = lambda item: item[1]['box'][0])

    # TODO maybe take a stack approach if we get SS output that says which bracket matches
    # Closest min Y values will be a parenthesis pair
    # If bracket with same size within brackets, it will fail unless can determine bracket direction
    while len(n_data) > 1:
        main_circ = n_data[0][0]
        min_node = n_data[1][0]
        min_index = 1
        x1 = G.nodes[main_circ]['box'][1] # maybe use center
        x2 = G.nodes[min_node]['box'][1]
        min_dis = x2-x1
        for i in range(2, len(n_data)):
            other = n_data[i][0]
            x2 = G.nodes[other]['box'][1]
            dis = x2 - x1
            if dis < min_dis:
                min_index = i
                min_dis = dis
                min_node = other
        
        # add edge to connect parenthesis pair
        if not G.has_edge(main_circ, min_node):
            G.add_edge(main_circ, min_node)
        G.edges[(main_circ, min_node)]['bracket'] = True
        G.nodes[main_circ]['bracket'] = True
        G.nodes[min_node]['bracket'] = True # Only bracket pairs will be labeled as brackets, lone bracket ignored
        n_data.pop(min_index) # pop greater index first
        n_data.pop(0)
            
    return G

def sort_bracket_neis( G ):
    # Determining if a neighbor is inside or outside the bracket
    # Find the poly of both bracket's bb merged and use it to sort neighbors
    # Crossing are the nodes that "touch" one of the brackets before merge
    # Inside nodes are neighbors that are within the poly
    # Bracket label attachment must be Char in the right bottom corner
    bracket_selector = nx_merge.node_attr_match( G, 'paranthesis', True)
    graph = nx.subgraph_view(G, filter_node=bracket_selector)
    for node in graph.nodes:
        # TODO For now let's assume all brackets are round since selecting Circle
        G.nodes[node]['bracket_type'] = 'Round'
        # finding actual box container if there is a pair of brackets
        if 'contraction' in G.nodes[node]:
            box1 = G.nodes[node]['box']
            contract_nodes = G.nodes[node]['contraction']
            # Since bracket pair, only one node in contraction
            # TODO: Why loop here?
            for contract in contract_nodes:
                box2 = contract_nodes[contract]['box']
            G.nodes[node]['box2'] = box2
            # taking the first coor pair of box1 and the second coor pair of box2
            big_box = box1[:2] + box2[2:]
            poly = box(*big_box)

            # annotating neighbors
            neis = G.neighbors(node)
            crossing = set()
            inside = set()
            for nei in neis:
                 # need to determine if inside or outside box or crossing
                nei_box = G.nodes[nei]['shape_obj']
                if nei_box.contains(poly):
                    inside.add(nei)
                elif G.nodes[nei]['lbl_cls'] == 'Char':
                    # Assume that any connected char should be in righthand bottom corner
                    # and that there is only one since chars are connected
                    # Annotate character information and mark to contract node
                    # Note: the larger y-coor is the lower y-coor (0,0) is top lefthand corner
                    # TODO Find CDXML for top righthand corner (might not exist)
                    dis = Point(box2[2], box2[3]).distance(G.nodes[nei]['shape_obj'])
                    if dis < (G.graph['max_length'] * CLOSE_EDGE_BRACKET_PERCENT):
                        G.nodes[nei]['bracket'] = True
                        G.edges[(nei, node)]['bracket'] = True
                        name = G.nodes[nei]['lbl_type']

                        # RZ: HACK -- Molconvert generates 'ht' annotations for "MultipleGroup"
                        # Commenting out original condition, forcing use of 
                        # repeating structural unit (SRU)
                        #if name.isnumeric():
                        #    G.nodes[node]['attach'] = name
                        #    G.nodes[node]['bracket_usage'] = "MultipleGroup"
                        #else:
                        if True:   # HACK -- always use SRU
                            G.nodes[node]['attach'] = name
                            G.nodes[node]['bracket_usage'] = "SRU"
                        G.nodes[node]['attach_box'] = G.nodes[nei]['box']
                # only lines can be crossing
                elif G.edges[(nei, node)]['weight'] == 0:
                    crossing.add(nei)
            if not 'bracket_usage' in G.nodes[node]:
                G.nodes[node]['bracket_usage'] = "Unknown"
            G.nodes[node]['crossing'] = crossing
            G.nodes[node]['inside'] = inside
            G.nodes[node]['poly'] = poly
            G.nodes[node]['shape_obj'] = poly
    return G

def find_bracket_inner_nodes(G, F, added_pos):
    # G is the modified MST
    # F is the final graph where all other nodes and edges have already been added
    # added_pos is the position to F node dict
    # Finds the F nodes that are inside the bracket
    bracket_selector = nx_merge.node_attr_match( G, 'bracket', True )
    graph_view = nx.subgraph_view(G, filter_node=bracket_selector)

    # Labelling crossing bonds 
    bracket_crossing = dict() # dictionary relating bracket to crossing nodes
    for node in graph_view.nodes:
        if 'contraction' in G.nodes[node]: # makes sure bracket pair
            if len(G.nodes[node]['crossing']) > 0: # if there could be outside molecule
                inner_crossing = set()
                for cross in G.nodes[node]['crossing']:
                    verts = G.nodes[cross]['verts']
                    # if it is the inside vertex
                    if Point(verts[0]).distance(G.nodes[node]['poly']) == 0:
                        n1 = added_pos.get(tuple(verts[0]))
                        n2 = added_pos.get(tuple(verts[1]))
                        if n1 is None or n2 is None:    # HACK
                            continue
                        F.edges[(n1, n2)]['crossing'] = True
                        inner_crossing.add(n1)
                    else:
                        n1 = added_pos.get(tuple(verts[1]))
                        n2 = added_pos.get(tuple(verts[0]))
                        if n1 is None or n2 is None:    # HACK
                            continue
                        F.edges[(n1, n2)]['crossing'] = True
                        inner_crossing.add(n1)
                bracket_crossing[node] = inner_crossing
            else: # no conncetion to outside molecule (if there is one)
                inner_nodes = set()
                for inside in G.nodes[node]['inside']:
                    if G.nodes[inside]['lbl_cls'] == 'Char':
                        inner_nodes.add(G.nodes[inside]['pos'])
                    else:
                        verts = G.nodes[inside]['verts']
                        inner_nodes.add(added_pos.get(tuple(verts[0])))
                        inner_nodes.add(added_pos.get(tuple(verts[1])))
                bracket_crossing[node] = G.nodes[node]['inside']
    
    # Now add the complete inner structure as a bracket inner object
    edge_selector = nx_merge.edge_attr_exclude(F, 'crossing', True)
    graph_view = nx.subgraph_view(F, filter_edge=edge_selector)
    CCs = list(nx.connected_components(graph_view)) # list of sets of connected components
    for bracket in bracket_crossing:
        G.nodes[bracket]['inner_objects'] = set()
        for cc in CCs:
            cross_lst = list(bracket_crossing[bracket])
            if len(cross_lst) > 0:
                if cross_lst[0] in cc:
                    G.nodes[bracket]['inner_objects'].update(cc)
                    break
    return bracket_crossing.keys()
