###########################################################################
#
# modules/chemscraper/parser_debug.py
#
###########################################################################
# Created by: Ming Creekmore, 28 July 2023
###########################################################################

import os
from nx_debug_fns import *   # Includes network
import nx_merge
from shapely.geometry import Polygon, Point, LineString, box

################################
# Debug for Intersect Positions
################################

def adding_all_carbons( G ):
    # This is for visualization of where the positions of carbons of all lines are (do we have correct verts)
    # Adding carbons to graph. Assume verts of all lines and char_pos have already been found
    # Only add carbon if there is not a character node nearby
    line_selector = nx_merge.select_all_lines(G)
    graph_view = nx.subgraph_view(G, filter_node=line_selector)

    c_num = 1
    nodes = list(graph_view.nodes)
    for node in nodes:
        if 'verts' in G.nodes[node]:
            nodes_added, c_num = add_endpoint_nodes(G, node, c_num)
            # add bond between two nodes
            G.add_edge(nodes_added[0], nodes_added[1])
            G.nodes[node]['end_nodes'] = nodes_added

    return G

def add_endpoint_nodes(G, node, c_num):
    # adds two endpoints of a line as two carbon nodes
    # if endpoint is near a char, then doesn't add carbon node
    # Returns two nodes at endpoints (whether carbon or char) as nodes_added

    verts_to_add = [0,1] # tells which vertices still need to be added as a carbon node
    nodes_added = [] # character nodes added (so we can add edge later)
    # Finding if there are any character nodes
    for nei in G.neighbors(node):
        lbl = G.nodes[nei]['lbl_cls'] 
        if lbl == 'Char':
            nodes_added.append(nei)
            vert1 = G.nodes[node]['verts'][0]
            vert2 = G.nodes[node]['verts'][1]
            char_pos = G.nodes[nei]['pos']
            # Finding which vertex to replace with the character node pos
            if abs(Point(vert1).distance(Point(char_pos))) < abs(Point(vert2).distance(Point(char_pos))):
                verts_to_add.remove(0)
            else:
                verts_to_add.remove(1)
    n = G.nodes[node]
    # Add whatever vertices weren't replaced by char
    for vert in verts_to_add:
        # Note: cannot name as C_1 in case it overlaps with character cluster
        c_name = 'Carbon_' + str(c_num) 
        c_num += 1
        G.add_nodes_from([(c_name, {'pos': n['verts'][vert],'node': [node],'lbl_cls': 'Carbon'})])
        nodes_added.append(c_name)
    return nodes_added, c_num

def output_mst_PDF(title, dict_msts, mst_dir,fns_lst = [], selector = None, graph_descr = False, hide_enum=False, in_graphs=None):
    """
    Outputs a PDF file of a dictionary of a list of networkx graphs after a series of functions
    are performed on that graph
    Also outputs a graph_description to cmd line output if requested
    Args:
        @param title: title of the pdf being generated
        @param dict_msts: dict of networkX MST graphs over graphic objects
        @param mst_dir: directory that pdf will be saved to
        @param fns_lst: the list of functions used to transform graph
        @param selector: A function that takes in a graph and returns a selector function for a subgraph
        @graph_descr: True will give graph description to cmd line output
        @hide_enum: True will hide the numbers in the name labels in the graph PDF output

    Returns:
        Updated dict_final_graph: dict of visual graphs used in cdxml_conversion
    """
    check("Generating PDF for", title)
    merged_mst_dict = {}
    graph_title_pair_list = []
    for page_id in dict_msts:
        region = 0
        merged_mst_dict[page_id] = []
        for mst_graph in dict_msts[ page_id ]:
            if graph_descr:
                check("\nPage", page_id+1)
                check("Molecule", region)
            # transform graph using the functions given
            G = mst_graph

            #check("Graph G", gdescr(G))
            for fn in fns_lst:
                # RZ: Debug for new line closing method, requires input graphs
                # (deviation from single graph in/out pattern)
                # Checking function name attribute to catch special case.
                if fn.__name__ == 'close_edges':
                    G=fn(G, in_graphs[ page_id ][ region ])  # Formulas/regions are a list for each page
                else:
                    G = fn(G)

                #ncheck(">>>  After fn " + fn.__name__ + ", graph  G", gdescr(G))
            
            merged_mst_dict[page_id].append(G)
            if selector is not None:
                #pcheck("Types of G, selector", (type(G), type(selector)))
                G = nx.subgraph_view(G, selector(G))
            if graph_descr:
                show_vgdescr(G)
            next_pair = ( G, title + ': ( page_id: ' + str(page_id) + ', region: ' + str(region) + ')' )
            graph_title_pair_list.append( next_pair )
            region += 1
    title = title.upper().replace(' ', '_')
    draw_gtlist(graph_title_pair_list, os.path.join(mst_dir, "TEST_" + title + ".pdf"), hide_enum=hide_enum, invert_y=True)
    return merged_mst_dict
    
