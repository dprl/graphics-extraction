import argparse
import time
import tempfile
import os
import json
from fastapi import FastAPI, File
from starlette.responses import StreamingResponse, FileResponse

from modules.chemscraper.server.utils import process_pdf, process_yolo,\
    server_combine, annotate_whole_pdf, get_symbolscraper_version, get_yolo_version,\
    convert_pdf2image, process_yolo_from_images
import uvicorn

from modules.protables.debug_fns import DebugTimer
import modules.protables.debug_fns as debug_fns

debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.
app = FastAPI()
DPI = 300

# if env file not found we want to fail fast
SSCRAPER_SERVER_NAME = os.environ.get('SSCRAPER_SERVER_NAME', "localhost")
YOLO_SERVER_NAME = os.environ.get('YOLO_SERVER_NAME', "localhost")
LGAP_SERVER_NAME = os.environ.get('LGAP_SERVER_NAME', "localhost")

@app.post("/extractPdf")
async def extract(generate_svg: bool = False, pdf: bytes = File(..., media_type="application/pdf")):
    dTimer = DebugTimer("ChemScraper pipeline")
    json = process_pdf(pdf, SSCRAPER_SERVER_NAME)
    dTimer.qcheck("SymbolScraper (PDF instruction extraction)")

    print('\n [ Converting PDF to images ... ]')
    convert_pdf2image(pdf)
    dTimer.qcheck("Converting PDF to images")

    print('\n [ YOLOv8 MOLECULE DETECTION RUNNING ON PDF PAGE IMAGES... ]')
    # csv = process_yolo(pdf, DPI, YOLO_SERVER_NAME)
    csv = process_yolo_from_images(img_dir="temp/pdf_img_dir", service_name=YOLO_SERVER_NAME)
    dTimer.qcheck("YOLO molecule region detector")
    tsv, zip_io = server_combine(json, generate_svg, csv_dict=csv, pdf=pdf,
                                 lgap_server_name=LGAP_SERVER_NAME, dTimer=dTimer)
    return StreamingResponse(
        iter([zip_io.getvalue()]), 
        media_type="application/x-zip-compressed", 
        headers = { "Content-Disposition": f"attachment; filename=cdxml_tsv.zip"}
    )
    #return StreamingResponse(iter([tsv.getvalue()]), media_type="text/csv")

@app.post("/visualize")
def visualize(pdf: bytes = File(..., media_type="application/pdf")):
    xml = process_pdf(pdf, SSCRAPER_SERVER_NAME)
    csv = process_yolo(pdf, DPI, YOLO_SERVER_NAME)
    tsv = server_combine(xml, csv_dict=csv, lgap_server_name=LGAP_SERVER_NAME)
    output_pdf = tempfile.NamedTemporaryFile(delete=False).name
    annotate_whole_pdf(pdf, tsv, output_pdf)
    return FileResponse(
        output_pdf,
        media_type='application/pdf',
        filename="fake"
    )

@app.get("/get_version")
def get_version():    
    root = os.getcwd()
    with open(os.path.join(root,".cz.json")) as f:
        config = json.load(f)    
    chemscraper_version = config["commitizen"]["version"]
    try:
        symbolscraper_version = get_symbolscraper_version(SSCRAPER_SERVER_NAME)
    except:
        symbolscraper_version = "<SERVER ERROR>"

    try:
        yolo_version = get_yolo_version(YOLO_SERVER_NAME)
    except:
        yolo_version = "<SERVER ERROR>"
    
    return {
        "chemscraper_version":chemscraper_version,
        "symbolscraper_version":symbolscraper_version,
        "yolo_version":yolo_version,
        "lgap_version":"<pending>"
    }


# Create argparse function to accept port for the server
def parse_args():
    parser = argparse.ArgumentParser(description="Run the server")
    parser.add_argument("--port", type=int, default=8001, help="Port to run the server on")
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    uvicorn.run("server:app", host="0.0.0.0", port=args.port)
