## MST Post-Processing Algorithm
**Input:** Initial MST constructed from a fully connected graph derived from the symbol scraper graphics components.

1. **Connect closest lines based on proximity:** Compare every node in the graph to every other and if there are any nodes that are *very close* (close to 0 units) to each other based on their polygon distances and does not exist in the MST, a new edge is added.  
**Result:** This connects a ring and completes a loop(s) if it exists.

2. **Find the Centroid for each node in the graph**

3. **Find all the Character Sub-Atom Clusters (groups) using Custom BFS search in the graph:** Iterate through all the nodes, keep track of which node has been visited. If a character node is encountered, perform Breadth-First Search on the node. *Stop* when a line node is encountered.  
**Result:** This gives all the sub-atom clusters as one entity.

4. **Find if any of the Character Clusters are a part of the main-chain (loop/straight):** If there are more than *1 collective line neighbors* for the character cluster, this means that they CANNOT be an isolated character cluster.  
**Result:** Identifies which character clusters are inside the primary carbon chain and which are not.

5. **For the sub-atom clusters NOT a part of the primary chain find its connection type to the main chain:** This is done by taking the *one* line neighbor of the cluster and comparing distance of its centroid with the character centroid and finding if there are any other lines that fall in the same range. If so, they are added to the bond lines for the character cluster.  
**Result:** Gives the type of bond (single, double, triple, wavy or wedge) connecting the character cluster to the main chain.

6. **Group the main chain bonds together:** Parallel lines go together by comparing centroid distances.  
**Result:** This gives the single, double or triple bond connecting each line of the main-chain

7. **If NO main-chain bonds, directly reconstruct the CDXML graph**

8. **If there is a main chain, find if there is a loop or not in the main chain:** If no loop, start with two initial carbons, else start with one.

9. **For each bond line cluster in the main chain, find its nearest neighbor:** Keep iterating through the main-chain line clusters taking the next neighbor at each step until all the line clusters have been visited.

10. **Reconstruct the CDXML Graph**
