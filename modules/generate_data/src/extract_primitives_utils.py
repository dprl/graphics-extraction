import os
from typing import Tuple, Dict, List
import numpy as np

from modules.chemscraper.nx_debug_fns import shapesdraw
from modules.generate_data.src.demo_utils import display_pixel, display_pixel_old


from modules.protables.debug_fns import DebugTimer
import modules.protables.debug_fns as debug_fns

import cv2

import skimage
from skimage.io import imread
from skimage.measure import label
from skimage.morphology import skeletonize, medial_axis

from shapely.geometry import LineString

from collections import defaultdict
from modules.generate_data.src.utils.utils import imshow

# VERBOSE_DEBUG = True
VERBOSE_DEBUG = False

# TIME = True
TIME = False

if TIME: dTimer = DebugTimer("CV process")

def show(img: np.array):
    if VERBOSE_DEBUG:
        scale = 0.5
        img = cv2.resize(img, (0, 0), fx = scale, fy = scale)
        cv2.imshow("", img)
        while True:
            if cv2.waitKey(0) & 0xFF == ord('q'):
                break
    return

# VERBOSE_DEBUG = True
VERBOSE_DEBUG = False

# TIME = True
TIME = False

if TIME: dTimer = DebugTimer("CV process")

def show(img: np.array):
    if VERBOSE_DEBUG:
        scale = 0.5
        img = cv2.resize(img, (0, 0), fx = scale, fy = scale)
        cv2.imshow("", img)
        while True:
            if cv2.waitKey(0) & 0xFF == ord('q'):
                break
    return

class GeometryHandler:    
    def line2labelmap(self, line_dict: Dict, shape: Tuple) -> np.array:
        label_map = np.zeros(shape, dtype=int)
        for line_id, points in line_dict.items():
            label_map[points[:, 1], points[:, 0]] = line_id + 1
        return label_map

    def relabel(self, line_dict: Dict):
        old2new = {old_key: new_key for new_key, (old_key, _) in
                        enumerate(sorted(line_dict.items(), key=lambda x:tuple(x[1].min(axis=0))))}
        line_dict = {old2new[old_key]:line for (old_key, line) in line_dict.items()}
        return line_dict

def get_contour_map_hierarchy(hierarchy:np.array, mapping_ids:List[int]) -> List[List]:
    parents = list(np.argwhere(hierarchy[:,3]==-1).flatten())
    children = list(np.argwhere(hierarchy[:,3]!=-1).flatten())
    contour_groups = [[parents[i]] for i in range(len(parents))]
    for id in children:
        my_parent = mapping_ids.index(hierarchy[id, 3])
        contour_groups[parents.index(my_parent)].append(id)
    return contour_groups

def get_countour_map_from_image(image: np.array) -> Tuple[List[List], List[np.array], List[np.array]]:
    contours, hierarchy = cv2.findContours(image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    contours_hierarchy = [(contour.squeeze(1), hierarchy[0][idx], idx) for idx,contour in enumerate(contours)]
    contours_hierarchy_sorted = sorted(contours_hierarchy,key=lambda x:tuple(x[0].min(axis=0)))
    contour_map = get_contour_map_hierarchy(np.array([i[1] for i in contours_hierarchy_sorted]), [c[2] for c in contours_hierarchy_sorted])
    return contour_map, [i[0] for i in contours_hierarchy_sorted]

def segment_touching_bonds(markers, long_line_points):
    # display_pixel(markers, save_path='temp1.png')
    # Step 1: Find Marker IDs for Short Lines
    long_line_ids = set()
    for point in  long_line_points:
        x, y = int(point[0]), int(point[1])
        marker_value = markers[y, x]  # Note: OpenCV uses (y, x) indexing
        if marker_value > 1:  # Ignore background and boundary (-1)
            long_line_ids.add(marker_value)

    # Step 2: Set Marker Pixels Belonging to Long Lines to 0 (Background)
    markers_copy = markers.copy()
    long_line_mask = np.isin(markers, list(long_line_ids))
    markers_copy[long_line_mask] = 0
    # display_pixel(markers_copy, save_path='temp2.png')

    # Step 3: Apply Connected Components to Remaining Short Line Regions
    # This ensures new IDs do not overlap with the existing ones
    _, new_markers = cv2.connectedComponents((markers_copy > 1).astype(np.uint8))
    max_marker_id = np.max(markers)
    new_markers[new_markers > 0] += max_marker_id  # Shift new marker IDs to avoid overlap
    # display_pixel(new_markers, save_path='temp3.png')
    
    # Update the markers array with the new marker IDs for long line regions
    markers_copy[new_markers > 0] = new_markers[new_markers > 0]

    # Step 4: Add Back Marker Pixels Belonging to Long Lines
    markers_copy[long_line_mask] = markers[long_line_mask]
    # display_pixel(markers_copy, save_path='temp4.png')

    # Step 5: Relabel Marker IDs Efficiently to Make Them Sequential and Unique, Keeping Background as 1
    # final_markers = relabel_markers_efficient(markers_copy)

    # display_pixel(final_markers)

    # Step 5: Relabel Marker IDs to Make Them Sequential and Unique, Keeping Background as 1
    # unique_labels = np.unique(markers_copy)
    # unique_labels = unique_labels[unique_labels > 1]  # Ignore background and boundary
    # relabel_map = {old_label: new_label for new_label, old_label in enumerate(unique_labels, start=2)}
    # relabel_map[1] = 1  # Keep background as 1

    # final_markers = markers_copy.copy()
    # for old_label, new_label in relabel_map.items():
    #     final_markers[markers_copy == old_label] = new_label
    return markers_copy

def relabel_markers_efficient(markers_copy):
    # Find unique labels greater than 1 (ignore background and boundary)
    unique_labels = np.unique(markers_copy)
    unique_labels = unique_labels[unique_labels > 1]

    # Create a lookup table for efficient relabeling
    relabel_map = np.zeros(np.max(unique_labels) + 1, dtype=np.int32)
    relabel_map[unique_labels] = np.arange(2, 2 + len(unique_labels))
    relabel_map[1] = 1  # Keep background as 1

    # Use the lookup table to relabel the markers efficiently
    final_markers = relabel_map[markers_copy]

    return final_markers

#     return markers_copy

def process_markers_old(markers, long_line_points):
    # Step 1: Find Marker IDs for Long Lines
    long_line_ids = set()
    for point in long_line_points:
        x, y = int(point[0]), int(point[1])
        marker_value = markers[y, x]  # Note: OpenCV uses (y, x) indexing
        if marker_value > 1:  # Ignore background and boundary (-1)
            long_line_ids.add(marker_value)

    # Step 2: Set Marker Pixels Belonging to Long Lines to 0 (Background)
    markers_copy = markers.copy()
    long_line_indices = np.argwhere(np.isin(markers, list(long_line_ids)))
    for idx in long_line_indices:
        markers_copy[idx[0], idx[1]] = 0

    # Step 3: Apply Connected Components to Remaining Short Line Regions
    # This ensures new IDs do not overlap with the existing ones
    new_long_line_markers = np.zeros_like(markers, dtype=np.int32)
    _, new_markers = cv2.connectedComponents((markers_copy > 1).astype(np.uint8))
    max_marker_id = np.max(markers)
    new_markers[new_markers > 0] += max_marker_id  # Shift new marker IDs to avoid overlap
    
    # Update the markers array with the new marker IDs for long line regions
    markers_copy[new_markers > 0] = new_markers[new_markers > 0]

    # Step 4: Add Back Marker Pixels Belonging to Long Lines
    for idx in long_line_indices:
        markers_copy[idx[0], idx[1]] = markers[idx[0], idx[1]]

    # Step 5: Relabel Marker IDs to Make Them Sequential and Unique
    # final_markers = label(markers_copy, connectivity=2)  # Relabel sequentially starting from 1
    # final_markers[markers_copy == 1] = 1  # Ensure background remains labeled as 1
    display_pixel(markers_copy)
    return markers_copy

def global_algo(wob: np.array):
    # TIME = True
    dTimer = DebugTimer("Global Algo")
    '''
    [NEW] Overview:
        medial axis
        LSD
        shorten lines
        filter small ines
        merge close lines
        watershed
    '''
    if TIME: dTimer.qcheck("Global Algo Start")


    # Preprocess: Remove Image Border Lines
    if VERBOSE_DEBUG: print('Preprocess')
    ###########################################################################
    cv2.line(wob, (1, wob.shape[0]-1), (wob.shape[1]-1, wob.shape[0]-1), 0, 3)
    cv2.line(wob, (wob.shape[1]-1, 0), (wob.shape[1]-1, wob.shape[0]-1), 0, 3)
    cv2.line(wob, (1, 1), (1, wob.shape[0]-1), 0, 3)
    cv2.line(wob, (1, 1), (wob.shape[1]-1, 1), 0, 3)

    # Resize
    # wob = cv2.resize(wob, (0, 0), fx = 0.5, fy = 0.5, interpolation = cv2.INTER_CUBIC)
    # _, wob = cv2.threshold(wob, 0, 255, cv2.THRESH_OTSU)
    
    dst = np.zeros_like(wob)
    
    # wob = cv2.erode(wob, None) # Ideally this wouldn't be needed

    if VERBOSE_DEBUG: show(wob)
    if TIME: dTimer.qcheck("Preprocess Done")
    ###########################################################################

    # Skeletonize.
    if VERBOSE_DEBUG: print('Medial Axis/Skeletonization')
    ###########################################################################
    # sks = skimage.img_as_ubyte(medial_axis(wob).astype(bool))
    sks = skimage.img_as_ubyte(skeletonize(wob.astype(bool)))
    if VERBOSE_DEBUG:
        show(sks)
    if TIME: dTimer.qcheck("Medial Axis/Skeletonization Done")
    ###########################################################################


    # LSD
    if VERBOSE_DEBUG: print('LSD')
    ###########################################################################
    # lsd = cv2.createLineSegmentDetector(cv2.LSD_REFINE_NONE)
    lsd = cv2.createLineSegmentDetector(cv2.LSD_REFINE_STD)
    # lsd = cv2.createLineSegmentDetector(cv2.LSD_REFINE_ADV)
    
    raw_lines = lsd.detect(sks)[0]
    if VERBOSE_DEBUG: print('LSD Lines:', len(raw_lines))

    if VERBOSE_DEBUG: 
        drawn_img = lsd.drawSegments(np.zeros_like(sks), raw_lines)
        show(drawn_img)

    FILTER_THRESHOLD = 25
    # Each endpoint is shortened by this amount, so the real amount a line is shortened is double of this
    LINE_SHORTEN_AMOUNT = 5
    LINE_MERGE_WIDTH = 2
    lines = []

    long_line_points = []
    if raw_lines is not None:
        for i, line in enumerate(raw_lines, start=1):
            x1, y1, x2, y2 = line.flatten()
            line = LineString([(x1, y1), (x2, y2)]).normalize()
            
            # if line.length < FILTER_THRESHOLD:
            #     continue
            
            line = LineString([line.interpolate(LINE_SHORTEN_AMOUNT), line.interpolate(-LINE_SHORTEN_AMOUNT)])
            
            # if line.length < FILTER_THRESHOLD:
            #     continue

            if line.length >= FILTER_THRESHOLD:
                midpoint = line.interpolate(0.5, normalized=True)
                long_line_points.append(midpoint.coords[0])
                
            lines.append(line)
            cv2.line(dst, np.array(line.coords[0]).astype(int), np.array(line.coords[1]).astype(int), 
                     255, LINE_MERGE_WIDTH)

    if VERBOSE_DEBUG:
        show(dst)
        print('Lines After Filter:', len(lines))

    line_image = dst.copy()
    line_image = cv2.bitwise_and(wob, line_image)
    if TIME: dTimer.qcheck("LSD Done")
    ###########################################################################


    # Segment with watershed.
    if VERBOSE_DEBUG: print('Watershed')
    ###########################################################################
    BACKGROUND = 1
    unknown = cv2.subtract(wob, line_image)

    if VERBOSE_DEBUG:
        show(unknown)

    _, markers = cv2.connectedComponents(line_image)
    markers = markers+1
    markers[unknown==255] = 0

    if VERBOSE_DEBUG:
        marker_hue = np.uint8(179*markers/np.max(markers))
        blank_ch = 255*np.ones_like(marker_hue)
        marker_image = cv2.merge([marker_hue, blank_ch, blank_ch])

        marker_image = cv2.cvtColor(marker_image, cv2.COLOR_HSV2BGR)
        marker_image[marker_hue==0] = 0
        show(marker_image)

    dst = cv2.cvtColor(wob.copy(), cv2.COLOR_GRAY2BGR)

    if VERBOSE_DEBUG:
        print('Watershed Seeds:', len(np.unique(markers)))

    markers = cv2.watershed(dst, markers)
    ###########################################################################

    REMOVE_WATERSHED_BORDERS = True
    if REMOVE_WATERSHED_BORDERS:
        # Step 1: Create a mask to remove borders
        border_mask = np.zeros(markers.shape, dtype=bool)
        border_mask[:2, :] = True
        border_mask[-2:, :] = True
        border_mask[:, :2] = True
        border_mask[:, -2:] = True

        # Set the border pixels to BACKGROUND
        markers[border_mask] = BACKGROUND

        # Step 2: Create a mask for pixels to be processed
        mask = (markers == -1) | ((wob == 255) & (markers == 1))

        # Step 3: For each pixel that needs to be processed, replace the marker based on neighborhood
        x_indices, y_indices = np.where(mask)

        for x, y in zip(x_indices, y_indices):
            neighborhood = markers[max(x - 1, 0):min(x + 2, markers.shape[0]), max(y - 1, 0):min(y + 2, markers.shape[1])]
            neighborhood_flat = neighborhood.flatten()

            # Extract unique values greater than 1 and their counts
            vals, counts = np.unique(neighborhood_flat[neighborhood_flat > 1], return_counts=True)

            if len(counts) == 0:
                markers[x, y] = BACKGROUND
            else:
                # Set the marker to the most frequent value in the neighborhood
                markers[x, y] = vals[np.argmax(counts)]

    ## Removes watershed borders and fills in gaps around border intersections.
    #REMOVE_WATERSHED_BORDERS = True
    #if VERBOSE_DEBUG and REMOVE_WATERSHED_BORDERS: print('Remove Watershed Borders')
    ############################################################################
    #if REMOVE_WATERSHED_BORDERS:
    #    for x, y in np.ndindex(markers.shape):
    #        # HACK
    #        if x in (0, 1, markers.shape[0]-2, markers.shape[0]-1) or y in (0, 1, markers.shape[1]-2, markers.shape[1]-1):
    #            markers[x, y] = BACKGROUND
    #            continue

    #        if markers[x, y] == -1 or (wob[x, y] == 255 and markers[x, y] == 1):
    #            arr = markers[max(x-1, 0):min(x+2, markers.shape[0]), max(y-1, 0):min(y+2, markers.shape[1])].flatten()
    #            vals, counts = np.unique(arr[arr > 1], return_counts=True)

    #            if len(counts) == 0:
    #                markers[x, y] = BACKGROUND
    #                continue
    #            markers[x, y] = vals[np.argmax(counts)]

    if VERBOSE_DEBUG:
        for i, m in enumerate(np.unique(markers)):
            if m == 1:
                dst[markers == i] = (255, 255, 255)
                continue
            dst[markers == i] = (np.random.rand(3) * 255).astype('uint8')
        show(dst)
    if TIME: dTimer.qcheck("Watershed Done")
    ###########################################################################


    # Properly label empty connected components.
    if VERBOSE_DEBUG: print('Label Empty CCs')
    ###########################################################################
    empty_cc_mask = np.where(markers == BACKGROUND, wob, 0)
    _, ccs = cv2.connectedComponents(empty_cc_mask)

    ccs = ccs + np.max(markers)
    ccs[ccs == np.max(markers)] = 0
    markers = markers + ccs
    markers[markers == -1] = 1

    if VERBOSE_DEBUG:
        ccs_hue = np.uint8(179*ccs/np.max(ccs))
        blank_ch = 255*np.ones_like(ccs_hue)
        ccs_image = cv2.merge([ccs_hue, blank_ch, blank_ch])

        ccs_image = cv2.cvtColor(ccs_image, cv2.COLOR_HSV2BGR)
        ccs_image[ccs_hue==0] = 0
        show(ccs_image)
    ###########################################################################


    # Process markers.
    ## FIx undersementation of lines touching bonds
    markers = segment_touching_bonds(markers, long_line_points)

    # Display End Result If Debugging.
    if VERBOSE_DEBUG: print('Done With CV Process')
    ###########################################################################
    if VERBOSE_DEBUG:
        for i, m in enumerate(np.unique(markers)):
            if m == 1:
                dst[markers == m] = (255, 255, 255)
                continue
            dst[markers == m] = (np.random.rand(3) * 255).astype('uint8')
        dst = cv2.resize(dst, (0, 0), fx = 1, fy = 1)
        cv2.imwrite('temp.png', dst)
        show(dst)

    # Resize
    # markers = cv2.resize(markers.astype('uint8'), (0, 0), fx = 2, fy = 2)

    if TIME: dTimer.qcheck("Label Empty CC Done")
    ###########################################################################

    
    # Extract Primitives(Pixels). 
    if VERBOSE_DEBUG: print('Extract Prim(Pixels)')
    ###########################################################################
    primitives = defaultdict(list)
    unique_labels = np.unique(markers)
    unique_labels = unique_labels[np.where(unique_labels==1)[0][0]+1:] # Ignore -1, 0, and 1
    mixed_indices = np.argwhere(np.isin(markers, unique_labels))

    for idx in mixed_indices:
        lbl = markers[idx[0], idx[1]]
        primitives[lbl].append([idx[1], idx[0]])

    for k in sorted(primitives.keys()):
        primitives[k] = np.array(primitives[k]).reshape((-1,2))

    if TIME: dTimer.qcheck("Extract Prim(Pixels) Done")
    ###########################################################################

   
    # Extract Primitives(Contours) 
    if VERBOSE_DEBUG: print('Extract Prim(Contours)')
    ###########################################################################
    contours = []
    # markers[markers == -1] = 0
    # markers[markers == 1] = 0
    
    # contour_map, all_contours = get_countour_map_from_image(markers)
    # contours = []
    # for i, contour_group in enumerate(contour_map):
    #     contours_concat = all_contours[contour_group[0]]
    #     contours_just_draw = [all_contours[contour_group[0]]]
    #     for j in range(len(contour_group) - 1):
    #         contours_concat = np.concatenate((contours_concat, all_contours[contour_group[j + 1]]))
    #         contours_just_draw.append(all_contours[contour_group[j + 1]])
    #     contours.append(contours_just_draw)
    #     # import pdb; pdb.set_trace()

    # if VERBOSE_DEBUG:
    #     CC_image = np.zeros(wob.shape)
    #     cv2.drawContours(CC_image, all_contours, -1, color=1, thickness=cv2.FILLED)
    #     show(CC_image)
    # if TIME: dTimer.qcheck("Extract Prim(Contours) Done")
    ###########################################################################
    if TIME: print(dTimer)

    return primitives, lines, contours

def get_new_components(bow_image: np.array, wob_image: np.array, filename = None, save_path = None, visualize: bool = False) -> Tuple[Dict, Dict]:
    # make sure the image is the correct format
    # dTimer = DebugTimer("CV process")
    # dTimer.check("Start")
    wob_image = np.uint8(wob_image.astype(bool)) * 255
    max_dim = max(wob_image.shape)

    primitives, skeleton, contours = global_algo(wob_image)
    # dTimer.check("Global Algo Done")

    count_non_filtered = 0
    relabled_dict = {}
    for _, pixels in sorted(primitives.items(), key=lambda
                              x:tuple(x[1].min(axis=0))):
        ## Remove very small pixel noises
        # if len(pixels) / max_dim  > 0.003:
        if len(pixels) > 5:
            relabled_dict[count_non_filtered] = pixels
            count_non_filtered += 1

    handler = GeometryHandler()
    # relabled_dict = handler.relabel(relabled_dict)
    # import pdb; pdb.set_trace()

    if TIME: dTimer.qcheck("Relabel Done")
    #VISUALIZATION        
    # import pdb; pdb.set_trace()
    # display_pixel(handler.line2labelmap(relabled_dict,image.shape))
    if visualize and save_path and filename:
        shapesdraw(skeleton, save_path=os.path.join(save_path, filename + "_skeleton.pdf"))
        display_pixel(handler.line2labelmap(relabled_dict, wob_image.shape), save_path=os.path.join(save_path, filename + "_labelmap.pdf"))

    if TIME: dTimer.qcheck("Ouput Images Done")
    if TIME: print(dTimer)
    keys = list(relabled_dict.keys())
    return relabled_dict, contours

def get_images_processed(file: str) -> Tuple[np.array, np.array]:
    image = imread(file, as_gray=True).astype('uint8')
    _, wob = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU)
    return wob, ~wob

def run_one_image(file_path: str, out_path: str) -> None:
    wob_img, bow_img = get_images_processed(file_path)
    get_new_components(bow_img, wob_img, visualize=True, save_path=out_path, filename=os.path.basename(file_path)[:-4])
