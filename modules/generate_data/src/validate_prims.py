import os
import re
import argparse
import glob
from tqdm import tqdm
from modules.utils.dprl_map_reduce import map_concat
from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None


def check_primitive_uniqueness(file_path):
    """Check if a primitive belongs to more than one object in an .lg file."""
    primitive_to_objects = {}
    
    with open(file_path, 'r') as file:
        for line in file:
            if line.startswith("O,"):
                parts = line.strip().split(", ")
                if len(parts) >= 5:
                    obj_id = parts[1]
                    primitives = parts[4:]
                    sym_label = parts[2]
                    if sym_label.isdigit() and len(sym_label) > 1:
                        return False
                    for primitive in primitives:
                        if primitive in primitive_to_objects:
                            primitive_to_objects[primitive].add(obj_id)
                        else:
                            primitive_to_objects[primitive] = {obj_id}
    
    # Check if any primitive belongs to more than one object
    for obj_list in primitive_to_objects.values():
        if len(obj_list) > 1:
            return False
    return True

def process_directory_single(file_path):
    global CURR_COUNT, MAX_COUNT
    unique_files = []
    if check_primitive_uniqueness(file_path):
        unique_files.append(os.path.splitext(os.path.basename(file_path))[0])

    # HACK: Track progress using global thread-locked variable CURR_COUNT
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} files processed [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} files processed [ {percentage:3.2%} ]                    \n\n')
        #time.sleep(0.25) # for debugging
    return unique_files

def process_directory(input_dir, output_file, input_list=None):
    """Process all .lg files in a directory and list those where primitives belong to unique objects."""
    global CURR_COUNT, MAX_COUNT
    lg_files = glob.glob(os.path.join(input_dir, "*.lg"))
    if input_list:
        with open(input_list, 'r') as f:
            allowed_files = set(line.strip() for line in f)
        lg_files = [f for f in lg_files if os.path.splitext(os.path.basename(f))[0] in allowed_files]

    MAX_COUNT = len(lg_files)
    unique_files = map_concat(process_directory_single, lg_files)
    # for file_path in tqdm(lg_files, desc="Processing files", unit="file"):
    #     if check_primitive_uniqueness(file_path):
    #         unique_files.append(os.path.splitext(os.path.basename(file_path))[0])

    print(f"Total files after filtering = {len(unique_files)}")
    
    with open(output_file, 'w') as out_file:
        for filename in unique_files:
            out_file.write(filename + "\n")
    
    print(f"Processing complete. Results saved in {output_file}")

def main():
    parser = argparse.ArgumentParser(description="Check uniqueness of primitives in .lg files.")
    parser.add_argument("--input_dir", type=str, required=True, help="Directory containing .lg files")
    parser.add_argument("--output_file", type=str, required=True, help="Output file to save results")
    parser.add_argument("--input_list", type=str, help="File containing list of filenames to filter")
    args = parser.parse_args()
    process_directory(args.input_dir, args.output_file, args.input_list)

if __name__ == "__main__":
    main()
