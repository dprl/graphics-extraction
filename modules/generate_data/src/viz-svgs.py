import argparse
import streamlit as st
import os
import glob
import base64

st.set_page_config(layout="wide")

# def display_svgs(run_name, svg_paths):
#     svg_paths = sorted(svg_paths)
#     header_cols = st.columns([1])
#     st.markdown(f"**Run: {run_name}**", unsafe_allow_html=True)
#     with header_cols[0]:        
#         st.markdown("**Predicted SVGs**", unsafe_allow_html=True)    
#     for svg_path in svg_paths:     
#         filename = os.path.basename(svg_path)[:-4]
#         cols = st.columns([1])
#         with cols[0]:
#             display_svg(svg_path, filename)
    
def display_svgs(run_name, svg_paths, page, items_per_page):
    svg_paths = paginate(sorted(svg_paths), page, items_per_page)
    st.markdown(f"**Run: {run_name}**", unsafe_allow_html=True)
    st.markdown("**Predicted SVGs**", unsafe_allow_html=True)
    for svg_path in svg_paths:
        filename = os.path.basename(svg_path)[:-4]
        display_svg(svg_path, filename)

def display_pdf(path, image):
    """Helper function to display a PDF file."""
    if not image:
        with open(path, "rb") as f:
            base64_pdf = base64.b64encode(f.read()).decode('utf-8')
        pdf_display = f'<iframe src="data:application/pdf;base64,{base64_pdf}" width="100%" height="550" type="application/pdf"></iframe>'
        st.markdown(pdf_display, unsafe_allow_html=True)
    else:
        st.image(path)

def display_svg(path, filename=""):
    """Helper function to display an SVG file."""
    try:
        with open(path, "r") as svg_file:
            svg_data = svg_file.read()
        b64_svg = base64.b64encode(svg_data.encode('utf-8')).decode("utf-8")
        svg_display = f"""
        <div style="display: flex; justify-content: center; align-items: center; background-color: white; width: 100%;">
            <div style="text-align: center;">
                <img src="data:image/svg+xml;base64,{b64_svg}" alt="SVG Image" style="max-width: 100%; height: auto;">
                <p style="color: black;">{filename}</p>
            </div>
        </div>
        """
        st.markdown(svg_display, unsafe_allow_html=True)
    except Exception as e:
        st.error(f"Error displaying SVG: {e}")

def display_header_row(header_cols, header_name, graph_pdf_paths):
    with header_cols[0]:
        st.markdown(header_name, unsafe_allow_html=True)
    if graph_pdf_paths:
        with header_cols[1]:
            st.markdown("**Predicted visual graph**", unsafe_allow_html=True)
    with header_cols[-1]:
        st.markdown("**Predicted converted SVG**", unsafe_allow_html=True)

def display_files(run_name, svg_paths, pdf_paths, graph_pdf_paths, image=False,
                  header_name="**Input PDF**", page=1, items_per_page=10):
    num_cols = 2 + (1 if graph_pdf_paths else 0)
    if len(svg_paths) != len(pdf_paths):
        st.error("The lists of SVGs and PDFs do not have the same length.")
        return

    # Display the header row with titles
    st.markdown(f"**Run name: {run_name}**")
    header_cols = st.columns([1] * num_cols)
    display_header_row(header_cols, header_name, graph_pdf_paths)

    zipped_paths = zip(pdf_paths, graph_pdf_paths, svg_paths) if graph_pdf_paths else zip(pdf_paths, svg_paths)
    zipped_paths = paginate(list(zipped_paths), page, items_per_page)

    for paths in zipped_paths:
        display_file_row(paths, image, num_cols, graph_pdf_paths)

def display_file_row(paths, image, num_cols, graph_pdf_paths):
    filename = os.path.basename(paths[0])[:-4].replace("_labelmap", "")
    cols = st.columns(num_cols)
    
    with cols[0]:
        display_pdf(paths[0], image)
    if graph_pdf_paths:
        with cols[1]:
            display_pdf(paths[1], False)
        with cols[2]:
            display_svg(paths[2], filename)
    else:
        with cols[1]:
            display_svg(paths[1], filename)

def paginate(items, page, items_per_page=10):
    """Return the appropriate slice from items for the given page and items_per_page."""
    start = (page - 1) * items_per_page
    end = start + items_per_page
    return items[start:end]

def display_pagination_controls(total_items, items_per_page=10):
    """Display pagination controls and update the page number."""
    if 'page' not in st.session_state:
        st.session_state['page'] = 1  # Initialize the page if it doesn't exist in the session state

    num_pages = (total_items + items_per_page - 1) // items_per_page
    col1, col2, col3, col4 = st.columns([1, 1, 1, 2])

    # Page selector
    with col4:
        # Let user choose page using a select box or number input
        page_choice = st.number_input('Go to page', min_value=1, max_value=num_pages, value=st.session_state['page'], step=1, key='page_selector')
        if page_choice != st.session_state['page']:
            st.session_state['page'] = page_choice

    # Define callbacks for button presses
    def go_previous():
        st.session_state['page'] -= 1
    
    def go_next():
        st.session_state['page'] += 1

    # Display the "Previous" button only if it's not the first page
    with col1:
        if st.session_state['page'] > 1:
            prev_button = st.button('Previous', on_click=go_previous)

    # Display the current page number
    with col2:
        st.write(f"Page {st.session_state['page']} of {num_pages}")

    # Display the "Next" button only if it's not the last page
    with col3:
        if st.session_state['page'] < num_pages:
            next_button = st.button('Next', on_click=go_next)


# Custom function to parse command-line arguments
def parse_args():
    # Ignore the first argument ('streamlit', 'run', 'script.py'), parse the rest
    parser = argparse.ArgumentParser(description='Display SVG and PDF files')
    parser.add_argument('--svg_folder', '-s', help='Path to the folder containing SVG files',
                        default="modules/qdgga-parser/data/generated-dataset/chem/train/cdxml_svg/line/svgs")
    parser.add_argument('--pdf_folder', '-p', help='Path to the folder containing PDF files',
                        default="modules/qdgga-parser/data/generated-dataset/chem/train/pdf/")
    parser.add_argument('--img_folder', '-i', help='Path to the folder containing PNG files',
                        default="")
    parser.add_argument('--just_svgs', '-j', help='Just rendering SVG files', type=bool,
                        default=False)
    parser.add_argument('--run_name', '-n', help='Name of run at the top of the webpage', type=str,
                        default="DEFAULT")
    parser.add_argument('--filelist', '-f', help='File list to select', type=str,
                        default="")
    parser.add_argument('--graph_folder', '-g', help='Path to the folder containing graph files',
                        default="")
    parser.add_argument('--prim_pdf_folder', '-pi', help='Path to the folder\
                        containing primitive pdf files',
                        default="")
    parser.add_argument('--items_per_page', '-ip', help='No. of items per page',
                        default=10)
    args = parser.parse_args()
    return args

def filter_paths(paths, selected_files, svg_path=False, split_idx=0):
    # if not svg_path:
    #     if not split_idx:
    #         paths = list(filter(lambda x: os.path.basename(x).split(".")[0] in
    #                             selected_files, paths))
    #     else:
    #         paths = list(filter(lambda x: os.path.basename(x).split(".")[0][:split_idx] in
    #                             selected_files, paths))
    # else:
    #     paths = list(filter(lambda x: os.path.basename(os.path.dirname(x))[:-4] in
    #                         selected_files, paths))
    if not split_idx:
        paths = list(filter(lambda x: os.path.basename(x).split(".")[0] in
                            selected_files, paths))
    else:
        paths = list(filter(lambda x: os.path.basename(x).split(".")[0][:split_idx] in
                            selected_files, paths))

    return paths

def get_paths(args):
    # svg_paths = sorted(glob.glob(os.path.join(args.svg_folder, '*', '*.svg')),
    #                    key=lambda x:os.path.basename(os.path.dirname(x))[:-4])
    svg_paths = sorted(glob.glob(os.path.join(args.svg_folder, '*.svg')),
                       key=lambda x:os.path.basename(x)[:-4])        
    pdf_paths = sorted(glob.glob(os.path.join(args.pdf_folder, '*.pdf')),
                       key=lambda x:os.path.basename(x)[:-4])        
    png_paths = sorted(glob.glob(os.path.join(args.img_folder, '*.PNG')),
                       key=lambda x:os.path.basename(x)[:-4])        
    prim_pdf_paths = sorted(glob.glob(os.path.join(args.prim_pdf_folder,
                                                   '*_labelmap.pdf')),
                           key=lambda x:os.path.basename(x)[:-4])        
    graph_pdf_paths = sorted(glob.glob(os.path.join(args.graph_folder, '*.pdf')),
                           key=lambda x:os.path.basename(x)[:-4])        
    return svg_paths, pdf_paths, png_paths, prim_pdf_paths, graph_pdf_paths

def filter_all_paths(selected_files, svg_paths, pdf_paths, png_paths,
                             prim_pdf_paths, graph_pdf_paths):
    # svg_paths = filter_paths(svg_paths, selected_files, svg_path=True)
    svg_paths = filter_paths(svg_paths, selected_files)
    selected_files = set(map(lambda x:os.path.basename(x)[:-4], svg_paths))
    pdf_paths = filter_paths(pdf_paths, selected_files)
    png_paths = filter_paths(png_paths, selected_files)
    prim_pdf_paths = filter_paths(prim_pdf_paths, selected_files, split_idx=-9)
    graph_pdf_paths = filter_paths(graph_pdf_paths, selected_files)
    return svg_paths, pdf_paths, png_paths, prim_pdf_paths, graph_pdf_paths


def main():
    args = parse_args()
    items_per_page = args.items_per_page

    filelist_file = args.filelist
    selected_files = set()
    if filelist_file:
        with open(filelist_file, "r") as f:
            selected_files = set(filter(None, f.read().strip("").split("\n")))

    svg_paths, pdf_paths, png_paths, prim_pdf_paths, graph_pdf_paths = get_paths(args)
    if selected_files:
        svg_paths, pdf_paths, png_paths, prim_pdf_paths, graph_pdf_paths = \
            filter_all_paths(selected_files, svg_paths, pdf_paths, png_paths,
                             prim_pdf_paths, graph_pdf_paths)
        
    svg_files = set(map(lambda x:os.path.splitext(os.path.basename(x))[0], svg_paths))
    svg_paths, pdf_paths, png_paths, prim_pdf_paths, graph_pdf_paths = \
        filter_all_paths(svg_files, svg_paths, pdf_paths, png_paths,
                         prim_pdf_paths, graph_pdf_paths)
    run_name = args.run_name    
    if args.just_svgs:
        display_pagination_controls(len(svg_paths), items_per_page)
        display_svgs(run_name, svg_paths, st.session_state['page'], items_per_page)
    else:
        display_pagination_controls(len(svg_paths), items_per_page)
        if png_paths:
            display_files(run_name, svg_paths, png_paths, graph_pdf_paths,
                          image=True,
                          header_name="**Input real image**",
                          page=st.session_state['page'],
                          items_per_page=items_per_page)
        elif prim_pdf_paths:
            display_files(run_name, svg_paths, prim_pdf_paths, graph_pdf_paths,
                          header_name="**Input primitive**",
                          page=st.session_state['page'],
                          items_per_page=items_per_page)
        else:
            display_files(run_name, svg_paths, pdf_paths,  graph_pdf_paths,
                          header_name="**Input rendered PDF**",
                          page=st.session_state['page'],
                          items_per_page=items_per_page)


if __name__ == '__main__':
    main()
