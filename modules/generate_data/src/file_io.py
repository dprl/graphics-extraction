import os
import numpy as np
import cv2
from itertools import chain
from operator import itemgetter
from modules.chemscraper.nx_debug_fns import str_remove_suffix
from modules.generate_data.src.utils.utils import imshow
from modules.chemscraper.nx_debug_fns import shapedraw, draw_points


def read_region_csv_from_io(io, num_pages):
    page_region_bbs = [[] for _ in range(num_pages)]
    num_regions = 0
    for line in io:  # for each equation
        bbox_string_list = line.strip("\n").split(",")
        (page, min_x, min_y, max_x, max_y) = [round(float(c)) for c in bbox_string_list]
        # if int(page) >= numPages or int(page) < 0:
        #    pcheck(str(page) + " page and " + str(numPages) + " given pages.")
        # if maxX > minX and maxY > minY:
        page_region_bbs[int(page)].append((min_x, min_y, max_x, max_y))
        # pcheck(page_region_bbs, "next region BB entry")
        num_regions += 1

    # logging.debug("    " + str(num_regions) + " formula regions")
    return page_region_bbs

def points2contours(points):
    # Create a blank image
    # Determine the bounding box of the points
    x_min, y_min = np.min(points, axis=0)
    x_max, y_max = np.max(points, axis=0)
    
    # Create an image of the size of the bounding box
    width, height = x_max - x_min + 1, y_max - y_min + 1
    image = np.zeros((height, width), dtype=np.uint8)
    # Adjust points to the new coordinate system
    adjusted_points = points - np.array([x_min, y_min])

    # Draw the points onto the image
    # cv2.polylines(image, [adjusted_points], isClosed=True, color=255, thickness=1)
    cols, rows = adjusted_points.T
    image[rows, cols] = 255
    # Find contours
    # contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours, _ = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    original_contours = [(contour + np.array([[x_min, y_min]])).tolist()
                         for contour in contours]
    # image_new = np.zeros((y_max + 1, x_max + 1), dtype=np.uint8)
    # image_new = np.zeros_like(image)
    # if contours:
    #     cv2.drawContours(image_new, contours, contourIdx=-1, color=255,
    #                      thickness=-1)
    # imshow(image_new)
    # import pdb; pdb.set_trace()
    return original_contours

def generate_lg(args, filename, graph, pdf2lineids, line_dict, contours=None,
                mode="pdf"):
    path = os.path.join(args.save_path, 'lgs', mode)
    os.makedirs(path, exist_ok=True)

    outString = f"# IUD, {filename}\n"

    outString += f"# [ OBJECTS ]\n"
    outString += f"#    Objects (O): {len(graph.nodes())}\n"
    outString += "### Format: O, objId, class, 1.0 (weight), [ primitiveId list ]\n"
    prefix = "O, "
    middle = ", 1.0, "

    # Sort left-right top-down
    removed_nodes = set()
    sorted_nodes = sorted(sorted(graph.nodes(data=True), key=lambda x:x[1]['box'][1]), 
                          key=lambda x:x[1]['box'][0])
    for node, data in sorted_nodes:
        prims = ""
        obj_id = data["obj_id"]

        if data["lbl_cls"] == "Char" or data["lbl_cls"] == "Charge":
        # if data["lbl_cls"] == "Char": 
            label = str_remove_suffix(node)
            # label = data["lbl_type"]
        elif data['lbl_cls'] == 'Annotation':
            label = str_remove_suffix(node) + '_Ann' # Special class for annotation digits
        else:
            label = data.get("contraction_label", str_remove_suffix(node))
        if mode == "pdf":
            prims = ", ".join(str(x) for x in sorted(obj_id, key=lambda x: int(x)))
        else:
            prims = sorted(list(chain(*[pdf2lineids[x] for x in obj_id])),
                           key=lambda x:int(x))
            if len(prims):
                prims = ", ".join(str(x) for x in prims)
            else:
                removed_nodes.add(node)
        if prims:
            outString += prefix + node + ", " + label + middle + prims + "\n"

    # Write relation lines
    if len(graph.edges()) > 0:
        outString += "\n# [ RELATIONSHIPS ]\n"
        outString += f"#    Relationships (R): {len(graph.edges())}\n"
        outString += "### Format: R, parentId, childId, class, 1.0 (weight)\n"
        prefix = "R, "
        suffix = ", 1.0"
   
        for p, c in sorted(graph.edges()):
            # Remove edges for removed nodes
            if p not in removed_nodes and c not in removed_nodes:
                # edge = graph[p][c]
                label = "CONNECTED"
                # label = graph[p][c]['label']
                outString += prefix + str(p) + ", " + str(c) + ", " + label + \
                        suffix + "\n"
                outString += prefix + str(c) + ", " + str(p) + ", " + label + \
                        suffix + "\n"

    outString += "\n# [PRIMITIVE FEATURES]\n"
    if mode == "pdf":
        prefix = "#cc, "
        # Write #cc i.e. connected components' box coordinates
        for node, data in sorted_nodes:
            box_dict = data["prim_box_dict"]
            # Skip combined characters to prevent repeating writing ccs
            if not data.get("combined_char", False):
                obj_id = data["obj_id"]
                for cc in obj_id:
                    name = str(cc)
                    ul_x, ul_y, br_x, br_y = box_dict[cc]
                    outString += prefix + name + f", {ul_y}, {ul_x}, {br_y}, {br_x}" + "\n"

        outString += "\n"
        prefix = "#contours, "
        for node, data in sorted_nodes:
            contours_dict = data["prim_contours_dict"]
            # Skip combined characters to prevent repeating writing ccs
            if not data.get("combined_char", False):
                obj_id = data["obj_id"]
                for cc in obj_id:
                    name = str(cc)
                    contours = contours_dict.get(cc, [])
                    # if contours:
                    #     image_new = np.zeros((1576, 3062), dtype=np.uint8)
                    #     import pdb; pdb.set_trace()
                    #     if contours:
                    #         cv2.drawContours(image_new, contours, contourIdx=-1, color=255,
                    #                          thickness=-1)
                    #     import pdb; pdb.set_trace()
                        
                    for contour in contours:
                        contours_str = ", ".join(str(coordinate) for point_list in contour
                                              for point in point_list for coordinate in point)
                        outString += prefix + name + f", {contours_str}" + "\n"
    else:
        prefix = "#contours, "
        # Write #cc i.e. connected components' box coordinates
        for line_id, points in sorted(line_dict.items(),
                                          key=itemgetter(0)):
            name = str(line_id)
            old_contours = points2contours(points)
            for contour in old_contours:
                contours_str = ", ".join(str(coordinate) for point_list in contour
                                      for point in point_list for coordinate in point)
                outString += prefix + name + f", {contours_str}" + "\n"

        # New Contours
        # for i, cs in enumerate(contours):
        #     name = str(i)
        #     for contour in cs:
        #         contours_str = ", ".join(str(point) for point_list in contour for point in point_list)
        #         outString += prefix + name + f", {contours_str}" + "\n"

    return outString

    # with open(os.path.join(path, f"{filename}.lg"), "w") as f:
    #     f.write(outString)
