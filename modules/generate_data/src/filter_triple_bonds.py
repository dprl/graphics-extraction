import os
import argparse
import pandas as pd

# Custom function to parse command-line arguments
def parse_args():
    parser = argparse.ArgumentParser(description='Filter files')
    parser.add_argument('--input_csv', '-i', help='Path to the input csv',
                        default="")
    parser.add_argument('--output_csv', '-o', help='Path to the output csv',
                        default="")
    parser.add_argument('--complement', '-c', type=int, help='Generate complement file or not', default=0)
    args = parser.parse_args()
    if not args.output_csv:
        args.output_csv = os.path.join(os.path.splitext(args.input_csv)[0] + "_triple_bonds.csv")

    return args

def main():
    args = parse_args()
    # Step 1: Load the list of image IDs from the text file
    dataset = pd.read_csv(args.input_csv)
    # just_exact_matches = concatenated[["LONGEST_LENGTHS_DIFF_RATIO","NEG_CHARGE_Y_POSITION","NEG_CHARGE_LENGTH_TOLERANCE","levenshtein_normalized"]]

    triple_bond_set = dataset[dataset.SMILES.str.contains("#")]
    # final = triple_bond_set[["Unnamed: 0", "ground_truth"]]
    with open(os.path.splitext(args.output_csv)[0] + ".txt", "w") as f:
        f.write("\n".join(triple_bond_set.pubchem_cid.astype("string")).strip())
    triple_bond_set.to_csv(args.output_csv, index=False)
    num_filtered_files = len(triple_bond_set.index)
    print(f">> Filtering complete. \n   The filtered CSV is saved at: {args.output_csv}")

    if args.complement:
        complement_csv = os.path.splitext(args.output_csv)[0] + "_complement.csv"
        complement = dataset[~dataset.SMILES.str.contains("#")]
        with open(os.path.splitext(complement_csv)[0] + ".txt", "w") as f:
            f.write("\n".join(complement.pubchem_cid.astype("string")).strip())
        complement.to_csv(complement_csv, index=False)
        num_complement = len(complement.index)

        print(f"   The complement of filtered CSV is saved at: {complement_csv}")

    print(f">> Total formulas after filtering: {num_filtered_files}.")
    if args.complement:
        print(f">> Total formulas except filtered ones: {num_complement}.")

if __name__ == '__main__':
    main()
