import os
import shutil
import pandas as pd
import argparse
import glob
from tqdm import tqdm

def merge_files(input_dir, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    # Get a list of all CSV filenames in the directory
    csv_files = [os.path.basename(f) for f in glob.glob(os.path.join(input_dir, "chunk1", "*.csv"))]
    tsv_files = [os.path.basename(f) for f in glob.glob(os.path.join(input_dir, "chunk1", "*.tsv"))]
    
    # csv_files = ["smiles_input.csv", "symbols_distribution.csv"]
    # tsv_files = ["prim_warnings.tsv"]

    # Initialize dictionaries to hold dataframes for merging
    csv_data = {file: [] for file in csv_files}
    tsv_data = {file: [] for file in tsv_files}

    # Find all chunk directories
    chunk_dirs = [d for d in glob.glob(os.path.join(input_dir, 'chunk*')) if os.path.isdir(d)]

    total_files = 0
    for chunk_dir in chunk_dirs:
        for root, dirs, files in os.walk(chunk_dir):
            total_files += len(files)

    with tqdm(total=total_files, desc="Processing files") as pbar:
        for chunk_dir in chunk_dirs:
            for root, dirs, files in os.walk(chunk_dir):
                # Compute the relative path from the chunk directory to the current root
                rel_path = os.path.relpath(root, chunk_dir)
                # The output subdir should be directly under the output_dir
                output_subdir = os.path.join(output_dir, rel_path)
                
                # Create the sub-directory structure in the output directory
                if not os.path.exists(output_subdir):
                    os.makedirs(output_subdir)
                
                for file in files:
                    file_path = os.path.join(root, file)
                    if file in csv_files:
                        csv_data[file].append(pd.read_csv(file_path))
                    elif file in tsv_files:
                        tsv_data[file].append(pd.read_csv(file_path, sep='\t'))
                    else:
                        # Copy non-csv/tsv files to the output directory
                        shutil.copy(file_path, os.path.join(output_subdir, file))
                    pbar.update(1)
    
    # Merge and save CSV files
    for file, dataframes in csv_data.items():
        if dataframes:
            merged_df = pd.concat(dataframes, ignore_index=True)
            merged_df.to_csv(os.path.join(output_dir, file), index=False)
    
    # Merge and save TSV files
    for file, dataframes in tsv_data.items():
        if dataframes:
            merged_df = pd.concat(dataframes, ignore_index=True)
            merged_df.to_csv(os.path.join(output_dir, file), sep='\t', index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge chunk directories into a single directory.")
    parser.add_argument("--input_dir", "-i", 
                        help="The input directory containing all chunk directories.")
    parser.add_argument("--output_dir", "-o", 
                        help="The output directory to store the merged results.",
                        default=None)

    args = parser.parse_args()
    if not args.output_dir:
        args.output_dir = os.path.join(args.input_dir, "combined")

    merge_files(args.input_dir, args.output_dir)
    print(">> Output files saved in:", args.output_dir)

