import os
import numpy as np
import cv2
from shapely.geometry import LineString, Point, Polygon, MultiLineString
from shapely.affinity import translate
from scipy.ndimage import label
from modules.chemscraper.nx_debug_fns import shapedraw, draw_points, shapesdraw

from modules.generate_data.src.utils.debug_fns import *
from modules.generate_data.src.demo_utils import display_pixel
from modules.generate_data.src.utils.utils import imshow

from skimage.io import imread
from scipy import ndimage as ndi
from skimage.segmentation import watershed
from typing import List, Union, Tuple, Dict
from shapely.ops import nearest_points
import math
from glob import glob
from scipy.spatial import KDTree


#Fake init to allow type hint
class ConnectedComponent:
    pass

class GeometryHandler:
    def __init__(self):
        self.DROP_BBOX_RATIO = 0.9
        self.LENGTH_SIMILARITY_RATIO = self.DROP_BBOX_RATIO        
        self.SIMPLIFY_TOLERANCE = 1
        self.MINIMUN_LINES_POLYGON = 5
        self.ANGLE_SIMILARITY_DIFFERENCE = 15  
        self.RATIO_TOLERANCE = 1 - self.DROP_BBOX_RATIO 

    def get_midpoint_point(self, p1:Point, p2:Point) -> Point:
        return Point((p1.x+p2.x)/2, (p1.y+p2.y)/2)
    
    def are_lines_similar_angles(self, line1:LineString, line2:LineString)->bool:
                
        l1p1 = Point(line1.coords[0])
        l1p2 = Point(line1.coords[1])
                
        l2p1 = Point(line2.coords[0])
        l2p2 = Point(line2.coords[1])

        if l1p1.distance(l2p1)<l1p1.distance(l2p2):
            l2_translated = translate(line2,xoff=l1p1.x-l2p1.x,yoff=l1p1.y-l2p1.y)
        else:
            l2_translated = translate(line2,xoff=l1p1.x-l2p2.x,yoff=l1p1.y-l2p2.y)

        l2_translated_p1 = Point(l2_translated.coords[0])
        l2_translated_p2 = Point(l2_translated.coords[1])
        
        if l2_translated_p1.distance(l1p1)>l2_translated_p2.distance(l1p1):
            reference_l2_translated = l2_translated_p1
        else:
            reference_l2_translated = l2_translated_p2
        
        third_side = LineString([reference_l2_translated, l1p2])
        a = line1.length
        b = l2_translated.length
        c = third_side.length      

        if a*b==0:            
            return False  
        
        args_cos = (a*a+b*b-c*c)/(2*a*b)
        if args_cos>=-1.0 and args_cos<=1.0:
            C = math.acos(args_cos)*180/math.pi
        else:
            #args_cos value might be calculated as 1.0000001 or similar when lines are colinear
            return True

        difference = C                
        return difference<self.ANGLE_SIMILARITY_DIFFERENCE

    
    def are_lines_coolinear(self, line1:LineString, line2:LineString)->bool:        
        return self.are_lines_similar_angles(line1, line2) and line1.distance(line2)==0
    
    def get_line_midpoint(self, line:LineString)->Point:
        return self.get_midpoint_point(Point(line.coords[0]),Point(line.coords[1]))
    
    def get_closest_line_from_list(self, source_line:LineString, lines_list:List[float], from_midpoint:bool=True)->LineString: 
        if from_midpoint:
            closest = lines_list[0]
            source_line_midpoint = self.get_line_midpoint(source_line)
            closest_midpoint = self.get_line_midpoint(closest)
            
            for line in lines_list:
                line_midpoint = self.get_line_midpoint(line)
                if source_line_midpoint.distance(closest_midpoint) > source_line_midpoint.distance(line_midpoint):
                    closest_midpoint = line_midpoint
                    closest = line
        else:
            closest = lines_list[0]
            for line in lines_list:                
                if source_line.distance(closest) > source_line.distance(line):                
                    closest = line
        return closest
           
    def azimuth(self, point1:Point, point2:Point)->float:
        '''azimuth between 2 shapely points (interval 0 - 360)'''
        angle = np.arctan2(point2.x - point1.x, point2.y - point1.y)
        return np.degrees(angle) if angle >= 0 else np.degrees(angle) + 360
    
    def normalize_angle_0_180(self, angle:float)->float:
        if angle >= 180:
            return angle-180
        return angle    

    def merge_lines(self, line1:LineString, line2:LineString)->LineString:    

        l1p1 = Point(line1.coords[0])
        l1p2 = Point(line1.coords[1])
        l2p1 = Point(line2.coords[0])
        l2p2 = Point(line2.coords[1])

        if l1p1 == l2p1:    
            return LineString([l1p2,l2p2])
        elif l1p2 == l2p1:
            return LineString([l1p1,l2p2])
        elif l1p1 == l2p2:
            return LineString([l1p2,l2p1])            
        else:
            return LineString([l1p1,l2p1])
    
    def get_mid_line(self, line1:LineString, line2:LineString)->LineString:
        l1p1 = Point(line1.coords[0])
        l1p2 = Point(line1.coords[1])
        l2p1 = Point(line2.coords[0])
        l2p2 = Point(line2.coords[1])
        
        if l1p1.distance(l2p1) > l1p1.distance(l2p2):
            return LineString([self.get_midpoint_point(l1p1,l2p2),self.get_midpoint_point(l1p2,l2p1)])
        else:
            return LineString([self.get_midpoint_point(l1p1,l2p1),self.get_midpoint_point(l1p2,l2p2)])    
        
    def are_lines_same_length(self, line1:LineString, line2:LineString)->bool:
        largest = max(line1.length, line2.length)
        smallest = min(line1.length, line2.length)
        #avoid breaking when largest length is zero
        if largest==0:
            return False
        return smallest/largest > self.LENGTH_SIMILARITY_RATIO
    
    def get_lines_from_polygon_merged(self, points:List[Point])->List[LineString]:
        all_lines = []
        for i,point in enumerate(points[:-1]):            
            point1 = point
            point2 = points[i+1]
            line = LineString([point1, point2])
            if i!=0 and self.are_lines_coolinear(line,all_lines[-1]):
                line = self.merge_lines(line,all_lines[-1])            
                all_lines[-1] = line
            else:
                all_lines.append(line)
        return all_lines

    def crop_image_from_points(self, points_shape:np.array, original_image:np.array)->Union[np.array, np.array, np.array]:
        origin = np.array([np.min(points_shape[:,1]),np.min(points_shape[:,0])])
        origin_inverted = np.array([origin[1],origin[0]])
        end = np.array([np.max(points_shape[:,1]),np.max(points_shape[:,0])])        
        translated_points = points_shape-origin_inverted        
        temp_image = (original_image[origin[0]:end[0]+1,origin[1]:end[1]+1])        
        return origin_inverted,temp_image,translated_points

    def build_geometries_from_contours(self, contours:Tuple, shape_image:Tuple) -> Union[List[Polygon],List[Polygon],List[LineString]]:
        all = []
        all_simplified = []
        all_lines = []        
        
        for idx,contour in enumerate(contours):
            if idx != 0:
                buffer = -1
            else:
                buffer = 1
            try:                
                polygon = Polygon(contour).buffer(buffer)
                simplified = polygon.simplify(tolerance=self.SIMPLIFY_TOLERANCE)  
                points = simplified.exterior.coords               
                if polygon.area/(shape_image[0]*shape_image[1]) > self.DROP_BBOX_RATIO:
                    continue
            except:  
                continue
            all.append(polygon)
            all_simplified.append(simplified)
            all_lines = all_lines+self.get_lines_from_polygon_merged([Point(p) for p in points])
        return all,all_simplified, all_lines

    def slice_cc_from_polygons(self, cc_object: ConnectedComponent, average_all_lines_length:float) -> Union[List[np.array],List[LineString]]:
        all_lines = cc_object.lines
        
        points_shape = cc_object.pixels
        origin_inverted = cc_object.translation
        cc_image = cc_object.cc_image
        if len(all_lines)<self.MINIMUN_LINES_POLYGON:            
            if len(points_shape)>1 and len(all_lines)>0:
                longest_in_list = all_lines[all_lines.index(max(all_lines, key=lambda x:x.length))]                
                pixels_cc = np.argwhere(cc_object.cc_image == 1)[:,[1,0]]
                return [pixels_cc+origin_inverted],[longest_in_list]
            else:
                return [],[] 

        sorted_lines = sorted(all_lines, key=lambda x:-x.length)            
        all_lines_midpoints = np.array([self.get_line_midpoint(l).coords[0] for l in sorted_lines])
        squeletons = []
        merged_data = set()                      
        count = 1
        blank_image = np.zeros(cc_image.shape).astype(float)        
        min_distance = np.full(cc_image.shape, np.inf)
        tree_midpoints = KDTree(all_lines_midpoints)
        segmented_labeled = cc_image.copy().astype(int)



        for idx,lin in enumerate(sorted_lines):
            current_midpoint = self.get_line_midpoint(lin)                        
            idx_closest = tree_midpoints.query(current_midpoint.coords[0],k=2)[1][1].item()
            closest_line = sorted_lines[idx_closest]            
            merged_pair = idx in merged_data or idx_closest in merged_data
            if (self.are_lines_similar_angles(lin, closest_line) or self.are_lines_same_length(lin,closest_line)) and not merged_pair:
                skeleton_found = self.get_mid_line(lin,closest_line)
                if skeleton_found.length<average_all_lines_length:
                    continue
                squeletons.append(skeleton_found)
                merged_data.add(idx)
                merged_data.add(idx_closest)
                
                p1 = (np.array(skeleton_found.coords[0])).astype(int)
                p2 = (np.array(skeleton_found.coords[1])).astype(int) 

                current_skeleton = blank_image.copy()
                cv2.line(current_skeleton, tuple(p1), tuple(p2),1, 1)

                mask = current_skeleton.astype(bool)
                distance = ndi.distance_transform_edt(~mask)

                closer = (distance < min_distance) & (cc_image == 1)
                segmented_labeled[closer] = count
                min_distance[closer] = distance[closer]
                count += 1
          
        if len(squeletons) < 2:   
            pixels_cc = np.argwhere(cc_object.cc_image == 1)[:,[1,0]]         
            return [pixels_cc+origin_inverted],[sorted_lines[0]]         

        filtered_squeletons = []
        primitives = []
        
        for index,c_squeleton in enumerate(squeletons):
            points = np.argwhere(segmented_labeled==index+1)[:,[1,0]]
            if len(points)>0:
                filtered_squeletons.append(c_squeleton)
                primitives.append(points+origin_inverted)
        
        return primitives, filtered_squeletons          
   
    def line2labelmap(self, line_dict: Dict, shape: Tuple) -> np.array:
        label_map = np.zeros(shape, dtype=int)
        for line_id, points in line_dict.items():
            label_map[points[:, 1], points[:, 0]] = line_id+1
        return label_map
    
    def transform_geometry_list(self,geometries:List, translation:np.array)->List:
        return [translate(geo,xoff=translation[0],yoff=translation[1]) for geo in geometries]        

    def needs_split(self,skeleton:LineString,closest:LineString)->bool:
        length_ratio = min(skeleton.length, closest.length)/max(skeleton.length, closest.length)
        is_half_or_third = length_ratio>(0.33-self.RATIO_TOLERANCE) and length_ratio<(0.50+self.RATIO_TOLERANCE)

        p1 = skeleton.interpolate(skeleton.project(Point(closest.coords[0])))
        p2 = skeleton.interpolate(skeleton.project(Point(closest.coords[1])))

        projected_line = LineString([p1,p2])

        in_sight = max(projected_line.length, closest.length)>0 and \
            (min(projected_line.length, closest.length)/max(projected_line.length, closest.length))>=math.cos(self.ANGLE_SIMILARITY_DIFFERENCE*math.pi/180)

        are_crossing = skeleton.distance(closest)==0

        return self.are_lines_similar_angles(skeleton,closest) and is_half_or_third and in_sight and not are_crossing

    def split_bond(self, to_split:LineString, reference: LineString, pixels:np.array, original_image:np.array) -> Union[List[LineString], List[np.array]]:        
        split_point1 = nearest_points(to_split, Point(reference.coords[0]))[0]
        
        split_point2 = nearest_points(to_split, Point(reference.coords[1]))[0]

        if split_point1.distance(Point(to_split.coords[0])) > split_point2.distance(Point(to_split.coords[0])):
            skeleton1 = LineString([split_point1, to_split.coords[1]])
            skeleton3 = LineString([split_point2, to_split.coords[0]])
        else:
            skeleton1 = LineString([split_point1, to_split.coords[0]])        
            skeleton3 = LineString([split_point2, to_split.coords[1]])
        skeleton2 = LineString([split_point1,split_point2])

        cc_image = self.line2labelmap({0:pixels},original_image.shape)
        origin_inverted,new_image, _ = self.crop_image_from_points(pixels,cc_image)
        
        if skeleton1.length/to_split.length < self.RATIO_TOLERANCE:
            new_skeletons = [skeleton2, skeleton3]
        elif skeleton2.length/to_split.length < self.RATIO_TOLERANCE:
            new_skeletons = [skeleton1, skeleton3]
        elif skeleton3.length/to_split.length < self.RATIO_TOLERANCE:
            new_skeletons = [skeleton1, skeleton2]
        else:
            new_skeletons = [skeleton1, skeleton2, skeleton3]
        
        blank_image = np.zeros(new_image.shape).astype(float)         
        min_distance = np.full(new_image.shape, np.inf)
        segmented_labeled = new_image.copy().astype(int)
        count = 1        
        for index, current_skeleton in enumerate(new_skeletons):            
            p1 = ((np.array(current_skeleton.coords[0]))-origin_inverted).astype(int)
            p2 = ((np.array(current_skeleton.coords[1]))-origin_inverted).astype(int)            
        
            current_skeleton = blank_image.copy()
            cv2.line(current_skeleton, tuple(p1), tuple(p2),1, 1)

            mask = current_skeleton.astype(bool)
            distance = ndi.distance_transform_edt(~mask)

            closer = (distance < min_distance) & (new_image == 1)
            segmented_labeled[closer] = count
            min_distance[closer] = distance[closer]
            count += 1
        
        primitives = []
        for index in range(len(new_skeletons)):
            points = np.argwhere(segmented_labeled==index+1)[:,[1,0]]
            if len(points)>0:
                primitives.append(points+origin_inverted)        

        return new_skeletons,primitives

    def create_polygons_from_CCs(self, original_image:np.array, inverted_image:np.array)->Union[List[ConnectedComponent],float]:
        cc_objects = []
        acumulator = 0
        max_accumulator = 0
        max_lengths = []
        counter = 0
        
        contour_map,all_contours = self.get_countour_map_from_image(inverted_image)

        CC_image = np.zeros(inverted_image.shape)
        cv2.drawContours(CC_image, all_contours, -1, color=1, thickness=cv2.FILLED)

        for i, contour_group in enumerate(contour_map):            
            contours_concat = all_contours[contour_group[0]]
            contours_just_draw = [all_contours[contour_group[0]]]
            for j in range(len(contour_group)-1):                
                contours_concat = np.concatenate((contours_concat,all_contours[contour_group[j+1]])) 
                contours_just_draw.append(all_contours[contour_group[j+1]])            
            CC_image = np.zeros(original_image.shape)
            cv2.drawContours(CC_image, contours_just_draw, -1, color=1, thickness=cv2.FILLED)            
            origin_inverted, new_image, translated_points = self.crop_image_from_points(contours_concat, CC_image)            
            contours = [all_contours[contour_idx]-origin_inverted for contour_idx in contour_map[i]]                        
            cc_object = ConnectedComponent(i, translated_points, contours,
                                        original_image, new_image, origin_inverted)
            cc_object.build_geometries(self)            
            cc_objects.append(cc_object)
            acumulator+=cc_object.average_line_length
            max_accumulator += cc_object.max_line_length
            if cc_object.average_line_length>0:
                counter += 1                        
                max_lengths.append(cc_object.max_line_length)
        
        average_all_lines_length = acumulator / counter if counter else 0
        # average_all_lines_max_length = max_accumulator/counter

        # Estimate char bond threshold based on line with max difference
        # max_lengths = sorted(max_lengths)
        # adjacent_differences = [abs(max_lengths[i]-max_lengths[i+1]) for i in range(len(max_lengths)-1)]
        # max_diff_idx = np.argmax(adjacent_differences) + 1
        # char_bond_threshold = max_lengths[max_diff_idx]

        # return cc_objects, average_all_lines_length, average_all_lines_max_length, char_bond_threshold
        return cc_objects, average_all_lines_length
    
    def get_middle_line_polygon(self, polygon:Polygon)->LineString:
        polygon = polygon.minimum_rotated_rectangle
        if Point(polygon.exterior.coords[0]).distance(Point(polygon.exterior.coords[1])) < Point(polygon.exterior.coords[0]).distance(Point(polygon.exterior.coords[3])):
            p1 = self.get_midpoint_point(Point(polygon.exterior.coords[0]),Point(polygon.exterior.coords[1]))
            p2 = self.get_midpoint_point(Point(polygon.exterior.coords[2]),Point(polygon.exterior.coords[3]))
        else:
            p1 = self.get_midpoint_point(Point(polygon.exterior.coords[0]),Point(polygon.exterior.coords[3]))
            p2 = self.get_midpoint_point(Point(polygon.exterior.coords[1]),Point(polygon.exterior.coords[2]))
                        
        p1_ = nearest_points(polygon,p1)[0]
        p2_ = nearest_points(polygon,p2)[0]
        
        return LineString([p1_,p2_])        

    def segment_CCs(self,cc_objects:List[ConnectedComponent], average_all_lines_length) -> Union[Dict,List,List]:
        dict_primitives = {}
        skeleton_all = []
        all_simplified = []    

        counter_primitives = 0

        for idx, cc in enumerate(cc_objects):
            # if cc.average_line_length == 0:
            if cc.max_line_length == 0:
                continue
            # Character vs bond line CCs
            # if cc.max_line_length < char_bond_threshold:
            #     pixels_cc = np.argwhere(cc.cc_image == 1)[:,[1,0]]
            #     dict_primitives[counter_primitives] = pixels_cc+cc.translation                       
            #     longest_line = self.get_middle_line_polygon(Polygon(cc.pixels))
            #     skeleton_all = skeleton_all + self.transform_geometry_list([longest_line],cc.translation)
            #     all_simplified = all_simplified + self.transform_geometry_list(cc.geometries_simplified,cc.translation)
            #     counter_primitives+=1
            # else:
            slices, skeleton = self.slice_cc_from_polygons(cc, average_all_lines_length)                            
            skeleton_translated = self.transform_geometry_list(skeleton,cc.translation)                
            skeleton_all = skeleton_all + skeleton_translated
            all_simplified = all_simplified + self.transform_geometry_list(cc.geometries_simplified,cc.translation)
            for idx, slice in enumerate(slices):
                dict_primitives[counter_primitives + idx] = np.array(slice)                    
                    
            counter_primitives += len(slices) 
        return dict_primitives,skeleton_all,all_simplified

    def split_bonds(self,dict_primitives:Dict,skeleton_all:List,original_image:np.array) -> Union[Dict,List]:
        
        already_splited = {}
        for idx,skeleton in enumerate(skeleton_all):
            closest = self.get_closest_line_from_list(skeleton,skeleton_all[:idx]+skeleton_all[idx+1:], from_midpoint=False)
            idx_closest = skeleton_all.index(closest)            
            if skeleton.length>closest.length:
                needs_split = self.needs_split(skeleton, closest)
            else:
                needs_split = self.needs_split(closest, skeleton)
            if needs_split:                      
                if skeleton.length>closest.length and not idx in already_splited:                
                    
                    skeletons,primitives = self.split_bond(skeleton, closest, dict_primitives[idx], original_image)
                    already_splited[idx] = [
                        skeletons,
                        primitives
                    ]
                elif not idx_closest in already_splited:                
                    
                    skeletons,primitives = self.split_bond(closest, skeleton, dict_primitives[idx_closest], original_image)
                    already_splited[idx_closest] = [
                        skeletons,
                        primitives
                    ]
        for split_index, values in already_splited.items():
            skeletons,primitives = values
            for idx,prim in enumerate(primitives):
                if idx == 0:
                    skeleton_all[split_index] = skeletons[0]
                    dict_primitives[split_index] = prim
                else:
                    skeleton_all.append(skeletons[idx])
                    dict_primitives[len(dict_primitives)] = prim                                                
        return dict_primitives, skeleton_all


    def get_contour_map_hierarchy(self, hierarchy:np.array, mapping_ids:List[int]) -> List[List]:
        parents = list(np.argwhere(hierarchy[:,3]==-1).flatten())
        children = list(np.argwhere(hierarchy[:,3]!=-1).flatten())        
        contour_groups = [[parents[i]] for i in range(len(parents))]        
        for id in children:                        
            my_parent = mapping_ids.index(hierarchy[id, 3])            
            contour_groups[parents.index(my_parent)].append(id)        
        return contour_groups


    def get_countour_map_from_image(self, image: np.array) -> Union[List[List],List[np.array],List[np.array]]:
        contours, hierarchy = cv2.findContours(image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)           
        contours_hierarchy = [(contour.squeeze(1), hierarchy[0][idx], idx) for idx,contour in enumerate(contours)]        
        contours_hierarchy_sorted = sorted(contours_hierarchy,key=lambda x:tuple(x[0].min(axis=0)))                
        contour_map = self.get_contour_map_hierarchy(np.array([i[1] for i in contours_hierarchy_sorted]), [c[2] for c in contours_hierarchy_sorted])                                        
        return contour_map,[i[0] for i in contours_hierarchy_sorted]
    def relabel(self, line_dict):
        old2new = {old_key: new_key for new_key, (old_key, _) in
                       enumerate(sorted(line_dict.items(), key=lambda x:tuple(x[1].min(axis=0))))}
        line_dict = {old2new[old_key]:line for (old_key, line) in line_dict.items()}
        return line_dict

class ConnectedComponent:
    def __init__(self, id:int, pixels:np.array, contours:Tuple,
                 original_image:np.array, cc_image:np.array, translation:np.array):
        self.id = id
        self.geometries = []
        self.geometries_simplified = []
        self.lines = []
        self.pixels = pixels
        self.contours = contours
        self.original_image = original_image
        self.cc_image = cc_image
        self.average_line_length = 0
        self.max_line_length = 0
        self.translation = translation        
    
    def build_geometries(self, handler:GeometryHandler)->None:
        all, all_simplified, all_lines = handler.build_geometries_from_contours(self.contours, self.original_image.shape)
        if len(all_lines)>0:
            self.average_line_length = sum([i.length for i in all_lines])/len(all_lines)
            self.max_line_length = max([i.length for i in all_lines])
            self.geometries = all
            self.geometries_simplified = all_simplified
            self.lines = all_lines            

def old_get_new_components(image:np.array, inverted_image, filename=None, save_path=None, visualize:bool=False)->Union[Dict,Dict]:    
    handler = GeometryHandler()        
    # cc_objects, average_all_lines_length, \
    #     average_all_lines_max_length, char_bond_threshold = handler.create_polygons_from_CCs(image, 
    #                                                                                          inverted_image)        
    cc_objects, average_all_lines_length = handler.create_polygons_from_CCs(image, inverted_image)        
    dict_primitives, skeleton_all, all_simplified = handler.segment_CCs(cc_objects, average_all_lines_length)        
    # dict_primitives, skeleton_all, all_simplified = handler.segment_CCs(cc_objects, 
    #                                                                     average_all_lines_max_length, 
    #                                                                     average_all_lines_length)        
    # dict_primitives, skeleton_all, all_simplified = handler.segment_CCs(cc_objects, char_bond_threshold, 
    #                                                                     average_all_lines_length)
    dict_primitives, skeleton_all = handler.split_bonds(dict_primitives,skeleton_all,image)   
    count_non_single = 0
    relabled_dict = {}
    for pixels in dict_primitives.values():
        if len(pixels)>1:
            relabled_dict[count_non_single] = pixels
            count_non_single+=1
    
    relabled_dict = handler.relabel(relabled_dict)
    #VISUALIZATION        
    if visualize and save_path and filename:
        display_pixel(handler.line2labelmap(relabled_dict,image.shape),
                      save_path=os.path.join(save_path, filename + "_labelmap.pdf"))
        shapesdraw(all_simplified, save_path=os.path.join(save_path, filename + "_simplified.pdf"))
        shapesdraw(skeleton_all, save_path=os.path.join(save_path, filename + "_skeleton.pdf"))
    keys = list(relabled_dict.keys())
    return relabled_dict, {keys[i]: keys[i] for i in range(len(keys))}

def get_img_single(file:str)->np.array:
    img_in = imread(file, as_gray=True).astype(float)
    
    _, img_in = cv2.threshold(img_in, 0.7, 1, cv2.THRESH_BINARY)

    img_in = 1. - img_in
    img_in = (img_in*255).astype('uint8')
    img_in = 255-img_in    

    return img_in

def get_images_processed(file:str)->Union[np.array,np.array]:
    img_in = imread(file, as_gray=True).astype(float)
    
    _, img_in = cv2.threshold(img_in, 0.7, 1, cv2.THRESH_BINARY)

    img_in = 1. - img_in
    img_in = (img_in*255).astype('uint8')
    img_in_inverted = 255-img_in    

    return img_in, img_in_inverted

def run_one_image(file_path:str, out_path:str)->None:    
    img_in_inverted, img_in = get_images_processed(file_path)
    old_get_new_components(img_in, img_in_inverted, visualize=True, save_path=out_path, filename=os.path.basename(file_path)[:-4])

def run_folder(folder_path:str, save_path:str)->None:
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    for file in glob(os.path.join(folder_path,"*.png")):
        image = get_img_single(file)        
        old_get_new_components(image, filename=os.path.basename(file)[:-4], save_path=save_path, visualize=True)

if __name__ == "__main__":    
    #PURELLY DEBUG
    run_one_image("DEBUG/BOND_SPLIT_ERROR/CLEF/US20070049758A1_p0021_x0521_y1929_c00031.png","DEBUG/BOND_SPLIT_ERROR/CLEF/visualization")
