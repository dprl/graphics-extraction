absl-py==0.12.0
aiohttp
alphashape==1.3.1
argon2-cffi==20.1.0
astroid==2.5.6
astunparse==1.6.3
async-generator==1.10
attrs==20.3.0
backcall==0.2.0
beautifulsoup4==4.9.2
bleach==3.3.0
cached-property==1.5.2
cachetools==4.2.1
certifi==2021.5.30
cffi==1.14.5
chardet==4.0.0
charset-normalizer==2.0.12
cloudpickle==2.1.0
cssselect2==0.4.1
cycler==0.11.0
decorator==4.4.2
defusedxml==0.7.1
entrypoints==0.3
fastapi
flatbuffers==1.12
future==0.18.2
gast==0.4.0
google-auth-oauthlib==0.4.4
google-auth==1.29.0
google-pasta==0.2.0
graphviz==0.17.0
grpcio==1.34.1
h5py==3.1.0
idna==2.10
imageio==2.9.0
imagesize==1.2.0
importlib-metadata==3.10.0
iniconfig==1.1.1
intervaltree==3.1.0
ipykernel==5.5.0
ipython-genutils==0.2.0
ipython==7.16.1
ipywidgets==7.6.3
isort==5.8.0
jedi==0.18.0
Jinja2==2.11.3
joblib==1.1.0
jsonschema==3.2.0
jupyter-client==6.1.12
jupyter-console==6.4.0
jupyter-core==4.7.1
jupyter==1.0.0
jupyterlab-pygments==0.1.2
jupyterlab-widgets==1.0.0
keras-nightly==2.5.0.dev2021032900
Keras-Preprocessing==1.1.2
kiwisolver==1.3.1
lazy-object-proxy==1.6.0
lxml==4.9.0
Markdown==3.3.4
MarkupSafe==1.1.1
matplotlib==3.3.4
mccabe==0.6.1
mistune==0.8.4
mr4mp==2.7.1
nbclient==0.5.3
nbconvert==6.0.7
nbformat
nest-asyncio==1.5.1
networkx==2.5.1
notebook==6.3.0
numpy==1.21.6
oauthlib==3.1.0
opencv-python==4.6.0.66
opt-einsum==3.3.0
packaging==20.9
pandas==1.1.5
pandocfilters==1.4.3
parso==0.8.1
parts==1.6.0
pdf2image==1.16.2
pexpect==4.8.0
pickleshare==0.7.5
Pillow==8.4.0
pip==21.1.2
pluggy==0.13.1
prometheus-client==0.9.0
prompt-toolkit==3.0.18
protobuf==3.15.8
ptyprocess==0.7.0
py==1.10.0
pyasn1-modules==0.2.8
pyasn1==0.4.8
pycparser==2.20
pyenchant==3.2.1
Pygments==2.13.0
pylint==2.8.2
pymupdf==1.22.5
pyparsing==2.4.7
PyPDF2==1.28.6
pdf-annotate
pyrsistent==0.17.3
pytest==6.2.4
python-dateutil==2.8.1
python-engineio==4.2.1
python-multipart
python-socketio==5.4.0
python-Levenshtein
pytz==2021.1
PyWavelets==1.1.1
PyYAML
pyzmq==22.0.3
qtconsole==5.0.3
QtPy==1.9.0
rdkit-pypi==2021.9.4
reportlab==3.5.34
requests-oauthlib==1.3.0
requests==2.27.1
rsa==4.7.2
scikit-image==0.16.2
scikit-learn==0.24.1
scipy==1.4.1
Send2Trash==1.5.0
setuptools==52.0.0
Shapely==1.8.5.post1
six==1.16.0
sortedcontainers==2.3.0
soupsieve==2.2.1
sqlitedict==1.7.0
svglib==1.1.0
termcolor==1.1.0
terminado==0.9.4
testpath==0.4.4
tf-estimator-nightly==2.5.0.dev2021032501
threadpoolctl==3.1.0
tifffile==2020.9.3
tinycss2==1.1.1
toml==0.10.2
toolz==0.11.1
tornado==6.1
tqdm==4.59.0
traitlets==4.3.3
typed-ast==1.4.3
typing-extensions==3.7.4.3
urllib3==1.26.4
uvicorn[standard]
uvloop
wandb==0.12.15
wcwidth==0.2.5
webencodings==0.5.1
Werkzeug==1.0.1
wheel==0.36.2
widgetsnbextension==3.5.1
wrapt==1.12.1
zipp==3.4.1
